/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#pragma once
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

#ifdef __cplusplus
extern "C" {
#endif

int fmft_min(int i, int j);
int fmft_max(int i, int j);
/**
 * Compute the phi angle of spherical coordinate system
 * from position in 3D Cartesian coordinate system.
 * \param x Position along the x axis of Cartesian coordinate system.
 * \param y Position along the y axis of Cartesian coordinate system.
 */
float fmft_spherical_coord_phi_from_xy(float x,float y);

/**
 * Compute the theta angle of spherical coordinate system
 * from position in 3D Cartesian coordinate system.
 * \param z Position along the z axis of Cartesian coordinate system.
 * \param r Distance from the center of Cartesian coordinate system.
 */
float fmft_spherical_coord_theta_from_zr(float z, float r);

#ifdef __cplusplus
}
#endif

#ifndef DOXYGEN_SHOULD_SKIP_THIS
///////////////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.          //
///////////////////////////////////////////////////////////////////////////////
void fmft_root_printf(const int mpi_rank, const char* fmt, ... );

int fmft_is_whitespace_string(const char *s);

void fmft_safe_getline(char **lineptr, size_t *n, FILE *stream);

unsigned int fmft_count_lines(FILE *file);

unsigned int fmft_count_words(const char string[ ]);

#endif // DOXYGEN_SHOULD_SKIP_THIS
