/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "translation_matrix.h"

/**
 * This struct is used for storing elements of translation matrix
 * calculated up to polynomial order N in precision defined by the GMP_PREC constant.
 */
struct fmft_amatrix_gmp
{
	int N; /**< Highest polinoial order, for which matrix elements are stored.*/
	fpval* data; /**< Array of size N*N*N*N. */
};

//TODO find out if it is overloaded by default
mpf_class pow(const mpf_class &a, const int n)
{
	mpf_t rop;
	mpf_init(rop);
	mpf_pow_ui(rop, a.get_mpf_t(), n);
	mpf_class retval(rop);
	mpf_clear(rop);
	return retval;
}

inline double to_d(const mpf_class &a)
{
	return a.get_d();
}

inline double to_d(const double a)
{
	return a;
}

struct fmft_tmatrix_gmp* fmft_tmatrix_gmp_allocate(const int N)
{
	struct fmft_tmatrix_gmp* T_arb = (struct fmft_tmatrix_gmp*) calloc (1, sizeof(struct fmft_tmatrix_gmp));
	T_arb->N = N;
	T_arb->data = new fpval[N*N*N*N*N];

	return T_arb;
}

void fmft_tmatrix_gmp_free(struct fmft_tmatrix_gmp* T_arb)
{
	delete[] T_arb->data;
	free(T_arb);
}

/**
 * Allocates an instance of A_array_arb struct
 * that is intended to store A matrix elements
 * up to polynomial order N in precision defined by the GMP_PREC constant.
 * \param N Highest polynomial order, for which matrix elements will be stored.
 * \return Pointer to the allocated struct.
 */
struct fmft_amatrix_gmp* fmft_amatrix_allocate(const int N)
{
	struct fmft_amatrix_gmp* A_arb = (struct fmft_amatrix_gmp*) calloc (1, sizeof(struct fmft_amatrix_gmp));
	A_arb->N = N;
	A_arb->data = new fpval[2*N*N*N*N];

	return A_arb;
}

/**
 * Deallocates an instance of A_array_arb struct.
 */
void fmft_amatrix_free(struct fmft_amatrix_gmp* A_arb)
{
	delete[] A_arb->data;
	free(A_arb);
}

/**
 * Indexer for
 * arrays storing elements of A matrix.
 * \param N Highest polynomial order, for which the matrix elements are stored.
 * \param l In range [0, N-1].
 * \param l1 In range [0, N-1].
 * \param m In range [0, min(l, l1)]
 * \param k In range [abs(l-l1), l+l1]
 */
inline int index_amatrix(int N, int l, int l1, int m, int k)
{
	return 2*N*(N*(N*l + l1) + m) + k;
}

/**
 * Allocates an array of arbitrary precision floating point elements
 * of size n+1
 * and fills it with values of descending (i.e. normal) factorial
 * of each i in range [0, n].
 * \param n Highest number, for which the factorial will be calculated.
 * \return Pointer to the filled array.
 */
static fpval* factorial_descending_generate_gmp(const int n)
{
	fpval *desc_fact = new fpval[n+1];
	desc_fact[0] = 1;
	for (int i = 1; i <= n; ++i)
	{
		desc_fact[i] = desc_fact[i-1] * i;
	}

	return desc_fact;
}

static fpval factorial_descending_fetch_gmp(const fpval* desc, const int n)
{
	return desc[n];
}

/**
 * Allocates an array of arbitrary precision floating point elements
 * of size n+1
 * and fills it with values of rising factorial of x
 * for each i in range [0, n].
 * Rising factorial x_i = x*(x+1)...(x+i-1)
 * \param x Value, for each rising factorials will be caclulated.
 * \param n Highest number, for which the factorial will be calculated.
 * \return Pointer to the filled array.
 */
static fpval* factorial_rising_generate_gmp(const fpval x, const int n)
{
	fpval *rise_fact = new fpval[n+1];
	//TODO Find out if rising fatorial really defined for i = 0;
	rise_fact[0] = x;
	if (n > 0) {
		rise_fact[1] = x;
	}
	for (int i = 2; i <= n; ++i) {
		rise_fact[i] = (x + i - 1) * rise_fact[i-1];
	}

	return rise_fact;
}

//TODO Decide if needed
static fpval X_gmp(const int n, const int l, const int j, const fpval* desc_fact, const fpval* rise_fact_half)
{
	fpval one;
	if ((n-l-j-1) % 2 == 0)	{
		one = 1;
	} else 	{
		one = -1;
	}
	////fpval result = gmp_factorial(n-l-1);
	//fpval result = sqrt(descend_factorial1(desc, n-l-1)*rising_factorial1(rise, 0.5, n)*0.5);
	//result *= one/(descend_factorial1(desc, j)*descend_factorial1(desc, n-l-j-1)*rising_factorial1(rise, 0.5, l+j+1));
	fpval result = sqrt(desc_fact[n-l-1] * rise_fact_half[n] * 0.5);
	result *= one/(desc_fact[j] * desc_fact[n-l-j-1] * rise_fact_half[l+j+1]);
	return result;
}

/**
 * Allocates an array of arbitrary precision floating point elements
 * of size N*N*N
 * and fills it with values returned by X_arb()
 * for each n in range [1, N], l in range [0, n-1], j in range [0, n-l-1].
 * \param N Highest polynomial order, for which X_arb will be calculated.
 * \param desc_fact Pointer to an array of descending factorials generated with generate_desc_fact_array_arb(4*N)
 * \param rise_fact Pointer to an array of rising factorials generated by generate_rise_fact_array_arb(0.5, 4*N)
 * \return Pointer to the filled array.
 */
static fpval* xarray_generate_gmp(const int N, const fpval* desc_fact, const fpval* rise_fact_half)
{
	fpval* X_tab;
	X_tab = new fpval[N*N*N];
	for (int n = 1; n <= N; ++n) {
		for (int l = 0; l < n; ++l) {
			for(int j = 0; j <= n-l-1; j++) {
				X_tab[((n - 1)*N + l)*N + j] = X_gmp(n, l, j, desc_fact, rise_fact_half);
			}
		}
	}

	return X_tab;
}

static fpval C_gmp(const int N, const int n, const int l, const int n1, const int l1, const int k, const fpval* X_tab)
{
	fpval result=0;
	for (int j = 0; j <= fmft_min(n-l-1, k); ++j)
	{
		for (int j1 = 0; j1 <= fmft_min(n1-l1-1, k); ++j1)
		{
			//printf("n %d l %d n1 %d l1 %d k %d   j %d j1 %d\n", n, l, n1, l1, k,  j, j1);
			if (j + j1 == k)
				result += X_tab[((n - 1)*N + l)*N + j] * X_tab[((n1 - 1)*N + l1)*N + j1];//*X[((n1*NMAX+l1)*2*NMAX+j1)];
		}
	}

	return result;
}

/**
 * Calculates wigner 3-j symbol
 * \param desc Pointer to an array of descending factorials generated by factorial_descending_generate_gmp()
 */
static fpval fmft_wigner_3j_symbol_gmp(
			const int j1, const int j2, const int j3,
			const int m1, const int m2, const int m3,
			const fpval* desc)
{
//======================================================================
// After Wigner3j.m by David Terr, Raytheon, 6-17-04
//
// Compute the Wigner 3j symbol using the Racah formula.
//
//  / j1 j2 j3 \    '
//  |          |    '
//  \ m1 m2 m3 /    '
//
// Reference: Wigner 3j-Symbol entry of Eric Weinstein's Mathworld:
// http://mathworld.wolfram.com/Wigner3j-Symbol.html
//======================================================================

	// Error checking
	if (
		(2*j1 != floor(2*j1)) ||
		(2*j2 != floor(2*j2)) ||
		(2*j3 != floor(2*j3)) ||
		(2*m1 != floor(2*m1)) ||
		(2*m2 != floor(2*m2)) ||
		(2*m3 != floor(2*m3)) ) {
		printf("All arguments must be integers or half-integers.\n");
		exit(0);
	}

	// Additional check if the sum of the second row equals zero
	if (m1 + m2 + m3 != 0) {
		printf("3j-Symbol unphysical\n");
	}

	if (j1 - m1 != floor(j1 - m1)) {
		printf("2*j1 and 2*m1 must have the same parity\n");
	}

	if (j2 - m2 != floor(j2 - m2)) {
		printf("2*j2 and 2*m2 must have the same parity\n");
	}

	if (j3 - m3 != floor ( j3 - m3 )) {
		printf("2*j3 and 2*m3 must have the same parity\n");
	}

	if ((j3 > j1 + j2)  || (j3 < abs(j1 - j2))) {
		printf("j3 is out of bounds.\n");
	}

	if (abs(m1) > j1) {
		printf("m1 is out of bounds.\n");
	}

	if (abs(m2) > j2) {
		printf("m2 is out of bounds.\n");
	}

	if (abs(m3) > j3) {
		printf("m3 is out of bounds.\n");
	}

	int t1 = j2 - m1 - j3;
	int t2 = j1 + m2 - j3;
	int t3 = j1 + j2 - j3;
	int t4 = j1 - m1;
	int t5 = j2 + m2;

	int tmin = fmft_max(0, fmft_max(t1, t2));
	int tmax = fmft_min(t3, fmft_min(t4, t5));

	fpval wigner = 0;
	fpval one;
	for (int t = tmin; t <= tmax; ++t) {
		if(t % 2 == 0) {
			one = 1;
		} else {
			one = -1;
		}
		/*
    		wigner += one / ( descend_factorial1(desc, t) *\
    						descend_factorial1(desc, t-t1) *\
    						descend_factorial1(desc, t-t2) *\
    						descend_factorial1(desc, t3-t) *\
    						descend_factorial1(desc,t4-t) *\
    						descend_factorial1(desc,t5-t) );
		*/
		wigner += one /(factorial_descending_fetch_gmp(desc, t) *
				factorial_descending_fetch_gmp(desc, t  - t1) *
				factorial_descending_fetch_gmp(desc, t  - t2) *
				factorial_descending_fetch_gmp(desc, t3 - t) *
				factorial_descending_fetch_gmp(desc, t4 - t) *
				factorial_descending_fetch_gmp(desc, t5 - t) );
	}
	if ((j1 - j2 - m3) % 2 == 0) {
		one = 1;
	} else {
		one = -1;
	}

	wigner *= one * sqrt(
				factorial_descending_fetch_gmp(desc, j1+j2-j3) *
				factorial_descending_fetch_gmp(desc, j1-j2+j3) *
				factorial_descending_fetch_gmp(desc, -j1+j2+j3) /
				factorial_descending_fetch_gmp(desc, j1+j2+j3+1) *
				factorial_descending_fetch_gmp(desc, j1+m1) *
				factorial_descending_fetch_gmp(desc, j1-m1) *
				factorial_descending_fetch_gmp(desc, j2+m2) *
				factorial_descending_fetch_gmp(desc, j2-m2) *
				factorial_descending_fetch_gmp(desc, j3+m3) *
				factorial_descending_fetch_gmp(desc,j3-m3)
    		            );

	return wigner;
}

/**
 * Allocates an instance of A_array arb struct
 * and fills it.
 * \param N Highest polynomial order, for which elements of A matrix will be calculated.
 * \param desc_fact Pointer to an array of descending factorials generated with generate_desc_fact_array_arb()
 * \return Pointer to the filled struct.
 */
static struct fmft_amatrix_gmp* fmft_amatrix_create(const int N, const fpval* desc_fact)
{
	struct fmft_amatrix_gmp* Ak;
	Ak = fmft_amatrix_allocate(N);
	for (int l = 0; l < N; ++l) {
		printf("\rl = %d ", l);
		fflush(stdout);
		for (int l1 = 0; l1 < N; ++l1) {
			for (int m = 0; m <= fmft_min(l, l1); ++m) {
				for (int k = abs(l - l1); k <= l + l1; ++k) {
					fpval rootInsides = (2 * l + 1) * (2 * l1 + 1);
					fpval one;
					if (((k + l1 - l)/2 + m) % 2 == 0) {
						one = 1;
					} else {
						one = -1;
					}

					fpval val = one * (2*k + 1) * sqrt(rootInsides) *
									fmft_wigner_3j_symbol_gmp(l, l1, k, 0, 0, 0, desc_fact) *
									fmft_wigner_3j_symbol_gmp(l, l1, k, m, -m, 0, desc_fact);
					Ak->data[index_amatrix(N, l, l1, m, k)] = val;

					/*
					fpval val=one*(2*k+1)*sqrt(rootInsides) *\
									j_symb_type_3(l, l1, k, 0, 0, 0, desc_fact) *
									j_symb_type_3(l, l1, k, m, -m, 0, desc_fact);
					Ak->data[index_A_array(N, l, l1, m, k)] = val;

					val =j_symb_type_3(l, l1, k, m, -m, 0, desc, rise);
					printf("l %d l1 %d m %d k %d val=%.30f\n", l, l1, m, k, to_d(zindex_matrix_4d(M, l, l1, m, k)));
					gmp_printf("l %d l1 %d m %d k %d Ak %.30Ff\n", l, l1, m, k, val.get_mpf_t());
					 */
				}
			}
		}
	}
	printf("\n");

	return Ak;
}

/**
 * Calculates a value of Laguerre polynomial for parameter x
 * \param x Laguerre polynomial parameter
 * \param upper Laguerre polynomial upper index
 * \param lower Laguerre polynomial lower index
 * \return Value of Laguerre polynomial caculated in GMP_PREC precision.
 */
static fpval fmft_polynomial_laguerre_gmp(const fpval x, const fpval upper, const int lower)
{
	//int i; //Index.
	fpval i; //Index.
	fpval one =1.0;
	fpval two =2.0;

	fpval current_L = -x + upper + 1.0000;
	fpval prev_L = 1.00;
	fpval dp_L; //double previous L.

	if (lower == 0)
		return prev_L;

	// The algorithm starts at L_0^upper (= 1) and uses the recursion relation until L_lower^upper
	for (i = 2; i <= lower; ++i) {
		dp_L = prev_L;
		prev_L = current_L;
		current_L = one/i * ( (two*i - one + upper - x)*prev_L - (i - one + upper)*dp_L );//increment lower by 1.
	}

	return current_L;
}

/**
 * Calculates the integral in eq. (10) of "High-order analytic translation matrix elements for real-space six-dimensional polar Fourier correlations"
 * \param desc_fact Pointer to an array of descending factorials generated with generate_desc_fact_array_arb()
 * \param X_tab Pointer to an array of X entities generated with gmp_xarray_generate();
 * \return Value of the integral caculated in GMP_PREC precision.
 */
static fpval inv_Bessel_integr_gmp(
				const int N,
				const int n, const int l,
				const int n1, const int l1,
				const int k,
				const fpval rho2,
				const fpval* desc,
				const fpval* X_tab)
{
	fpval result = 0;
	fpval alpha = k;
	alpha  += 0.5;
	if (n - l + n1 - l1 - 2 < 2) {
		for (int j = 0; j <= n - l + n1 - l1 - 2; j++) {
			//alpha   = k;
			//alpha  += 0.5;
			result += C_gmp(N, n, l, n1, l1, j, X_tab) *
					factorial_descending_fetch_gmp(desc, j+(l+l1-k)/2) *
					fmft_polynomial_laguerre_gmp(rho2/4.0, alpha, j+(l+l1-k)/2);
		}
	} else {
		int coeff0 = (l+l1-k)/2;

		fpval Lprev = fmft_polynomial_laguerre_gmp(rho2/4.0, alpha, 0 + coeff0);
		result += C_gmp(N, n, l, n1, l1, 0, X_tab) *
				factorial_descending_fetch_gmp(desc, 0+coeff0) *
				Lprev;

		fpval Lcur  = fmft_polynomial_laguerre_gmp(rho2/4.0, alpha, 1 + coeff0);
		result += C_gmp(N, n, l, n1, l1, 1, X_tab) *
				factorial_descending_fetch_gmp(desc, 1+coeff0) *
				Lcur;

		fpval Lnext = 0;
		for(int j = 2; j <= n - l + n1 - l1 - 2; j++)
		{
			Lnext   = (2.0*(-1 + j + coeff0) + alpha + 1.0 - rho2/4.0)*Lcur - (-1 + j + coeff0 + alpha)*Lprev;
			Lnext  /= (j + coeff0);
			result += C_gmp(N, n, l, n1, l1, j, X_tab) *
					factorial_descending_fetch_gmp(desc, j + coeff0) *
					Lnext;
			Lprev   = Lcur;
			Lcur    = Lnext;
		}

	}

	//fpval expon = fpval(0.363309569359011254550541411658);
	return result * sqrt(pow(rho2/4.0, k)) * exp(to_d(-rho2/4.0));
}

struct fmft_tmatrix_gmp* fmft_tmatrix_gmp_compute(const int N, const float lambda, const double z)
{
	mpf_set_default_prec(GMP_PREC);
	struct fmft_tmatrix_gmp* T_arb;
	T_arb = fmft_tmatrix_gmp_allocate(N);
	fpval *descFact;
	fpval *riseFact;
	fpval *X_tab;
	fpval half = 0.5;

	descFact = factorial_descending_generate_gmp(4*N);
	riseFact = factorial_rising_generate_gmp(half, 4*N);
	X_tab = xarray_generate_gmp(N, descFact, riseFact);

	printf("Calculating Ak elements...\n");
	struct fmft_amatrix_gmp* A;
	A = fmft_amatrix_create(N, descFact);
	printf("done.\n");

	fpval R = z;
	int sign = (R < 0) ? (-1) : 1;
	fpval rho2 = R * R;
	rho2  /= lambda;
	fpval integral;
	fpval val;

	printf("Calculating T elements:\n");
	for (int n = 1; n <= N; ++n) {
		printf("\rn = %d ", n);
		fflush(stdout);
		for (int l = 0; l < n; ++l) {
			for (int n1 = 1; n1 <= n; ++n1) {
				for (int l1 = 0; l1 < n1; ++l1) {
					for (int k = abs(l-l1); k <= l + l1; k += 2) {
						integral = inv_Bessel_integr_gmp(N, n, l, n1, l1,
										 k, rho2, descFact, X_tab);
						for (int m = 0; m <= fmft_min(l, l1); ++m) {
							val = pow((double)sign, (double)(l1-l)) *
									A->data[index_amatrix(N, l, l1, m, k)] *
									integral;
							T_arb->data[fmft_index_tmatrix(N, n, l, n1, l1, m)] += val;
						}
					}
				}
			}
		}
	}
	for (int n = 1; n <= N; ++n) {
		for (int l = 0; l < n; ++l) {
			for (int n1 = 1; n1 <= n; ++n1) {
				for (int l1 = 0; l1 < n1; ++l1) {
					for (int m = 0; m <= fmft_min(l, l1); ++m) {
						val  = T_arb->data[fmft_index_tmatrix(N, n, l, n1, l1, m)];
						val *= pow(-1.0, (double)(l1-l));
						T_arb->data[fmft_index_tmatrix(N, n1, l1, n, l, m)] = val;
					}
				}
			}
		}
	}
	printf("\rdone.  \n");

	delete[] descFact;
	delete[] riseFact;
	delete[] X_tab;
	fmft_amatrix_free(A);

	return T_arb;
}


struct fmft_tmatrix* fmft_tmatrix_allocate(const int N)
{
	struct fmft_tmatrix* T = (struct fmft_tmatrix*) calloc(1, sizeof(struct fmft_tmatrix));
	T->N = N;
	T->data = (double*) calloc(N*N*N*N*N, sizeof(double));
	return T;
}


void fmft_tmatrix_free(struct fmft_tmatrix* T)
{
	free(T->data);
	free(T);
}


struct fmft_tmatrix* fmft_tmatrix_from_gmp(const struct fmft_tmatrix_gmp* T_gmp)
{
	struct fmft_tmatrix* T;
	T = fmft_tmatrix_allocate(T_gmp->N);

	for (int n = 1; n <= T->N; ++n)	{
		for (int n1 = 1; n1 <= n; ++n1) {               /////// \I switched these two to compare with existing
			for (int l = 0; l < n/*N*/; ++l) {	/////// /
				for (int l1 = 0; l1 < n1; ++l1) {
					int m_max = (l < l1 ? l : l1);
					for (int m = 0; m <= m_max; ++m) {
						double val = to_d(T_gmp->data[fmft_index_tmatrix(T_gmp->N, n, l, n1, l1, m)]);
						T->data[fmft_index_tmatrix(T->N, n, l, n1, l1, m)] = val;
						T->data[fmft_index_tmatrix(T->N, n1, l1, n, l, m)] = val * pow(-1.0, (double)(l1-l));
					}
				}
			}
		}
	}

	return T;
}


void fmft_tmatrix_gmp_write(
		const struct fmft_tmatrix_gmp* T_gmp,
		const char* filename)
{
	FILE *fp = fopen(filename, "wb");
	if (fp == NULL)	{
		fprintf(stderr, "File %s could not be opened.\n", filename);
		exit(EXIT_FAILURE);
	}
	double val;
	for (int n = 1; n <= T_gmp->N; ++n) {
		for (int n1 = 1; n1 <= n; ++n1) {           ///////I switched these two to compare with existing
			for (int l = 0; l < n/*N*/; ++l) {  ///////
				for (int l1 = 0; l1 < n1; ++l1)	{
					int m_max = (l < l1 ? l : l1);
					for (int m = 0; m <= m_max; ++m) {
						val = to_d(T_gmp->data[fmft_index_tmatrix(T_gmp->N, n, l, n1, l1, m)]);
						fwrite(&val, sizeof(double), 1, fp);
					}
				}
			}
		}
	}
	fclose(fp);

}

void fmft_tmatrix_read(
		struct fmft_tmatrix* T,
		const char* filename)
{
	double val;
	int err;

	// Binary input file
	FILE *fp = fopen(filename, "rb");
	if (fp == NULL)	{
		fprintf(stderr, "File %s could not be opened.\n", filename);
		exit(EXIT_FAILURE);
	}

	for (int n = 1; n <= T->N; ++n)	{
		for (int n1 = 1; n1 <= n; ++n1) {               /////// \I switched these two to compare with existing
			for (int l = 0; l < n/*N*/; ++l) {	/////// /
				for (int l1 = 0; l1 < n1; ++l1) {
					int m_max = (l < l1 ? l : l1);
					for (int m = 0; m <= m_max; ++m) {
						err = fread(&val, sizeof(double), 1, fp);
						if (err != 1) {
							fprintf(stderr, "Couldn't read matrix element from binfile:\n");
							fprintf(stderr, "n = %d, n1 = %d, l = %d, l1 = %d, m = %d, exiting...\n", n, n1, l, l1, m);
							exit(EXIT_FAILURE);
						}
						T->data[fmft_index_tmatrix(T->N, n, l, n1, l1, m)] = val;
						T->data[fmft_index_tmatrix(T->N, n1, l1, n, l, m)] = val * pow(-1.0, (double)(l1-l));
					}
				}
			}
		}
	}
	fclose(fp);
}

void fmft_tmatrix_tabulate(const int N, const float lambda, const double z, const char* path)
{
	struct fmft_tmatrix_gmp* T_arb;
	T_arb = fmft_tmatrix_gmp_compute(N, lambda, z);

	char T_file[512];
	sprintf(T_file, "%s/%s_%.2f_%d_%.2lf.dat", path, TFILE, lambda, N, z);
	printf("Writing translation matrix to file %s\n", T_file);

	fmft_tmatrix_gmp_write(T_arb, T_file);
	fmft_tmatrix_gmp_free(T_arb);
}

void fmft_tmatrix_load(struct fmft_tmatrix* T, const int Nmax, const float lambda, const double z, const char *path)
{
	char T_file[512];
	sprintf(T_file, "%s/%s_%.2f_%d_%.2lf.dat", path, TFILE, lambda, Nmax, z);
	if (access(T_file, F_OK) == -1)	{
		//tab_T_array(NMAX, z, path);
		fprintf(stderr, "Couldn't find the appropriate translation matrix file: %s\n", T_file);
		fprintf(stderr, "You can generate translation matrix files using the gen_trans_matrices.sh script\n");
		fprintf(stderr, "Exiting\n");
		exit(EXIT_FAILURE);
	}

	fmft_tmatrix_read(T, T_file);
}

struct fmft_coeffset* fmft_coeffset_translate(const struct fmft_coeffset* coeff, const struct fmft_tmatrix* T)
{
	int N = coeff->N;
	struct fmft_coeffset* tcoeff;
	tcoeff = fmft_coeffset_create(N);
	for(int n = 1; n <= N; ++n) {
		for(int l = 0; l < n; ++l) {
			for(int m = -l; m <= l; ++m) {
				double acc_r = 0.0;
				double acc_i = 0.0;
				for(int k = 1; k <= N; ++k) {
					for(int j = 0; j < k; ++j) {
						if(abs(m) <= j) {
							acc_r += T->data[fmft_index_tmatrix(N, n, l, k, j, abs(m))] *
									coeff->re[fmft_index_coeffset(N, k, j, m)];
							acc_i += T->data[fmft_index_tmatrix(N, n, l, k, j, abs(m))] *
									coeff->im[fmft_index_coeffset(N, k, j, m)];
						}
					}
				}
				tcoeff->re[fmft_index_coeffset(N, n, l, m)] = acc_r;
				tcoeff->im[fmft_index_coeffset(N, n, l, m)] = acc_i;
			}
		}
	}

	return tcoeff;
}
///////////////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.          //
///////////////////////////////////////////////////////////////////////////////
