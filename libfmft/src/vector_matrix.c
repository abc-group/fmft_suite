/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include "vector_matrix.h"

void fmft_rot_matrix_fill_active(double rm[3][3], double alpha, double beta, double gamma)
{
	rm[0][0] = cos(gamma)*cos(beta)*cos(alpha) - sin(gamma)*sin(alpha);
	rm[1][0] = cos(gamma)*cos(beta)*sin(alpha) + sin(gamma)*cos(alpha);
	rm[2][0] = -cos(gamma)*sin(beta);
	rm[0][1] = -sin(gamma)*cos(beta)*cos(alpha) - cos(gamma)*sin(alpha);
	rm[1][1] = -sin(gamma)*cos(beta)*sin(alpha) + cos(gamma)*cos(alpha);
	rm[2][1] = sin(gamma)*sin(beta);
	rm[0][2] = sin(beta)*cos(alpha);
	rm[1][2] = sin(beta)*sin(alpha);
	rm[2][2] = cos(beta);
//	{ cos(g)*cos(b)*cos(a) - sin(g)*sin(a),  cos(g)*cos(b)*sin(a) + sin(g)*cos(a), -cos(g)*sin(b)},
//	{-sin(g)*cos(b)*cos(a) - cos(g)*sin(a), -sin(g)*cos(b)*sin(a) + cos(g)*cos(a),  sin(g)*sin(b)},
//	{ sin(b)*cos(a)                       ,  sin(b)*sin(a)                       ,  cos(b)       }
}

void fmft_rot_matrix_fill_passive(double rm[3][3], double alpha, double beta, double gamma)
{
	rm[0][0] = cos(gamma)*cos(beta)*cos(alpha) - sin(gamma)*sin(alpha);
	rm[0][1] = cos(gamma)*cos(beta)*sin(alpha) + sin(gamma)*cos(alpha);
	rm[0][2] = -cos(gamma)*sin(beta);
	rm[1][0] = -sin(gamma)*cos(beta)*cos(alpha) - cos(gamma)*sin(alpha);
	rm[1][1] = -sin(gamma)*cos(beta)*sin(alpha) + cos(gamma)*cos(alpha);
	rm[1][2] = sin(gamma)*sin(beta);
	rm[2][0] = sin(beta)*cos(alpha);
	rm[2][1] = sin(beta)*sin(alpha);
	rm[2][2] = cos(beta);
//	{ cos(g)*cos(b)*cos(a) - sin(g)*sin(a),  cos(g)*cos(b)*sin(a) + sin(g)*cos(a), -cos(g)*sin(b)},
//	{-sin(g)*cos(b)*cos(a) - cos(g)*sin(a), -sin(g)*cos(b)*sin(a) + cos(g)*cos(a),  sin(g)*sin(b)},
//	{ sin(b)*cos(a)                       ,  sin(b)*sin(a)                       ,  cos(b)       }
}


int fmft_rot_matrix_compare(
			const double A[3][3],
			const double B[3][3],
			const double theta_cutoff)
{
	double AT[3][3];
	fmft_rot_matrix_transpose(AT, A);

	double C[3][3];
	fmft_rot_matrix_mult(C, AT, B);

	double theta = fmft_rot_matrix_get_theta(C);
	//printf("theta: %f, theta ang: %f\n", theta, theta*180.0/M_PI);

	if (theta < theta_cutoff)
		return 1;
	else
		return 0;
}

double fmft_rot_matrix_get_theta(const double A[3][3])
{
	double tr = (fmft_rot_matrix_trace(A) - 1.0) / 2.0;

	if (tr < -1.0)
		tr = -1.0;
	else if (tr > 1.0)
		tr = 1.0;

	double theta = acos(tr);

	return theta;
}

double fmft_rot_matrix_trace(const double A[3][3])
{
	double tr = A[0][0] + A[1][1] + A[2][2];

	return tr;
}

void fmft_rot_matrix_transpose(
			double AT[3][3],
			const double A[3][3])
{
	//swap rows and columns
	AT[0][0] = A[0][0];
	AT[1][0] = A[0][1];
	AT[2][0] = A[0][2];

	AT[0][1] = A[1][0];
	AT[1][1] = A[1][1];
	AT[2][1] = A[1][2];

	AT[0][2] = A[2][0];
	AT[1][2] = A[2][1];
	AT[2][2] = A[2][2];

}

void fmft_rot_matrix_mult(
			double C[3][3],
			const double A[3][3],
			const double B[3][3])
{
	C[0][0] = A[0][0] * B[0][0] + A[0][1] * B[1][0] + A[0][2] * B[2][0];
	C[0][1] = A[0][0] * B[0][1] + A[0][1] * B[1][1] + A[0][2] * B[2][1];
	C[0][2] = A[0][0] * B[0][2] + A[0][1] * B[1][2] + A[0][2] * B[2][2];

	C[1][0] = A[1][0] * B[0][0] + A[1][1] * B[1][0] + A[1][2] * B[2][0];
	C[1][1] = A[1][0] * B[0][1] + A[1][1] * B[1][1] + A[1][2] * B[2][1];
	C[1][2] = A[1][0] * B[0][2] + A[1][1] * B[1][2] + A[1][2] * B[2][2];

	C[2][0] = A[2][0] * B[0][0] + A[2][1] * B[1][0] + A[2][2] * B[2][0];
	C[2][1] = A[2][0] * B[0][1] + A[2][1] * B[1][1] + A[2][2] * B[2][1];
	C[2][2] = A[2][0] * B[0][2] + A[2][1] * B[1][2] + A[2][2] * B[2][2];
}

///////////////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.          //
///////////////////////////////////////////////////////////////////////////////
int fmft_rot_matrix_compare_fast(
			const double A[3][3],
			const double B[3][3],
			const double cos_theta_cutoff)
{
	double C00, C11, C22;
	//C=AT*B
	C00 = A[0][0] * B[0][0] + A[1][0] * B[1][0] + A[2][0] * B[2][0];
	C11 = A[0][1] * B[0][1] + A[1][1] * B[1][1] + A[2][1] * B[2][1];
	C22 = A[0][2] * B[0][2] + A[1][2] * B[1][2] + A[2][2] * B[2][2];

	double tr = (C00 + C11 + C22 - 1.0)/2.0;

	if (tr > cos_theta_cutoff)
		return 1;
	else
		return 0;
}
