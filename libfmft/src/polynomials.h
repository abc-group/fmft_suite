/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#pragma once
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "index.h"
#define MAXSLEN 200
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

/**
 * \addtogroup l_polynomials polynomials
 * \brief Computation of various polynomials.
 *
 * The polynomials coputed here are largely related to computation of
 * spherical harmonics and gaussian orbitals, and this whole module is
 * mostly an extention of the spf_coefficients stuff.
 *
 * In some cases (harmonic oscillator basis functions and spherical harmonics)
 * the computation of the corresponing entities is split into two functions
 * (first the normalization, then the actual thing). This is done to cut down
 * on the time spend in spf transforms by moving as much work as possible
 * out of the inner loops.
 * 
 * This module provides the following basic functionality:
 * - Computing harmonic oscillator (a.k.a. GTO) basis functions \f$R_{nl}(r)\f$ (done in two steps):
 * 	-# \ref fmft_poly_array_harmonic_oscillator_norm();
 * 	-# \ref fmft_poly_array_harmonic_oscillator();
 * - Computing associated Legendre polynomials  \f$ P_l^m(x) \f$:
 * 	-# \ref fmft_poly_array_associated_legendre()
 * - Computing generalized Laguerre polynomials \f$ L_{n}^{\alpha}(x)\f$:
 * 	-# \ref fmft_poly_array_laguerre()
 * - Computing mormalization coefficients for spherical harmonics:
 *   	-# \ref fmft_poly_array_spherical_harmonic_norm()
 *   	-# The actual spherical harmonics can be computed as 
 *   	   \f$Y_{l,m}(\phi, \psi) =
 *   	   	(cos(\phi) + i sin(\phi)) Y^{norm}_{l, m} P_{l, m}(cos(\psi)) \f$
 *   	   where \f$Y^{norm}_{l,m}\f$ is the normalization coefficient and  \f$P_{l,m}\f$ is the
 *   	   associated Legendre poynomial.
 *
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * 	Standard LM indexer.
 * 	\param N Highest polynomial order.
 * 	\param l In range [0, N-1]
 * 	\param m In range [-l, l]
 */
static inline int fmft_index_lm(int N, int l, int m)
{
	return N*(m + N-1) + l;
}

/**
 * 	Standard NL indexer.
 * 	\param N Highest polynomial order.
 * 	\param n In range [1, N]
 * 	\param l In range [0, n-1]
 */
static inline int fmft_index_nl(int N, int n, int l)
{
	return (N * (l) + (n-1));
}

/**
 * 	Standard indexer for Laguerre arrays.
 * 	\param N	Highest polynomial order.
 * 	\param n	n in \f$ L_{n}^{\alpha} \f$
 * 	\param alpha	\f$ \alpha \f$ in \f$ L_{n}^{\alpha} \f$
 */
static inline int fmft_index_laguerre(int N, int n, int alpha)
{
	return (n*N - n*(n - 1)/2 + alpha);
}

/**
 * 	Generates an N*N array of
 * 	normalization coefficients \f$R^{norm}_{nl}(r) = \left[ \frac{2}{\lambda^{3/2}\pi^{1/2}} \frac{(n-l-1)!}{(1/2)_n} \right]^{1/2}\f$
 * 	for harmonic oscillator basis functions \f$R_{nl}(r) = R^{norm}_{n,l} exp(-r^2/2) r^l L_{n-l-1}^{(l+1/2)}(r^2)\f$.
 * 	\param N Highest polynomial order of harmonics.
 * 	\param lambda Scaling parameter.
 * 	\return Pointer to the generated array. Individual elements of this array can be accessed with fmft_index_nl() indexer.
 */
double* fmft_poly_array_harmonic_oscillator_norm(
					const int N,
					const float lambda);

/**
 * 	Fills a pre-allocated array R with values of harmonic oscillator
 * 	basis functions \f$ R_{n,l}(r) = R^{norm}_{n,l} e^{(-r^{2}/2)} r^l L_{n-l-1}^{(l+1/2)}(r^2) \f$. computed for polynomial orders up to N at point r.
 * 	Here \f$L_n^l(x)\f$ stands for generalized Laguerre polynomials.
 * 	\param R Pointer to a pre-allocated array of size (N*N). Individual elements of this array can be acessed with fmft_index_nl() indexer.
 * 	\param R_norm Pointer to an array of normalization coefficients
 *               \f$R^{norm}_{n,l}(r) = \left[ \frac{2}{\lambda^{3/2}\pi^{1/2}} \frac{(n-l-1)!}{(1/2)_n} \right]^{1/2} \f$
 *               calculated by the function fmft_poly_array_harmonic_oscillator_norm().
 * 	\param N Highest polynomial order for which basis functions are computed.
 * 	\param r Distance from the center of the coordinate system.
 * 	\param lambda Scaling parameter.
 */
void fmft_poly_array_harmonic_oscillator(
					double* R,
					const double* R_norm,
					const int N,
					const float r,
					const float lambda);

/**
 * 	Generates an N*N array of normalization coefficients \f$ Y^{norm}_{l,m}\f$ for a set of
 * 	spherical harmonics \f$ Y_{l,m}\f$ up to polynomial order N.
 * 	\f$ Y^{norm}_{l,m} = \sqrt{{{2l+1}\over{4\pi}} {{(l-m)}\over{(l+m)}}} \f$.
 * 	Spherical harmonics can then be computed as
 * 	\f$ Y_{l,m}(\phi, \psi) =
 *   	   	(cos(\phi) + i sin(\phi)) Y^{norm}_{l, m} P_{l, m}(cos(\psi)) \f$
 * 	\param N Highest polynomial order of spherical harmonics.
 * 	\return Pointer to the generated array. Individual elements of this array can be accessed with fmft_index_lm() indexer.
 */
double* fmft_poly_array_spherical_harmonic_norm(const int N);

/**
 * 	Fills a pre-allocated array P with values of associated Legandre
 * 	polynomials \f$ P_l^m(x) \f$ calculated up to order N for a specific parameter value x.
 * 	\param P Pointer to a pre-allocated array of size (N*(2*N - 1)). Individual elements of this array can be accessed with fmft_index_lm() indexer.
 * 	\param N Highest polynomial order of spherical harmonics.
 * 	\param x Associated Legendre polynomials' parameter.
 */
void fmft_poly_array_associated_legendre(
					double* P,
					const int N,
					const double x);

/**
 * 	Generates an N*N array of generalized Laguree polynomial  \f$ L^{\alpha}_{n}(x) \f$ values calculated up to order N
 * 	for parameter x. The values are computed using a recurrence formula:
 * 	\f$ L^{\alpha}_{k + 1}(x) = \frac{(2k + 1 + \alpha - x)L^{\alpha}_k(x) - (k + \alpha) L^{\alpha}_{k - 1}(x)}{k + 1} \f$,
 * 	\f$ L^{\alpha}_0(x) = 1 \f$, \f$ L^{\alpha}_1(x) = 1 + \alpha - x \f$.
 * 	\param  N Highest order for which the polynomials will be computed.
 * 	\param x Laguerre polynomials' parameter.
 * 	\param alpha_min ???.
 * 	\return Pointer to the generated array. Individual elements of this array can be accessed with index_Laguerre() indexer.
 */
double* fmft_poly_array_laguerre(
				const int N,
				const float x,
				const float alpha_min);

#ifdef __cplusplus
}
#endif

/** @}*/
