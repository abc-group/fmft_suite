/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#pragma once
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "grid.h"
#include "index.h"
#include "polynomials.h"
#include "simple_functions.h"

/**
 * \addtogroup l_coefficients spf_coefficients
 * \brief Spherical polar Fourier transforms and the like.
 *
 * Everything here revolves around the so-called Sphrerical Polar Fourier transforms -
 * that is, computing the expansion coefficients of a 3D density functions over
 * a basis composed of spherical and radial harmonics:
 *
 * \f[ A(\rho, \theta, \phi) = \sum_{n=1}^{N}\sum_{l=0}^{n-1}\sum_{m=-l}^{l}a_{nlm} R_{nl}(\rho) Y_{lm}(\theta, \phi) \f]
 *
 * Here
 * \f$ A(\rho, \theta, \phi) \f$ is some density function,
 * \f$ R_{nl}(\rho) \f$ are the radial harmonics (in this case - Gaussian orbitals),
 * \f$ Y_{lm}(\theta, \phi) \f$ are the standatd spherical harmonics and
 * \f$ a_{nlm} \f$ are the expansion coefficients, sometimes referred to as the SPF coefficients.
 *
 * \f$ R_{nl} \f$ and \f$ Y_{lm} \f$ are given by the following expressions:
 * \f[ R_{nl}(\rho) = \left[ \frac{2}{\lambda^{3/2}\pi^{1/2}} \frac{(n-l-1)!}{(1/2)_n} \right]^{1/2} exp(-r^2/2) r^l L_{n-l-1}^{(l+1/2)}(r^2) \f]
 * \f[ Y_{lm}(\theta,\phi) = \frac{1}{2\pi} e^{im\phi} \sqrt{\frac{(l+1)(l-m)!}{2 (l+m)!}} P_m^l(\theta) \f],
 * where \f$ L_{n}^{alpha}(x) \f$ are the generalized Laguerre plynomials, and \f$ P_{lm} \f$ are the associated Legendre polynomials.
 *
 * Since the basis set is orthonormal, the expansion coefficients can computed as overlap integrals:
 * \f[ a_{nlm} = \int A(\rho, \theta, \phi) R_{nl}(\rho) Y_{lm}(\theta, \phi) dV \f]
 *
 * In practice, the functions here deal with density functions defined on 3D grids proveded by the Density grid module.
 *
 *
 * This module provides the following basic functionality:
 * - Computing expansion coefficients sets from 3D density grids and restoring grids back:
 * 	-# \ref fmft_coeffset_from_grid();
 * 	-# \ref fmft_coeffset_to_grid();
 * - Reading and writing coefficient sets from/to files:
 * 	-# \ref fmft_coeffset_read()
 * 	-# \ref fmft_coeffset_write()
 * - Some basic memory management:
 * 	-# \ref fmft_coeffset_stack_create()
 * 	-# \ref fmft_coeffset_stack_free()
 *
 * Quite often, it is convinient to describe some 3D object with a set of
 * density functions that correspond to its different properties
 * (say, steric volume, charge distribution and electrostatic potential of a molecule).
 *
 * The module provides the fmft_coeffset_stack data structure
 * and some functions that allow to conviniently manipulate such "property stacks" in one shot:
 * - Computing "stacks" of expansion coefficients sets from "stacks" of 3D density grids:
 * 	-# \ref fmft_coeffset_stack_from_grids();
 * - Reading and writing coefficient set stacks from/to files:
 * 	-# \ref fmft_coeffset_stack_read()
 * 	-# \ref fmft_coeffset_stack_write()
 * - Getting low-order coeffset stacks from high order ones (when a coarser representation is desirable):
 *   	-# \ref fmft_coeffset_stack_get_low_order()
 * - Scaling the coefficient sets in a coefficient stack (needed to weight the contributions of different properties whe computing sums of correlations):
 *	-# \ref fmft_coeffset_stack_scale()
 *
 * For more advanced operations on coefficients, such as rotation and translation,
 * see \ref l_rotation and \ref l_translation modules.
 *
 * For correlation of coefficient stacks, whcih provides is a very fast method
 * for rigid body search and 3D pattern recognition, see the \ref l_correlate module.
 *
 * @{
 */


/**
 * 	A set of decomposition coefficients of a 3D density function
 * 	in a basis of spherical and radial harmonics.
 * 	Referred to as Spherical polar fourier (SPF) coefficients.
 * 	This structure is used for storing spf coefficients
 * 	calculated up to polynomial order N.
 * 	Individual spf coefficients can be accessed
 * 	using fmft_index_coeffset() indexing function.
 */
struct fmft_coeffset
{
	int N; 		/**< Highest polynomial order of the coefficients (Depth of SPF transform).*/
	float lambda; 	/**< Lambda scaling parameter of the radial harmonics that were used for calculation.*/
	struct fmft_vector_3f origin; /**< origin of the spherical coordinate system
					in which the decomposition was performed. */
	double *re; 	/**< Real parts of the coefficients. Array of size N*N*(2*N - 1)*/
	double *im; 	/**< Imaginary parts of the coefficients. Array of size N*N*(2*N - 1)*/
};

/**
 * 	A "stack" of decomposition coefficient sets
 * 	computed from a set of density functions using the same
 * 	spherical coordinate system, lambda scaling factor
 * 	and highest polynomial order of basis functions.
 * 	Typically used to store the coefficients computed
 * 	for the functions describing multiple 3D properties
 * 	of the same object (e.g, steric density, charge distribution
 * 	and electrostatic potential).
 *
 * 	Individual coefficients can be accessed using the fmft_index_coeffset_stack() indexer.
 */
struct fmft_coeffset_stack
{
	int N; 		/**< Highest polynomial order of the coefficients (Depth of SPF transform). */
	int count; 	/**< Number of coefficient sets in a stack. */
	float lambda; 	/**< Lambda scaling parameter of the radial harmonics that were used for calculation.*/
	struct fmft_vector_3f origin; /**< origin of the spherical coordinate system
					in which the decomposition was performed. */
	double *re; 	/**< Real parts of the coefficients. Array of size N*N*(2*N - 1)*/
	double *im; 	/**< Imaginary parts of the coefficients. Array of size N*N*(2*N - 1)*/
};

/**
 *	A set of weights for the coefficient sets in the fmft_coeffset_stack objects.
 *	Can be applied to the stack using the fmft_coeffset_stack_scale() function
 */
struct fmft_weightset
{
	int n;		/**< Number of weights in a set */
	int label;	/**< Index of weight set*/
	double* vals;	/**< A pointer to an array of weight values */
};

/**
 *	Holds several weight sets;
 */
struct fmft_weightset_list
{
	int count;	/**< Number of weight sets */
	struct fmft_weightset** wset; /**< A pointer to an array of weight sets */
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * 	Indexer for the \ref fmft_coeffset.re and \ref fmft_coeffset.im fields
 * 	of the fmft_coeffset data structure.
 * 	\param N	Highest polynomial order, for which the coefficients were calculated.
 * 	\param n	In range [1, N].
 * 	\param l	In range [0, n-1].
 * 	\param m	In range [-l, l].
 */
static inline int fmft_index_coeffset(int N, int n, int l, int m)
{
	return N*(N*(m + (N - 1)) + l) + n - 1;
}

/**
 * 	Indexer for the \ref fmft_coeffset_stack.re and \ref fmft_coeffset_stack.im fields
 * 	of the fmft_coeffset_stack data structure.
 * 	\param N	Highest polynomial order, for which the coefficients were calculated
 * 	\param nsets	The total number of coefficient sets in a stack.
 * 	\param p	Coefficient set index. In range [0, nsets-1].
 * 	\param n	In range [1, N].
 * 	\param l	In range [0, n-1].
 * 	\param m	In range [-l, l].
 */
static inline int fmft_index_coeffset_stack(int N, int nsets, int p, int n, int l, int m)
{
	return nsets*(N*(N*(m + (N - 1)) + l) + n - 1) + p;
}


/**
 *	Create an instance of fmft_coeffset object
 *	capable of storing spf coeffitients up to polynomial order N.
 *	\param N	Highest polynomial order, for which the coefficients are stored.
 *	\return Pointer to a fmft_coeffset struct.
 */
struct fmft_coeffset* fmft_coeffset_create(int N);

/**
 *	Create an instance of fmft_coeffset_stack object
 *	capable of storing spf coeffitients computed up to polynomial order N
 *	for count coefficients sets.
 *	\param N	Highest polynomial order, for which the coefficients are stored.
 *	\param count	The number of coefficient sets in a stack
 *	\return Pointer to a fmft_coeffset struct.
 */
struct fmft_coeffset_stack* fmft_coeffset_stack_create(int N, int count);

/**
 *	Deallocates an instance of fmft_coeffset;
 */
void fmft_coeffset_free(struct fmft_coeffset* coeff);


/**
 *	Deallocates an instance of fmft_coeffset_stack;
 */
void fmft_coeffset_stack_free(struct fmft_coeffset_stack* coeff_st);


/**
 *	Create an instance of fmft_coeffset and fill it
 *	according to the contents of file filename.
 *	See fmft_coeffset_write() for the description of the file format.
 *	\param filename	Path to the file containing coefficients.
 *	\return A pointer to the resultsing fmft_coeffset.
 */
struct fmft_coeffset* fmft_coeffset_read(const char *filename);

/**
 * 	Writes a set of coefficients held by an instance of fmft_coeffset to a file
 * 	The first line of the file will contain the largest polynomial order N,
 * 	The second line will contain the value of the lambda parameter.
 * 	The third line will contain the origin of the spherical coordinate system.
 * 	All consecutive lines contain real and imaginary parts of
 * 	the appropriate spf coefficients.
 * 	(i.e 4th line holds real and imaginary parts of n=1, l=0, m=0 element,
 * 	5th line - n=2, l=0, m=0. 6th - n=2, l=1, m=-1 and so on)
 * 	\param coeff	Pointer to a fmft_coeffset structure.
 * 	\param filename	Path to a file to be created.
 */
void fmft_coeffset_write(const struct fmft_coeffset* coeff, const char *filename);

/**
 *	Create an instance of fmft_coeffset_stack an fill it
 *	according to the contents of file filename.
 *	See fmft_coeffset_stack_write() for the description of the file format.
 *	\param filename	Path to the file containing coefficients.
 *	\return A pointer to the resultsing fmft_coeffset_stack.
 */
struct fmft_coeffset_stack* fmft_coeffset_stack_read(const char *filename);

/**
 * 	Writes a stack of coefficient sets held by an instance of fmft_coeffset_stack to a file.
 * 	The first line of the file will contain
 * 	the largest polynomial order N and the number of coefficient sets,
 * 	The second line will contain the value of the lambda parameter.
 * 	The third line will contain the origin of the spherical coordinate system.
 * 	All consecutive lines contain real and imaginary parts of
 * 	the appropriate spf coefficients.
 * 	(i.e 4th line holds real and imaginary parts of n=1, l=0, m=0 element
 * 	of the first coefficien set,
 * 	5th line - n=2, l=0, m=0. 6th - n=2, l=1, m=-1 and so on)
 * 	\param coeff_st	Pointer to a fmft_coeffset_stack structure.
 * 	\param filename	Path to a file to be created.
 */
void fmft_coeffset_stack_write(const struct fmft_coeffset_stack* coeff_st, const char *filename);

/**
 * 	Take the coefficients in a coefficient stack up to order polynomial order N
 * 	and return them in a new instance of fmft_coeffset_stack object.
 * 	N is supposed to be lower than the original polynomial order of the stack.
 * 	\param coeff_st	Original coefficient stack
 * 	\param N	The new highest pomlynomial order.
 * 	\return A new instance of fmft_coeffset_stack containing coefficients up to polynomial order N.
 */
struct fmft_coeffset_stack* fmft_coeffset_stack_get_low_order(const struct fmft_coeffset_stack* coeff_st, const int N);

/**
 * 	Scale the coefficient sets in a stack according to a set of weights and return the result.
 * 	All coefficients in a single set are scaled by the same factor.
 *
 * 	\param coeff_st	Input stack of coefficieint sets.
 * 	\param wset	A set of weights
 * 	\return A scaled stack of coefficient sets.
 */
struct fmft_coeffset_stack* fmft_coeffset_stack_scale(const struct fmft_coeffset_stack* coeff_st, struct fmft_weightset* wset);


/**
 *	Compute the (spf) expansion coefficients over a set of basis functions
 *	composed of spherical and radial harmonics
 *	for a density function defined on a grid
 *	(that is, perform a Spherical Polar Fourier Transform).
 *	\param g	3D grid representation of the density function.
 *	\param N	Highest polynomial order for which the coefficients are to be computed.
 *	\param lambda	The lambda scaling parameter of the Radial harmonics (Here, Gaussian orbitals)
 *	\param origin	Origin of the spherical coordinate system in which the decomposition is performed.
 *			If NULL is passed, it will be set to the geometric center of the grid.
 *	\param r_cutoff	The radius from the origin, beyond which the density function values are ignored.
 *			(Helps to speed up the calculations when doing multiple origin decompostions
 *			of very large density grids. The parameter is ignored if set to a negative value.)
 *	\return Pointer to an instance of fmft_coeffset containing the resulting coefficients.
 */
struct fmft_coeffset* fmft_coeffset_from_grid(
					const struct fmft_grid_3D* g,
					const int N,
					const float lambda,
					const struct fmft_vector_3f* origin,
					const float r_cutoff);

/**
 *	Compute the (spf) expansion coefficients over a set of basis functions
 *	composed of spherical and radial harmonics
 *	for a stack of density functions defined on grids.
 *	(that is, perform a series of Spherical Polar Fourier Transforms).
 *	\param gs	A list of 3D grids representating the density functions of interest.
 *	\param N	Highest polynomial order for which the coefficients are to be computed.
 *	\param lambda	The lambda scaling parameter of the Radial harmonics (Here, Gaussian orbitals)
 *	\param origin	Origin of the spherical coordinate system in which the decomposition is performed.
 *			If NULL is passed, it will be set to the geometric center of the first grid.
 *	\param r_cutoff	The radius from the origin, beyond which the density function values are ignored.
 *			(Helps to speed up the calculations when doing multiple origin decompostions
 *			of very large density grids. The parameter is ignored if set to a negative value.)
 *	\return Pointer to an instance of fmft_coeffset_stack containing a "stack" of the resulting coefficient sets.
 */
struct fmft_coeffset_stack* fmft_coeffset_stack_from_grids(
					const struct fmft_grid_3D_list* gs,
					const int N,
					const float lambda,
					const struct fmft_vector_3f* origin,
					const float r_cutoff);

/**
 *	Reconstruct a 3D density function based on a set of expansion coefficients.
 *	\param coeff		Coefficient set that will be used for reconstruction.
 *	\param grid_shape	Resulting grid shape.
 *	\param grid_delta	Resulting grid cell size.
 *	\param grid_origin	Resulting grid origin. If NULL is passed, will be set to a value
 *				that ensures that the geometric center of the grid corresponds
 *				to the fmft_coeffset origin.
 *	\return An instance of the fmft_grid_3D object containing the grid representation of the recuonstructed density function.
 */
struct fmft_grid_3D* fmft_coeffset_to_grid(
			const struct fmft_coeffset* coeff,
			const struct fmft_vector_3i* grid_shape,
			const struct fmft_vector_3f* grid_delta,
			const struct fmft_vector_3f* grid_origin);



struct fmft_weightset* fmft_weightset_create(int n);

struct fmft_weightset_list* fmft_weightset_list_create(int count);

void fmft_weightset_free(struct fmft_weightset* wset);

void fmft_weightset_list_free(struct fmft_weightset_list* wlist);

struct fmft_weightset_list* fmft_weightset_list_read(char* path);

#ifdef __cplusplus
}
#endif

///////////////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.          //
///////////////////////////////////////////////////////////////////////////////

/** @}*/
