/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#ifndef VECTOR_MATRIX_H_
#define VECTOR_MATRIX_H_

#include <stdlib.h>
#include <math.h>

/**
 * \addtogroup l_vector_matrix vector_matrix
 * \brief Vector-matrix and Matrix-matrix operations.
 *
 * Some useful linear algebra routines,
 * mostly related to rotation matrices.
 *
 * @{
 */


/**
 * Simple 3D vector,
 * Integer coordinates.
 */
struct fmft_vector_3i
{
	int X;
	int Y;
	int Z;
};

/**
 * Simple 3D vector,
 * Floating point coordinates.
 */
struct fmft_vector_3f
{
	float X;
	float Y;
	float Z;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
	Calculates the elements of the rotation matrix
	for Euler angles alpha, beta, gamma (Z-Y-Z rotation)
	for normal rotation.
	\param rm  Rotation matrix to be filled
	\param alpha	Euler angle (radians)
	\param beta	Euler angle (radians)
	\param gamma	Euler angle (radians)
*/
void fmft_rot_matrix_fill_active(
			double rm[3][3],
			double alpha,
			double beta,
			double gamma);

/**
	Calculates the elements of the rotation matrix
	for Euler angles alpha, beta, gamma (Z-Y-Z rotation)
	for "passive" rotation
	(this matrix will actually be a transposed "active" rotation matrix).
	\param rm  Rotation matrix to be filled
	\param alpha	Euler angle (radians)
	\param beta	Euler angle (radians)
	\param gamma	Euler angle (radians)
*/
void fmft_rot_matrix_fill_passive(
			double rm[3][3],
			double alpha,
			double beta,
			double gamma);

/**
 * 	Checks whether the specified rotation matrices
 * 	correspond to "close" rotations.
 * 	By "close" we mean that that there exists a rotation matrix C,
 * 	for which B = AC, and C corresponds to a rotation about a certain
 * 	axis by angle not greater then theta_cutoff
 *	\param A	Rotation matrix A
 *	\param B	Rotation matrix B
 *	\param theta_cutoff Threshold;
 *	\return 1 if rotations are "close" and 0 otherwise
 */
int fmft_rot_matrix_compare(
			const double A[3][3],
			const double B[3][3],
			const double theta_cutoff);

/**
 * 	Get the theta angle of the axis-angle
 * 	representation of the rotation given by
 * 	the specified rotation matrix.
 * 	\param A	Rotation matrix.
 * 	\return Corresponding rotation angle.
 */
double fmft_rot_matrix_get_theta(const double A[3][3]);

/**
 * 	Compute the trace of 3x3 matrix.
 * 	\param A	3x3 matrix.
 * 	\return Trace.
 */
double fmft_rot_matrix_trace(const double A[3][3]);

/**
 * 	Transpose a 3x3 matrix.
 * 	\param AT	Preallocated matrix that will store the result.
 * 	\param A	Input matrix.
 */
void fmft_rot_matrix_transpose(
			double AT[3][3],
			const double A[3][3]);

/**
 * 	Get the Result of multiplication of matrix B by matrix A.
 * 	\param C	Resulting matrix.
 * 	\param A	3x3 matrix.
 * 	\param B	3x3 matrix.
 */
void fmft_rot_matrix_mult(
			double C[3][3],
			const double A[3][3],
			const double B[3][3]);

///////////////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.          //
///////////////////////////////////////////////////////////////////////////////

/**
 * 	A faster version of fmft_rot_matrix_compare().
 * 	Takes in the cosine of the cutoff angle to cut down on calculations.
 * 	\param A	Rotation matrix B
 *	\param B	Rotation matrix B
 *	\param cos_theta_cutoff Cosine of the threshold angle;
 *	\return 1 if rotations are "close" and 0 otherwise.
 */
int fmft_rot_matrix_compare_fast(
			const double A[3][3],
			const double B[3][3],
			const double cos_theta_cutoff);

#ifdef __cplusplus
}
#endif

/** @}*/
#endif /* VECTOR_MATRIX_H_ */
