/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#pragma once
#include <string.h>
#include <math.h>
#include "index.h"
#include "spf_coefficients.h"
//#define DFOLDER "data"
//#define DFILE "wigner_d"
//#define DFILE_NEW "wigner_d_new"
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

/**
 * \addtogroup l_rotation rotation_matrix
 * \brief Rotation of expansion coefficients.
 *
 * This module deals with various matrices that can be used to rotate (spf) expansion coefficients.
 * When we say "rotating coefficient" we mean obtaining a set of coefficients that
 * corresponds to rigid body rotation of the density function that was used to
 * obtain the original coefficient set.
 *
 * The major players here are the Wigner d-matrices \f$ d_{mm'}^{l}(\beta) \f$ and Wigner D-matrices \f$ D_{mm'}^{l}(\alpha, \beta, \gamma) \f$,
 * with the latter related to the former via the following formula:
 * \f[D_{mm'}^{l}(\alpha, \beta, \gamma) = e^{-im\alpha} d_{mm'}^{l}(\beta) e^{-im'\gamma}\f]
 *
 * When computing correlations on rotation groups, it is also convinient to have access to
 * multiple Wigner d-matrices precomputed for multiple values of the Euler beta angle
 * sampled in uniform steps. The fmft_wigner_d_set data structure and the related functions
 * are needed exaclty for that.
 *
 * This module provides the following basic functionality:
 * - Computing Wigner D-matrices, d-matrices and sets thereof:
 * 	-# \ref fmft_wigner_D_matrix_create();
 * 	-# \ref fmft_wigner_d_matrix_create();
 * 	-# \ref fmft_wigner_d_set_create();
 * - Rotating spf expansion coefficients using a precomputed Wigner D-matrix:
 * 	-# \ref fmft_coeffset_rotate()
 *
 * @{
 */

/**
 * 	Wigner d-matrix elements, calculated for a single value
 * 	of Euler beta angle up to polynomial order L.
 */
struct fmft_wigner_d_matrix
{
    int L;		/**< Highest polynomial order, for which matrix elements are stored. */
    double *data;	/**< array of size (L+1)*(2*L + 1)*(2*L + 1). */
};

/**
 * 	Wigner d-matrix elements, calculated for num_angles values
 * 	of the Euler beta angle up to to polynomial order L.
 */
struct fmft_wigner_d_set
{
    int L;		/**< Highest polynomial order, for which matrix elements are stored.*/
    int num_angles;	/**< Number of different Euler beta angle values, for which matrix elements are stored*/
    double *data;	/**< array of size num_angles*(L+1)*(2*L + 1)*(2*L + 1). */
};

/**
 * 	Wigner D-matrix (which serves as rotation matrix in SPF space).
 */
struct fmft_wigner_D_matrix
{
	int L;		/**< Highest polynomial order, for which matrix elements are stored. */
	double *re;	/**< Real parts of matrix elements. Array of size (L+1)*(2*L + 1)*(2*L + 1)*/
	double *im;	/**< Imaginary parts of matrix elements. Array of size (L+1)*(2*L + 1)*(2*L + 1)*/
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * 	Indexer for the fmft_wigner_d_matrix.data field.
 * 	\param L	Highest polynomial order, for which the matrix elements are stored.
 * 	\param l	In range [0, L].
 * 	\param m	In range [-l, l].
 * 	\param m1	In range [-l, l]
 */
static inline int fmft_index_d_array(int L, int l, int m, int m1)
{
	return (2*L + 1)*((2*L + 1)*l + (L + m)) + (L + m1);
}

/**
 * 	Indexer for fmft_wigner_d_set.data field.
 * 	\param L	Highest polynomial order, for which the matrices' elements are stored.
 * 	\param i	Index of matrix, calculated for a specific value of Euler beta angle, in range [0, num_angles-1].
 * 	\param l	In range [0, L].
 * 	\param m	In range [-l, l].
 * 	\param m1	In range [-l, l]
 */
static inline int fmft_index_d_set(int L, int i, int l, int m, int m1)
{
	//return (2*L + 1)*((2*L + 1)*((L+1)*b + l) + (L + m)) + (L + m1);
	return (L+1)*((2*L+1)*((2*L + 1)*i + (L + m1)) + (L + m)) + l;
}

/**
 * 	Indexer for fmft_wigner_D_matrix.re and fmft_wigner_D_matrix.im fields
 * 	of the fmft_wigner_D_matrix data structure.
 * 	\param L	Highest polynomial order, for which the matrix elements are stored.
 * 	\param l	In range [0, L].
 * 	\param m	In range [-l, l].
 * 	\param m1	In range [-l, l]
 *
 */
static inline int fmft_index_D_array(int L, int l, int m, int m1)
{
	return (2*L + 1)*((2*L + 1)*l + (L + m)) + (L + m1);
}


/**
 * 	Create an instance of fmft_wigner_d_matrix
 * 	storing matrix elements up to polynomial order N
 * 	corresponding to Euler beta angle.
 * 	\param L		Highest polynomial order, for which matrix elements are computed.
 * 	\param beta		Euler beta angle value in radian.
 * 	\return Initialized Wigner d-matrix.
 */
struct fmft_wigner_d_matrix* fmft_wigner_d_matrix_create(const int L, const double beta);

/**
 * 	Create an instance of fmft_wigner_d_set containing elements of
 * 	Wigner d-functions calculated up to polinomial order L
 * 	for num_angles values of beta angle, covering the range [0, pi] in
 * 	uniform steps.
 * 	\param num_angles 	Number of beta angle values, for which the d-functions will be calculated.
 * 	\param L		Highest polynomial order, for which matrix elements will be calculated.
 * 	\return Initialized set of wigner d-matrices.
 */
struct fmft_wigner_d_set* fmft_wigner_d_set_create(const int num_angles, const int L);

/**
 * 	Create an instance of fmft_wigner_D_matrix corresponding to
 * 	rotation by Euler angles alpha, beta, gamma.
 * 	Matrix elements are computed up to polynomial order L.
 * 	\param L 		Highest polynomial order, for which matrix elements will be calculated.
 * 	\param alpha		Alpha Euler angle in radian.
 * 	\param beta		Beta Euler angle in radian.
 * 	\param gamma		Gamma Euler angle in radian.
 * 	\return A pointer to the resulting matrix.
 */
struct fmft_wigner_D_matrix* fmft_wigner_D_matrix_create(const int L, const double alpha, const double beta, const double gamma);

/**
 * 	Deallocates an instance of fmft_wigner_d_matrix.
 * 	\param d		Pointer to an instance of fmft_wigner_d_matrix.
 */
void fmft_wigner_d_matrix_free(struct fmft_wigner_d_matrix* d);

/**
 * 	Deallocates an instance of fmft_wigner_d_set struct.
 * 	\param md		Pointer to an instance of fmft_wigner_d_set struct.
 */
void fmft_wigner_d_set_free(struct fmft_wigner_d_set* md);

/**
 * 	Deallocates an instance of fmft_wigner_D_matrix struct.
 * 	\param D		Pointer to an instance of D_array struct.
 */
void fmft_wigner_D_matrix_free(struct fmft_wigner_D_matrix* D);

/**
 * 	Perform a "rotation" of the expansion coefficient set by
 * 	multiplying it with a Wigner D-matrix.
 * 	\param coeffs	Pointer to an instance of the fmft_coeffset struct, storing a set coefficients to be rotated.
 * 	\param D		A Wigner D-matrix struct.
 * 	\return Pointer to the instance of fmft_coeffset containing the "rotated" coefficients.
 */
struct fmft_coeffset* fmft_coeffset_rotate(const struct fmft_coeffset* coeffs, const struct fmft_wigner_D_matrix* D);

#ifdef __cplusplus
}
#endif

///////////////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.          //
///////////////////////////////////////////////////////////////////////////////

#ifndef DOXYGEN_SHOULD_SKIP_THIS
///////////////////////////////////////////////////////////////////////////////
// Should get rid of this.                                                   //
///////////////////////////////////////////////////////////////////////////////
#endif // DOXYGEN_SHOULD_SKIP_THIS

/** @}*/
