/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <math.h>
#include "vector_matrix.h"
#include "simple_functions.h"

/**
 * \addtogroup l_grid grid
 * \brief Density grids and functions to manage them.
 *
 * This module provides the following basic functionality:
 * - Creation of emtpty density grids and grid lists:
 *	-# \ref fmft_grid_3D_create();
 *	-# \ref fmft_grid_3D_list_create();
 * - Density grid reading/writing in DX format:
 * 	-# \ref fmft_grid_3D_read();
 * 	-# \ref fmft_grid_3D_write();
 * - Reading grid lists from files
 *	-# \ref fmft_grid_3D_list_read()
 *
 * @{
 */

/**
 * 3D density grid
 */
struct fmft_grid_3D
{
	struct fmft_vector_3i shape; /**< Grid shape. */
	struct fmft_vector_3f delta; /**< Grid cell size. */
	struct fmft_vector_3f origin; /**< Grid origin. */
	float* data; /**< Field values at grid points.
			  Use fmft_index_grid_3D() to access individual values.*/
};

/**
 * A list of 3D density grids
 *
 */
struct fmft_grid_3D_list
{
    int count; /**< Number of grids */
    struct fmft_grid_3D** A; /**< Array of fmft_grid_3D objects */
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Indexer for \ref fmft_grid_3D.data field of the fmft_grid_3D structure.
 * \param shape Grid dimensions.
 * \param x X coordinate.
 * \param y Y coordinate.
 * \param z Z coordinate.
 */
inline int fmft_index_grid_3D(struct fmft_vector_3i shape, int x, int y, int z)
{
	return z + shape.Z * (y + (shape.Y * x));
}

/**
 *	Create a fmft_grid_3D object for storing a Cartesian grid
 *	with dimensions	NX x NY x NZ.
 *	The value of the shape field of the grid is set accordingly.
 *	The value of the delta field is set to a unit vector.
 *	The value of the origin field is set to a zero vector.
 *	\param NX Grid size along X axis.
 *	\param NY Grid size along Y axis.
 *	\param NZ Grid size along Z axis.
 *	\return Pointer to the created grid.
 */
struct fmft_grid_3D* fmft_grid_3D_create(
				const int NX,
				const int NY,
				const int NZ);

/**
 *	Free a fmft_grid_3D object.
 *	\param g A pointer to an object to be deallocated.
 */
void fmft_grid_3D_free(struct fmft_grid_3D *g);

/**
 *	Create a grid list object.
 *	\param count The number of grids in a list.
 *	\return Pointer to the allocated fmft_grid_3D_list object.
 */
struct fmft_grid_3D_list* fmft_grid_3D_list_create(
				const int count);

/**
 *	Free a fmft_grid_3D object
 *	and all its contents.
 *	\param gl Object to be deallocated.
 */
void fmft_grid_3D_list_free(struct fmft_grid_3D_list *gl);

/**
 *	Create a density grid object and fill it according
 *	to the contents of the file specified by the
 *	filename parameter.
 *	\param filename Path to a standard dx file.
 *	\return Pointer to the initialized fmft_grid_3D object.
 */
struct fmft_grid_3D* fmft_grid_3D_read(const char* filename);

/**
 *	Create a grid list object and fill it according
 *	to the contents of the file specified by the
 *	filename parameter.
 *	\param filename Path to a file containing information about
 *	grids. The first line of the file contains a single integer -
 *	the number of grids in a list. All consequent
 *	lines contain paths to the appropriate dx files.
 *	\return Pointer to the initialized instance of fmft_grid_3D_list.
 */
struct fmft_grid_3D_list* fmft_grid_3D_list_read(const char* filename);

/**
 *	Create a .dx file at the location determined
 *	by the filename parameter and write the contents of grid struct
 *	into it.
 *	\param filename Path to a file to be created
 *	\param g Pointer to an instance of fmft_grid_3D
 */
void fmft_grid_3D_write(const struct fmft_grid_3D* g, const char* filename);

#ifdef __cplusplus
}
#endif

//////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present. //
//////////////////////////////////////////////////////////////////////

#ifndef DOXYGEN_SHOULD_SKIP_THIS
#endif // DOXYGEN_SHOULD_SKIP_THIS
/** @}*/
