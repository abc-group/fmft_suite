/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include "correlate.h"
#ifdef _MPI_
#include <mpi.h>
#endif // _MPI_

////////////////////////////////////////
// Some sorting utilities             //
////////////////////////////////////////
class fmft_cmp_real
{
	public:
	__real* arr;
	bool operator()(int i, int j) {
		return (arr[i] < arr[j]);
	}
};

class fmft_cmp_ftresults
{
	public:
	bool operator()(const struct fmft_result &i, const struct fmft_result &j) {
		return (i.val < j.val);
	}
};

inline void swap(double *array, int i, int j)
{
	double temp = array[j];
	array[j] = array[i];
	array[i] = temp;
}

void fmft_results_sort(struct fmft_result* scores, int num_best, int num_all)
{
	fmft_cmp_ftresults mcmp;
	std::partial_sort(scores, scores + num_best, scores + num_all, mcmp);
}

////////////////////////////////////////
// Core fmft correlation code         //
////////////////////////////////////////

struct fft_data
{
	int FFT_size;
	__fftw_complex* W;
	__real* E;
	__fftw_plan plan;
};

/**
 * 	Iteratively find minima in the FFT_ARRAYS->E array and clear the surrounding region
 * 	(i.e exclude all the orientations that are closer then theta on S2 and SO(3) to the given minima from further consideration)
 * 	\param FFT_arrays	Pointer to an instance of fft_data struct, containing the E array.
 *        			E arrays contains the scores for all the orientations of receptor and ligand
 */
void fmft_filter_results(
		struct fft_data* FFT_arrays,
		struct fmft_result *results,
		const int results_per_z,
		const double z,
		const double theta);


static inline int index_loop2(int S, int s, int t, int m, int j, int l)
{
	//return (N*(N*(N*((2*N-1)*(N - 1 + s) + (N - 1 + t)) + m) + j) + l);
	int HS = S / 2;
	return ((HS+1)*((HS+1)*((HS+1)*((S)*(HS + s) + (HS + t)) + m) + j) + l);
}

static inline int index_loop3(int S, int beta1, int s, int t, int m, int j)
{
	//return (N*(N*((2*N - 1)*((2*N - 1)*beta1 + (N - 1 + s)) + (N - 1 + t)) + m) + j);
	int HS=S/2;
	return ((HS+1/*?*/)*((HS+1)*((S)*((S)*beta1 + (HS + s)) + (HS + t)) + m) + j);
}

/*
 * Index for the last stage of fft array filling routine.
 * The array is supposed to be used with complex-to-real DFT
 */
static inline int index_loop4(int S, int b, int b1, int i, int j, int k)
{
	int HS = S / 2;
	return (HS+1)*((S)*((S)*((HS+1)*b + b1) + i) + j) + k;
}


static __fftw_plan plan_3d_c2r_dft(struct fft_data* FFT_arrays)
{
	int FFT_halfsize = FFT_arrays->FFT_size/2;
	int rank = 3;
	int idim[3];
	idim[0] = FFT_arrays->FFT_size;
	idim[1] = FFT_arrays->FFT_size;
	idim[2] = FFT_halfsize + 1;
	int odim[3];
	odim[0] = FFT_arrays->FFT_size;
	odim[1] = FFT_arrays->FFT_size;
	odim[2] = FFT_arrays->FFT_size;
	int howmany = (FFT_halfsize + 1) * (FFT_halfsize + 1);
	int idist = idim[0] * idim[1] * idim[2];
	int odist = odim[0] * odim[1] * odim[2];
	int istride = 1;
	int ostride = 1;
	int *inembed = idim;
	int *onembed = odim;
	return _fftw_plan_many_dft_c2r(
				rank, odim, howmany,
				FFT_arrays->W, inembed,
				istride, idist,
				FFT_arrays->E, onembed,
				ostride, odist,
				FFTW_ESTIMATE);
}

static struct fft_data* prepare_fft_arrays(const int FFT_size)
{
	int FFT_halfsize = FFT_size / 2;
	struct fft_data* FFT_arrays;
	FFT_arrays = (struct fft_data*) calloc (1, sizeof(struct fft_data));
	FFT_arrays->FFT_size = FFT_size;

 	/*** ALLOCATE THE SCORING ARRAY ***/
	FFT_arrays->E = (__real*) _fftw_malloc(
					(FFT_halfsize + 1) *
					(FFT_halfsize + 1) *
					FFT_size *
					FFT_size *
					(FFT_halfsize + 1) * 2 * sizeof(__real));
	if (FFT_arrays->E == NULL) {
		printf("Error allocating memory for E array\n");
		exit(EXIT_FAILURE);
	}

	/*** ALLOCATE TEMPORARY ARRAY ***/
	FFT_arrays->W = (__fftw_complex*) _fftw_malloc(
						(FFT_halfsize + 1) *
						(FFT_halfsize + 1) *
						FFT_size *
						FFT_size *
						(FFT_halfsize + 1) * sizeof(__fftw_complex));
	if (FFT_arrays->W == NULL) {
		printf("Error allocating memory for W1 array\n");
		exit(EXIT_FAILURE);
	}

	/*** PREPARE FFT PLAN ***/
	FFT_arrays->plan = plan_3d_c2r_dft(FFT_arrays);

	return FFT_arrays;
}

static void fill_fft_array(
		const struct fmft_coeffset_stack* coeff_st_a,
		const struct fmft_coeffset_stack* coeff_st_b,
		struct fft_data* FFT_arrays,
		const double *T)
{
	if (coeff_st_a->count != coeff_st_b->count) {
		fprintf(stderr, "Number of coefficient sets in the correlated stacks doesn't match!\n");
		exit(EXIT_FAILURE);
	}
	if (coeff_st_a->N != coeff_st_b->N) {
		fprintf(stderr, "Polynomial order of coefficient sets in the correlated stacks doesn't match!\n");
		exit(EXIT_FAILURE);
	}
	int corr_count = coeff_st_a->count;
	int N = coeff_st_a->N;
	int L = N - 1;
	int Nsteps = 2 * L + 1;

	int FFT_size = FFT_arrays->FFT_size;//Linear size of Fourier Grid
	int FFT_halfsize = FFT_size / 2;
	int X = (Nsteps <= FFT_size) ? (L) : ((FFT_size - 1) / 2); //TODO Re-think the second option.

	double *ar = coeff_st_a->re;
	double *ai = coeff_st_a->im;
	double *br = coeff_st_b->re;
	double *bi = coeff_st_b->im;

	__fftw_complex* W = FFT_arrays->W;

	clock_t loopStart, loopEnd;


	/** fill with zeros **/
	int fft_array_size =
			(FFT_halfsize + 1) *
			(FFT_halfsize + 1) *
			(FFT_halfsize + 1) *
			FFT_size * FFT_size;
	for (int i = 0; i < fft_array_size; i++) {
		W[i][0]=0.0;
		W[i][1]=0.0;
	}

	loopStart = clock();
	for (int s = 0; s <= X; ++s) {
		for (int t = 0; t <= X; ++t) {
			//int sign_s  = pow(-1., 1.*s);
			int sign_t  = pow(-1., 1.*t);
			int sign_st = pow(-1., 1.*(s+t));
			for (int m = 0; m <= X; ++m) {
				//Calc Sdd
				int t_index1 = m * N;
				for (int j = fmft_max(abs(s), m); j <= X; ++j) {
					int t_index2 = (t_index1 + j) * N;
					for (int l = fmft_max(abs(t), m); l <= X; ++l) {
						//loop1Start = clock();
						int t_index3 = (t_index2 + l) * N;
						/** Calculate S_js,lt^(|m|) **/
						double S_r_pp = 0.0;
						double S_r_pm = 0.0;
						double S_i_pp = 0.0;
						double S_i_pm = 0.0;
						for (int i = j+1; i <= N; ++i) {
							int t_index4 = (t_index3 + i - 1) * N;
							for (int n = l+1; n <= N; ++n) {
								const int t_index_fin = t_index4 + n - 1;
								const double T_current = T[t_index_fin];
								double temp_r_pp = 0.0;
								double temp_i_pp = 0.0;
								double temp_r_pm = 0.0;
								double temp_i_pm = 0.0;
								for (int p = 0; p < corr_count; ++p) {
/*O(N^7) LOOP*/								const double ar_element = ar[fmft_index_coeffset_stack(N, corr_count, p, i, j, s)];
									const double ai_element = ai[fmft_index_coeffset_stack(N, corr_count, p, i, j, s)];
									const double br_element = br[fmft_index_coeffset_stack(N, corr_count, p, n, l, t)];
									const double bi_element = bi[fmft_index_coeffset_stack(N, corr_count, p, n, l, t)];
									const double r1 = ar_element*br_element;
									const double r2 = ai_element*bi_element;
									const double i1 = ar_element*bi_element;
									const double i2 = ai_element*br_element;

									temp_r_pp += (r1 + r2);
									temp_i_pp += (i1 - i2);
									temp_r_pm += (r1 - r2);
									temp_i_pm += (-i1 - i2);
								}

								S_r_pp += temp_r_pp * T_current;//*T_ij,nl^(|m|)
								S_i_pp += temp_i_pp * T_current;//*T_ij,nl^(|m|)

								S_r_pm += temp_r_pm * T_current * sign_t;//*T_ij,nl^(|m|)
								S_i_pm += temp_i_pm * T_current * sign_t;//*T_ij,nl^(|m|)

							} // n
						} // i
						W[index_loop2(FFT_size, s, t, m, j, l)][0] = (__real) S_r_pp;
						W[index_loop2(FFT_size, s, t, m, j, l)][1] = (__real) S_i_pp;

						W[index_loop2(FFT_size, s, -t, m, j, l)][0] = (__real) S_r_pm;
						W[index_loop2(FFT_size, s, -t, m, j, l)][1] = (__real) S_i_pm;

						W[index_loop2(FFT_size, -s, t, m, j, l)][0] = (__real)  S_r_pm * sign_st;
						W[index_loop2(FFT_size, -s, t, m, j, l)][1] = (__real) -S_i_pm * sign_st;

						W[index_loop2(FFT_size, -s, -t, m, j, l)][0] = (__real)  S_r_pp * sign_st;
						W[index_loop2(FFT_size, -s, -t, m, j, l)][1] = (__real) -S_i_pp * sign_st;
					} // l
				} // j
			} // m
		} // t
	} // s
	loopEnd = clock();
#ifdef VERBOSE
	printf("LoopN7 time: %f\n", 1.0f*(loopEnd - loopStart)/CLOCKS_PER_SEC);
#endif // VERBOSE
}

static void wigner_d_transform(
		struct fft_data* FFT_arrays,
		const double *d_small,
		const int N)
{
	int L = N - 1;
	int Nsteps = 2 * L + 1;

	int FFT_size = FFT_arrays->FFT_size;//Linear size of Fourier Grid
	int FFT_halfsize = FFT_size / 2;

	int X = (Nsteps <= FFT_size) ? (L) : ((FFT_size - 1) / 2); //TODO Re-think the second option.

	__real* E = FFT_arrays->E;
	__fftw_complex* W = FFT_arrays->W;

	__real acc_re, acc_im, w_re, w_im;
	clock_t loopStart, loopEnd;

#ifdef VERBOSE
	printf("Loop3\n");
#endif // VERBOSE

	/** fill with zeros **/
	int fft_array_size_real = 2 *
				(FFT_halfsize + 1) *
				(FFT_halfsize + 1) *
				(FFT_halfsize + 1) *
				FFT_size * FFT_size;
	for (int i = 0; i < fft_array_size_real; i++) {
		E[i]=0.0;
	}

	loopStart = clock();
	/** calculate second temporary array elements **/
	__real d_cur;
	for (int beta1_id = 0; beta1_id < FFT_halfsize + 1; ++beta1_id)	{
		for (int s = -X; s <= X; ++s) {
			for (int t = -X; t <= X; ++t) {
				for (int m = 0; m <= X; ++m) {
					for (int j = fmft_max(abs(m), abs(s)); j <= X; ++j) {
						acc_re = 0.0;
						acc_im = 0.0;
						for (int l = fmft_max(abs(m), abs(t)); l <= X; ++l) {
							w_re = W[index_loop2(FFT_size, s, t, m, j, l)][0];
							w_im = W[index_loop2(FFT_size, s, t, m, j, l)][1];

							d_cur = (__real) d_small[fmft_index_d_set(L, beta1_id, l, m, t)];//TODO: decide, what to do with L here

							acc_re += w_re * d_cur;
							acc_im += w_im * d_cur;
						}
						E[2*index_loop3(FFT_size, beta1_id, s, t, m, j) + 0] = acc_re;
						E[2*index_loop3(FFT_size, beta1_id, s, t, m, j) + 1] = acc_im;
					}
				}
			}
		}
	}
	loopEnd = clock();

#ifdef VERBOSE
	printf("Loop3 time: %f\n", 1.0f*(loopEnd - loopStart)/CLOCKS_PER_SEC);
#endif // VERBOSE

#ifdef VERBOSE
	printf("Loop4\n");
#endif // VERBOSE

	/** fill with zeros **/
	int fft_array_size_complex =
				(FFT_halfsize + 1) *
				(FFT_halfsize + 1) *
				(FFT_halfsize + 1) *
				FFT_size * FFT_size;
	for (int i = 0; i < fft_array_size_complex; i++) {
		W[i][0] = 0.0;
		W[i][1] = 0.0;
	}

	int s1, t1;
	loopStart = clock();
	/** calculate final array elements**/
	for (int beta_id = 0; beta_id < FFT_halfsize + 1; ++beta_id) {
		for (int beta1_id = 0; beta1_id < FFT_halfsize + 1; ++beta1_id)	{
			for (int s = -X; s <= X; ++s) {
				s1 = (s < 0)? (FFT_size + s):(s);
				for (int t = -X; t <= X; ++t) {
					t1 = (t < 0)? (FFT_size + t):(t);
					for (int m = 0; m <= X; ++m) {
						acc_re = 0.0;
						acc_im = 0.0;
						for (int j = fmft_max(abs(m), abs(s)); j <= X; ++j) {
							w_re = E[2 * index_loop3(FFT_size, beta1_id, s, t, m, j) + 0];
							w_im = E[2 * index_loop3(FFT_size, beta1_id, s, t, m, j) + 1];

							d_cur = (__real) d_small[fmft_index_d_set(L, beta_id, j, m, s)];//TODO: decide, what to do with L here

							acc_re += w_re * d_cur;
							acc_im += w_im * d_cur;
						}
						W[index_loop4(FFT_size, beta_id, beta1_id, s1, t1, m)][0] = acc_re;
						W[index_loop4(FFT_size, beta_id, beta1_id, s1, t1, m)][1] = acc_im;
					}
				}
			}
		}
	}
	loopEnd = clock();

#ifdef VERBOSE
	printf("Loop4 time: %f\n", 1.0f*(loopEnd - loopStart)/CLOCKS_PER_SEC);
#endif // VERBOSE
}

void fmft_coeffstack_correlate(
		struct fmft_result* results,
		const int results_per_z,
		const struct fmft_coeffset_stack* coeff_st_a,
		const struct fmft_coeffset_stack* coeff_st_b,
		const int FFT_size,
		const double Z_start,
		const double Z_stop,
		const double Z_step,
		const double angular_cutoff,
		const char* folder_name,
		const int mpi_rank)
{
	if (coeff_st_a->count != coeff_st_b->count) {
		fprintf(stderr, "[Error][libfmft] The number of coefficient sets in the correlated stacks is not equal.\n");
		exit(EXIT_FAILURE);
	}
	if (coeff_st_a->N != coeff_st_b->N) {
		fprintf(stderr, "[Error][libfmft] The polynomial order of coefficient sets in the correlated stacks is not equal.\n");
		exit(EXIT_FAILURE);
	}
	if (coeff_st_a->lambda != coeff_st_b->lambda) {
		fprintf(stderr, "[Error][libfmft] The lambda scaling parameter of the correlated stacks is not equal.\n");
		exit(EXIT_FAILURE);
	}

	int mpi_root_rank = 0;
	float percent_this_thread;

	clock_t time0 = clock();
	int N = coeff_st_a->N;
	int L = N - 1;

	/***PREPARE FFT ARRAYS & STUFF***/
	int FFT_halfsize = FFT_size/2;
	struct fft_data* FFT_arrays;
	FFT_arrays = prepare_fft_arrays(FFT_size);

	/***ALLOCATE and LOAD (OR CALCULATE) d_small MATRICES***/
	double *d_set;
	struct fmft_wigner_d_set* wd_set;
	wd_set = fmft_wigner_d_set_create(FFT_halfsize + 1, L);
	d_set = wd_set->data;

	/***ALLOCATE TRANSLATION ARRAYS***/
	struct fmft_tmatrix* T;
	T = fmft_tmatrix_allocate(N);

	/*****************************************
	 *         THE MAIN DOCKING LOOP         *
	 * Note, that we only calculate Sdd      *
	 * elements with nonnegative m indices,  *
	 * taking advantage of Sdd hermitian     *
	 * symmetries                            *
	 ****************************************/

	/** set up some timers **/
	clock_t start, stop;

	/** start the main loop **/
	for (double z = Z_start; z <= Z_stop; z += Z_step) {
		start = clock();
		/***LOAD (OR CALCULATE) TRANSLATIONAL MATRIX***/
		fmft_tmatrix_load(T, NMAX, coeff_st_a->lambda, z, folder_name);
		double *T_arr = T->data;

		/*** FILL FFT ARRAY***/
		fill_fft_array(coeff_st_a, coeff_st_b, FFT_arrays, T_arr);
		wigner_d_transform(FFT_arrays, d_set, N);
		stop = clock();
#ifdef VERBOSE1
		printf("For z = %5.2lf : Filling time    : %9.3f\n", z, 1.0*(stop - start)/CLOCKS_PER_SEC);
#endif // VERBOSE

		/*** EXECUTE FFT ***/
		/*****************************************************
		 * We are applying FFT to a range of                 *
		 * (FFT_halfsize+1)*(FFT_halfsize+1)                 *
		 * logically 3D arrays laid out consequentially      *
		 * in memory. Note, that we are using                *
		 * complex-to-real DFT.                              *
		 *****************************************************/
		start = clock();
		_fftw_execute(FFT_arrays->plan);
		stop = clock();
#ifdef VERBOSE1
		printf("              : FFT time        : %9.3f\n", 1.0*(stop - start)/CLOCKS_PER_SEC);
#endif // VERBOSE

		/*** FILTERING ***/
		/**********************************************
		 * Here we successively find local minima     *
		 * and then exclude the surrounding elements  *
		 * from further consideration                 *
		 **********************************************/
		clock_t scoring_time_start = clock();
		//best_scores array has score_count elements reserved for each translation step,
		//so we are passing a pointer to the corresponding memory region as a parameter.
		fmft_filter_results(
			FFT_arrays,
			results + (int)((z - Z_start) / Z_step) * results_per_z,
			results_per_z,
			z,
			angular_cutoff);

		clock_t scoring_time_stop = clock();
#ifdef VERBOSE1
		printf("              : Filtering time : %9.3f\n", 1.0*(scoring_time_stop - scoring_time_start)/CLOCKS_PER_SEC );
#endif // VERBOSE
		clock_t time1 = clock();
#ifdef VERBOSE1
		printf("              : Total time      : %9.3f sec\n\n", 1.0*(time1 - time0)/CLOCKS_PER_SEC);
#endif // VERBOSE

		percent_this_thread = 100 * (z - Z_start + 1) / (Z_stop - Z_start + 1);
		if (mpi_rank == mpi_root_rank)	{
			printf ("\r%3.0f%% complete (approximate, based on root cpu execution status.)", percent_this_thread);
			fflush(stdout);
		}
	}//loop in z

	if (mpi_rank == mpi_root_rank)	{
		printf("\n");
	}

	fmft_tmatrix_free(T);
	fmft_wigner_d_set_free(wd_set);
	_fftw_destroy_plan(FFT_arrays->plan);
	_fftw_free(FFT_arrays->E);
	_fftw_free(FFT_arrays->W);
	_fftw_cleanup();
}


////////////////////////////////////////
// File i/o                           //
////////////////////////////////////////

static void get_cartesian_trans(
			struct fmft_vector_3f* tr,
			const double z,
			const double beta,
			const double gamma)
{
	double rm[3][3];
	fmft_rot_matrix_fill_passive(rm, 0, beta, gamma);
	tr->X = rm[0][2] * z;
	tr->Y = rm[1][2] * z;
	tr->Z = rm[2][2] * z;
}

void fmft_results_write_ft(
			const struct fmft_result *results,
			const int nres,
			const struct fmft_vector_3f ref_lig_pos,
			const char* filename_ft,
			const char* filename_rm)
{
	FILE* ft_file = fopen(filename_ft, "w");
	if (ft_file == NULL) {
		fprintf(stderr, "File %s could not be opened.\n", filename_ft);
		exit(EXIT_FAILURE);
	}
	FILE* rm_file = fopen(filename_rm, "w");
	if (rm_file == NULL) {
		fprintf(stderr, "File %s could not be opened.\n", filename_rm);
		exit(EXIT_FAILURE);
	}

	struct fmft_vector_3f lig_trans;
	double rm[3][3];
	double rm1[3][3];
	double rm_final[3][3];
	for (int i = 0; i < nres; ++i) {
		const struct fmft_result *mv = &results[i];
		get_cartesian_trans(
				&lig_trans,
				mv->Z,
				mv->beta,
				mv->gamma);
		fmft_rot_matrix_fill_passive(
					rm,
					0,
					mv->beta,
					mv->gamma);
		fmft_rot_matrix_fill_active(
					rm1,
					mv->alpha1,
					mv->beta1,
					mv->gamma1);
		fmft_rot_matrix_mult(rm_final, rm, rm1);

		fprintf(ft_file,
			"%-6d % 6.2f % 6.2f % 6.2f %10.9lf\n",
			i,
			lig_trans.X - ref_lig_pos.X,
			lig_trans.Y - ref_lig_pos.Y,
			lig_trans.Z - ref_lig_pos.Z,
			mv->val);

		fprintf(rm_file,
			"%.9lf\t%.9lf\t%.9lf\t%.9lf\t%.9lf\t%.9f\t%.9lf\t%.9lf\t%.9f\n",
			/*i,*/
			rm_final[0][0], rm_final[0][1], rm_final[0][2],
			rm_final[1][0], rm_final[1][1], rm_final[1][2],
			rm_final[2][0], rm_final[2][1], rm_final[2][2]);
	}

	fclose(ft_file);
	fclose(rm_file);
}


////////////////////////////////////////
// Filtering                          //
////////////////////////////////////////
//TODO: The code below needs to be refactored and tested.
struct neighbors
{
	int num;
	int *alpha_id;
	int *beta_id;
	int *gamma_id;
	int howmany;
};

struct trig_tab
{
	int num;
	double *cos;
	double *sin;
	double *beta_cos;
	double *beta_sin;
};

static void deallocate_neighbors(struct neighbors *nbs)
{
	free(nbs->alpha_id);
	free(nbs->beta_id);
	free(nbs->gamma_id);
	free(nbs);
}

static void get_rec_neighbors_s2(
			struct neighbors* nbs,
			double min_passv_rm[3][3],
			const int FFT_size,
			const double theta,
			const struct trig_tab *tab)
{
	int FFT_halfsize = FFT_size / 2;
	double step = 2 * M_PI / FFT_size;
	double step_beta = M_PI / FFT_halfsize;
	double beta, gamma;

	double min_vec[3];//a unit vector corresponding to the position of the ligand on the S2 sphere, surrounding the receptor.
	min_vec[0] = min_passv_rm[0][2];
	min_vec[1] = min_passv_rm[1][2];
	min_vec[2] = min_passv_rm[2][2];

	double c_theta = cos(theta);

	double rm[3][3];
	double vec[3];

	/*************************************************************
	 * Here, we iterate through all the samples of the S2 sphere *
	 * as given by the beta and gamma angles and select the      *
	 * samples that are closer than a given cutoff value theta   *
	 * to the sample given by the min_vec unit vector            *
	 *************************************************************/
	for (int beta_id = 0; beta_id < FFT_halfsize + 1; ++beta_id) {
		beta = beta_id * step_beta;
		for (int gamma_id = 0; gamma_id < FFT_size; ++gamma_id)	{
			gamma = -gamma_id * step;
			fmft_rot_matrix_fill_passive(rm, 0, beta, gamma);
			vec[0] = rm[0][2];
			vec[1] = rm[1][2];
			vec[2] = rm[2][2];
			//calculate the cosine of the angle between two unit vectors;
			double c_probe_theta = min_vec[0]*vec[0] + min_vec[1]*vec[1] + min_vec[2]*vec[2];
			//cosine decreases on [0, Pi], so for probe_theta < theta we have c_probe_thera > c_theta
			if (c_probe_theta > c_theta) {
				nbs->alpha_id[nbs->num] = 0;
				nbs->beta_id[nbs->num] = beta_id;
				nbs->gamma_id[nbs->num] = gamma_id;
				nbs->num++;
				if (nbs->num >= nbs->howmany) {
					nbs->howmany += 1000;
					nbs->alpha_id = (int*) realloc(nbs->alpha_id, nbs->howmany*sizeof(int));
					nbs->beta_id = (int*) realloc(nbs->beta_id, nbs->howmany*sizeof(int));
					nbs->gamma_id = (int*) realloc(nbs->gamma_id, nbs->howmany*sizeof(int));
				}
				//printf("%5.1lf %5.1lf %5.1lf  (%d)\n", alpha1*180.0/M_PI, beta1*180.0/M_PI, gamma1*180.0/M_PI, (int)((alpha1+gamma1)*180.0/M_PI)%360);
			}
		}
	}
}

static void get_lig_neighbors_so3(
			struct neighbors* nbs,
			double C[3][3],
			const int FFT_size,
			const double theta,
			const struct trig_tab *tab)
{
	int FFT_halfsize = FFT_size / 2;
	//double step = 2 * M_PI / FFT_size;
	//double step_beta = M_PI / FFT_halfsize;

	double c_theta = cos(theta);

	double c_alpha1, c_beta1, c_gamma1;
	double s_alpha1, s_beta1, s_gamma1;

	double rm[3][3];
	//double rm_t[3][3];
	//double C_probe[3][3];
	//here we check all the possible rotations of the ligand
	//and add those which are close to min_rm1 to the list.
	for (int alpha1_id = 0; alpha1_id < FFT_size; ++alpha1_id) {
		//alpha1 = alpha1_id*step;
		c_alpha1 = tab->cos[alpha1_id];
		s_alpha1 = tab->sin[alpha1_id];
		for (int beta1_id = 0; beta1_id < FFT_halfsize + 1; ++beta1_id)	{
			//beta1 = beta1_id*step_beta;
			c_beta1 = tab->beta_cos[beta1_id];
			s_beta1 = tab->beta_sin[beta1_id];

			rm[0][2] = s_beta1*c_alpha1;
			rm[1][2] = s_beta1*s_alpha1;
			rm[2][2] = c_beta1;

			double cbca = c_beta1*c_alpha1;
			double cbsa = c_beta1*s_alpha1;

			for (int gamma1_id = 0; gamma1_id < FFT_size; ++gamma1_id) {
				//gamma1 = gamma1_id*step;
				//fill_passive_rotation_matrix(rm, alpha1, beta1, gamma1);
				c_gamma1 = tab->cos[gamma1_id];
				s_gamma1 = tab->sin[gamma1_id];

				rm[0][0] = c_gamma1*cbca - s_gamma1*s_alpha1;
				rm[1][0] = c_gamma1*cbsa + s_gamma1*c_alpha1;
				rm[2][0] = -c_gamma1*s_beta1;
				rm[0][1] = -s_gamma1*cbca - c_gamma1*s_alpha1;
				rm[1][1] = -s_gamma1*cbsa + c_gamma1*c_alpha1;
				rm[2][1] = s_gamma1*s_beta1;

				// Here is what is actually happening above:
				//rm[0][0] = c_gamma1*c_beta1*c_alpha1 - s_gamma1*s_alpha1;
				//rm[0][1] = c_gamma1*c_beta1*s_alpha1 + s_gamma1*c_alpha1;
				//rm[0][2] = -c_gamma1*s_beta1;
				//rm[1][0] = -s_gamma1*c_beta1*c_alpha1 - c_gamma1*s_alpha1;
				//rm[1][1] = -s_gamma1*c_beta1*s_alpha1 + c_gamma1*c_alpha1;
				//rm[1][2] = s_gamma1*s_beta1;
				//rm[2][0] = s_beta1*c_alpha1;
				//rm[2][1] = s_beta1*s_alpha1;
				//rm[2][2] = c_beta1;

				//fill_active_rotation_matrix(rm, alpha1_id * step, beta1_id * step_beta, gamma1_id * step);

				//Check whether the rotation is close to min_rm1, and if yes,
				//add it to the list.
				//N.B.: compare_rot_mats_fast() uses the trace of A_transposed*B matrix,
				//so it doesn't matter whether A and B are active or passive rotation matrices.
				//(but they should be of one type, obviously)
				if (fmft_rot_matrix_compare_fast(C, rm, c_theta) == 1) {
					nbs->alpha_id[nbs->num] = alpha1_id;
					nbs->beta_id[nbs->num] = beta1_id;
					nbs->gamma_id[nbs->num] = gamma1_id;
					nbs->num++;
					if (nbs->num >= nbs->howmany) {
						nbs->howmany += 1000;
						nbs->alpha_id = (int*) realloc(nbs->alpha_id, nbs->howmany*sizeof(int));
						nbs->beta_id = (int*) realloc(nbs->beta_id, nbs->howmany*sizeof(int));
						nbs->gamma_id = (int*) realloc(nbs->gamma_id, nbs->howmany*sizeof(int));
					}
				}
			}
		}
	}
}

static inline int fmft_index_fft_array(
			int S,
			int beta_id,
			int beta1_id,
			int gamma_id,
			int gamma1_id,
			int alpha1_id)
{
	int HS = S / 2;
	return S*(S*(S*((HS+1)*beta_id + beta1_id) + gamma_id) + gamma1_id) + alpha1_id;
}

void fmft_filter_results(
		struct fft_data* FFT_arrays,
		struct fmft_result *best_scores,
		const int score_count,
		const double z,
		const double theta)
{
	__fftw_complex* W = FFT_arrays->W;
	__real* E = FFT_arrays->E;
	int FFT_size = FFT_arrays->FFT_size;
	int FFT_halfsize = FFT_size / 2;
	int scoring_array_size =
			(FFT_halfsize + 1) *
			(FFT_halfsize + 1) *
			FFT_size *
			FFT_size *
			FFT_size;

	double step = 2.0 * M_PI / (double)FFT_size;
	double step_beta = M_PI / (double)FFT_halfsize;//TODO: check


	int* idx = (int*) W; //reusing the memory here
	for (int i = 0; i < scoring_array_size; ++i) {
		idx[i] = i;
	}

	if (score_count == 1) {
		fmft_cmp_real mcmp;
		mcmp.arr = E;

		std::partial_sort (idx + 0, idx + 1, idx + scoring_array_size, mcmp);

		int tmp = idx[0];

		int alpha1_id = tmp % FFT_size;
		tmp /= FFT_size;
		int gamma1_id = tmp % FFT_size;
		tmp /= FFT_size;
		int gamma_id = tmp % FFT_size;
		tmp /= FFT_size;
		int beta1_id = tmp % (FFT_halfsize + 1);
		tmp /= (FFT_halfsize+1);
		int beta_id = tmp % (FFT_halfsize + 1);

		double min_beta = beta_id * step_beta;
		double min_gamma = -gamma_id * step;
		double min_alpha1 = alpha1_id * step;
		double min_beta1 = beta1_id * step_beta;
		double min_gamma1 = gamma1_id * step;

		best_scores[0].val = (double) E[idx[0]];
		best_scores[0].Z = z;

		best_scores[0].beta = min_beta;
		best_scores[0].gamma = min_gamma;
		best_scores[0].alpha1 = min_alpha1;
		best_scores[0].beta1 = min_beta1;
		best_scores[0].gamma1 = min_gamma1;
	} else {
		int sort_bin_size = 100000;//TODO Just in case, think a bit more about this one
		int last_step =  scoring_array_size/sort_bin_size;

		int loc_score_id = 0;

		fmft_cmp_real mcmp;
		mcmp.arr = E;

		//TODO Should wrap this mess in some functions.
		struct trig_tab *tabulated;
		tabulated = (struct trig_tab*) malloc(sizeof(struct trig_tab));
		tabulated->num = FFT_size;
		tabulated->cos = (double*) malloc(sizeof(double) * (tabulated->num));
		tabulated->sin = (double*) malloc(sizeof(double) * (tabulated->num));
		tabulated->beta_cos = (double*) malloc(sizeof(double) * (tabulated->num / 2 + 1)); //TODO this is ugly
		tabulated->beta_sin = (double*) malloc(sizeof(double) * (tabulated->num / 2 + 1));

		for (int i = 0; i < tabulated->num; ++i) {
			tabulated->cos[i] = cos(i * step);
			tabulated->sin[i] = sin(i * step);
		}

		for (int i = 0; i < tabulated->num / 2 + 1; ++i) {
			tabulated->beta_cos[i] = cos(i * step_beta);
			tabulated->beta_sin[i] = sin(i * step_beta);
		}

		struct neighbors* rec_nbs;
		struct neighbors* lig_nbs;
		rec_nbs = (struct neighbors*) malloc(sizeof(struct neighbors));
		rec_nbs->num = 0;
		rec_nbs->howmany = 1000;
		rec_nbs->alpha_id = (int*) calloc(rec_nbs->howmany, sizeof(int));
		rec_nbs->beta_id = (int*) calloc(rec_nbs->howmany, sizeof(int));
		rec_nbs->gamma_id = (int*) calloc(rec_nbs->howmany, sizeof(int));

		lig_nbs = (struct neighbors*) malloc(sizeof(struct neighbors));
		lig_nbs->num = 0;
		lig_nbs->howmany = 1000;
		lig_nbs->alpha_id = (int*) calloc(lig_nbs->howmany, sizeof(int));
		lig_nbs->beta_id = (int*) calloc(lig_nbs->howmany, sizeof(int));
		lig_nbs->gamma_id = (int*) calloc(lig_nbs->howmany, sizeof(int));

		for (int sort_step = 0; (sort_step < last_step) && (loc_score_id < score_count); ++sort_step) {
			std::partial_sort(
					idx + sort_step * sort_bin_size,
					idx + (sort_step + 1) * sort_bin_size,
					idx + scoring_array_size,
					mcmp);
			//N.B.: Passive rotation marix is basically a transposed active one.
			double rec_prm[3][3];//receptor passive rotation matrix.
			double rec_arm[3][3];//receptor active rotation matrix.
			double lig_arm[3][3];//ligand active rotation matrix.
			double C[3][3];//ligand full active rotation matrix (C=rec_prm*lig_arm)
			double C_probe[3][3];
			for (int i = sort_step*sort_bin_size; (i < (sort_step+1)*sort_bin_size) && (loc_score_id < score_count); ++i) {
				if (E[idx[i]] != __REAL_MAX) {
					int tmp = idx[i];
					int alpha1_id = tmp % FFT_size;
					tmp /= FFT_size;
					int gamma1_id = tmp % FFT_size;
					tmp /= FFT_size;
					int gamma_id = tmp % FFT_size;
					tmp /= FFT_size;
					int beta1_id = tmp % (FFT_halfsize + 1);
					tmp /= (FFT_halfsize+1);
					int beta_id = tmp % (FFT_halfsize + 1);

					double min_beta = beta_id * step_beta;
					double min_gamma = -gamma_id * step;
					double min_alpha1 = alpha1_id * step;
					double min_beta1 = beta1_id * step_beta;
					double min_gamma1 = gamma1_id * step;

					best_scores[loc_score_id].val = (double) E[idx[i]];
					best_scores[loc_score_id].Z = z;

					best_scores[loc_score_id].beta = min_beta;
					best_scores[loc_score_id].gamma = min_gamma;
					best_scores[loc_score_id].alpha1 = min_alpha1;
					best_scores[loc_score_id].beta1 = min_beta1;
					best_scores[loc_score_id].gamma1 = min_gamma1;

					loc_score_id++;

					fmft_rot_matrix_fill_passive(
								rec_prm,
								0,
								min_beta,
								min_gamma);
					fmft_rot_matrix_fill_active(
								lig_arm,
								min_alpha1,
								min_beta1,
								min_gamma1);
					//the following gives us a full rotation of the ligand
					//(i.e rotation around the ligand's own center
					//followed by the rotation around the center of the receptor)
					fmft_rot_matrix_mult(C, rec_prm, lig_arm);

					rec_nbs->num = 0;
					get_rec_neighbors_s2(rec_nbs, rec_prm, FFT_size, theta, tabulated);
					//printf("rec_angles: %.2lf %.2lf\n", min_beta*180.0/M_PI, min_gamma*180/M_PI);
					//printf("rec_nbs->num: %d\n\n", rec_nbs->num);
					//clear region
					for (int j = 0; j < rec_nbs->num; ++j) {
						fmft_rot_matrix_fill_active(
									rec_arm,
									0,
									rec_nbs->beta_id[j] * step_beta,
									-rec_nbs->gamma_id[j] * step);
						fmft_rot_matrix_mult(C_probe, rec_arm, C);

						lig_nbs->num = 0;
						get_lig_neighbors_so3(lig_nbs, C_probe, FFT_size, theta, tabulated);

						for (int k = 0; k < lig_nbs->num; ++k) {
							E[fmft_index_fft_array(FFT_size,
									rec_nbs->beta_id[j],
									lig_nbs->beta_id[k],
									rec_nbs->gamma_id[j],
									lig_nbs->gamma_id[k],
									lig_nbs->alpha_id[k])] = __REAL_MAX;
						}
					}
				}
			}
		}
		deallocate_neighbors(rec_nbs);
		deallocate_neighbors(lig_nbs);

		free(tabulated->beta_cos);
		free(tabulated->beta_sin);
		free(tabulated->cos);
		free(tabulated->sin);
		free(tabulated);
	}
}
