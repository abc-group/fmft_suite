/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#pragma once
#include <stdio.h>
#include <float.h>
#include <algorithm>
#include <vector>
#include "index.h"
#include <fftw3.h>
#include <math.h>
//#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "translation_matrix.h"
#include "rotation_matrix.h"
#include "spf_coefficients.h"
//#include "angle_distance.h"
#include "vector_matrix.h"
#include "common.h"

/**
 * \addtogroup l_correlate correlate
 * \brief FMFT correlations.
 *
 * The most important part of the library, dedicated to computing correlations on 5D rotational manifolds.\n
 * In short, it provides the means for computing a sum of correlations of signals represented as
 * Spherical polar Fourier expansion coefficients on a grid formed by equispaced
 * spacing of 2 Euler angles of body A and 3 Euler angles of body B. The 6th degree of freedom -
 * trnaslation distance - is sampled explicitly.
 *
 * For more details see [1].
 *
 * -#  "Protein docking by manifold Fourier transforms",\n
 * 	Dzmitry Padhorny, Andrey Kazennov,
 * 	Brandon S. Zerbe, Kathryn A. Porter, Bing Xia, Scott E. Mottarella,
 * 	Yaroslav Kholodov, David W. Ritchie, Sandor Vajda, Dima Kozakov.\n
 *	<b>Proceedings of the National Academy of Sciences</b>\n
 *	Jul 2016, 113 (30) E4286-E4293; DOI: 10.1073/pnas.1603929113
 *
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Orientation of 2 rigid bodies in terms of 5 Euler angles (in radian)
 * and 1 center-center distance with a score assigned.
 */
struct fmft_result
{
	double val; 	/**< Score */
	double beta;	/**< Beta rotation angle of the first body */
	double gamma;	/**< Gamma rotation angle of the first body */
	double alpha1;	/**< Alpha rotation angle of the second body */
	double beta1;	/**< Beta rotation angle of the second body */
	double gamma1; 	/**< Gamma rotation angle of the second body */
	double Z;	/**< Distance between body centers (Coefficient stacks origins) */
};

/**
 * 	Compute 5D rotational correlations of 2 fmft_coeffset_stack 's
 * 	for a series of translation steps and report the top-scoring orientations.
 * 	(By top-scoring we mean those with lowest correlation values).
 *
 * 	The correlations are evaluated for each pair of coefficient sets in the two stacks,
 * 	and the sum of correlations for each orientation is taken as its score.
 * 	If different correlations in the sum need to be weighted, this should be
 * 	done before running this routine by scaling the coefficient sets in one of
 * 	the stacks with \ref fmft_coeffset_stack_scale() function.
 *
 * 	The routine effectively samples the whole conformational space of 2 rigid
 * 	body orientations on a grid.
 *
 * 	A requested number of results per each translation step is written to
 * 	to an array specified by the user, and it is the users responsibility
 * 	to ensure the array is able to store at least
 * 	((Z_stop - Z_start) / Z_step + 1) * results_per_z fmft_result elements.
 * 	Note that while the results for each translation are sorted,
 * 	the whole array is not.
 *
 * 	The FFT size specified by the user determines the angular sampling.
 * 	For gamma, alpha1 and gamma1 angles the angular steps are 360/FFT_size degrees,
 * 	and for beta and beta1 angles the steps are 180/(FFT_size/2+1) degrees.
 *
 * 	To ensure that the reported results do not oversample certain conformational
 * 	regions due to Euler angle degenracy, a filtering procedure is performed
 * 	on the raw correlation results that ensures that the reported minima are
 * 	not closer than cutoff angle to each other both in terms of S2 and SO(3)
 * 	orientations.
 *
 * 	Please note that this function uses tabulated translation matrices.
 * 	It looks for them in a folder specified by the user.
 *
 * 	\param results 		Preallocated array to wich the results will be written.
 * 	\param results_per_z	Number of top-scoring (lowwest correlation value) results to be reported for each translation step.
 * 	\param coeff_st_a	The first coefficient stack to be correlated.
 * 	\param coeff_st_b	The second coefficient stack to be correlated.
 * 	\param FFT_size		FFT size for which the correlations are computed. Determines the angular sampling step.
 * 	\param Z_start		Minimum translation distance (distance between fmft_coeffstack origins).
 * 	\param Z_stop		Maximim translation distance (distance between fmft_coeffstack origins).
 * 	\param Z_step		Translation step.
 * 	\param angular_cutoff 	Filtering parameter (angle in radians).
 * 				Results reported will have at least this distance both on S2 and SO(3) from each other.
 * 	\param tmatr_path	Folder containing tabulated translation matrices.
 * 	\param mpi_rank		MPI rank of the current thread. Only relevant when MPI is used.
 */
void fmft_coeffstack_correlate(
		struct fmft_result* results,
		const int results_per_z,
		const struct fmft_coeffset_stack* coeff_st_a,
		const struct fmft_coeffset_stack* coeff_st_b,
		const int FFT_size,
		const double Z_start,
		const double Z_stop,
		const double Z_step,
		const double angular_cutoff,
		const char* tmatr_path,
		const int mpi_rank);

/**
 * 	Write the docking results in the PIPER-like format.
 * 	\param results		Pointer to the results array.
 * 	\param nresults		How many results from the array to write.
 * 	\param ref_lig_pos	The reference position of the second body,
 * 				realtive to which the results will be provided.
 * 	\param filename_ft 	Name of the output ft file to be created.
 * 	\param filename_rm 	Name of the output rm file to be created.
 */
void fmft_results_write_ft(
		const struct fmft_result *results,
		const int nresults,
		const struct fmft_vector_3f ref_lig_pos,
		const char* filename_ft,
		const char* filename_rm);

/**
 * 	Perform a partial sort of the fmft_result array.
 * 	\param results	The input array to be sorted.
 * 	\param num_best	The number of top results to be sorted.
 * 	\param num_all	The total array size.
 */
void fmft_results_sort(
		struct fmft_result* results,
		int num_best,
		int num_all);

#ifdef __cplusplus
}
#endif

/** @}*/
