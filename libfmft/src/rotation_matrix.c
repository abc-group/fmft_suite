/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "rotation_matrix.h"

/**
 * Allocates an instance of d_array struct
 * that is intended to store Wigner d matrix elements up to polynomial order L.
 * \param L Highest polynomial order, for which matrix elements will be stored.
 * \return Pointer to the allocated struct.
 */
static struct fmft_wigner_d_matrix* fmft_wigner_d_matrix_allocate(const int L)
{
	struct fmft_wigner_d_matrix* d;
	d = (struct fmft_wigner_d_matrix*) calloc(1, sizeof(struct fmft_wigner_d_matrix));
	d->L = L;
	d->data = (double*) calloc((L+1)*(2*L + 1)*(2*L + 1), sizeof(double));
	return d;
}

/**
 * Allocates an instance of many_d_arrays struct
 * that is intended to store elements of num_angles Wigner d matrix up to polynomial order L.
 * \param L Highest polynomial order, for which matrix elements will be stored.
 * \param num_angles Number of different Euler beta angle values, for which matrix elements will be stored.
 * \return Pointer to the allocated struct.
 */
static struct fmft_wigner_d_set* fmft_wigner_d_set_allocate(const int L, const int num_angles)
{
	struct fmft_wigner_d_set* md;
	md = (struct fmft_wigner_d_set*) calloc(1, sizeof(struct fmft_wigner_d_set));
	md->L = L;
	md->num_angles = num_angles;
	md->data = (double*) calloc(num_angles*(L + 1)*(2*L + 1)*(2*L + 1), sizeof(double));
	return md;
}

/**
 * Allocates an instance of D_array struct
 * that is intended to store Wigner D matrix elements up to polynomial order L.
 * \param L Highest polynomial order, for which matrix elements will be stored.
 * \return Pointer to the allocated struct.
 */

static struct fmft_wigner_D_matrix* fmft_wigner_D_matrix_allocate(const int L)
{
	struct fmft_wigner_D_matrix* D;
	D = (struct fmft_wigner_D_matrix*) calloc(1, sizeof(struct fmft_wigner_D_matrix));
	D->L = L;
	D->re = (double*) calloc((L+1)*(2*L + 1)*(2*L + 1), sizeof(double));
	D->im = (double*) calloc((L+1)*(2*L + 1)*(2*L + 1), sizeof(double));
	return D;
}

void fmft_wigner_d_matrix_free(struct fmft_wigner_d_matrix* d)
{
	free(d->data);
	free(d);
}

void fmft_wigner_d_set_free(struct fmft_wigner_d_set* md)
{
	free(md->data);
	free(md);
}

void fmft_wigner_D_matrix_free(struct fmft_wigner_D_matrix* D)
{
	free(D->re);
	free(D->im);
	free(D);
}

struct fmft_wigner_d_matrix* fmft_wigner_d_matrix_create(const int L, const double beta)
{
	struct fmft_wigner_d_matrix* d;
	d = fmft_wigner_d_matrix_allocate(L);
	/* Wolfram Mathematica*/

	d->data[fmft_index_d_array(L, 0,  0,  0)] =  1.0;
	d->data[fmft_index_d_array(L, 1, -1, -1)] =  1.0 * (1.0 + cos(beta)) / 2.0;
	d->data[fmft_index_d_array(L, 1, -1,  0)] =  1.0 * sin(beta) / sqrt(2.0);
	d->data[fmft_index_d_array(L, 1, -1,  1)] =  1.0 * (1.0 - cos(beta)) / 2.0;
	d->data[fmft_index_d_array(L, 1,  0, -1)] = -1.0 * sin(beta) / sqrt(2.0);
	d->data[fmft_index_d_array(L, 1,  0,  0)] =  1.0 * cos(beta);
	d->data[fmft_index_d_array(L, 1,  0,  1)] =  1.0 * sin(beta) / sqrt(2.0);
	d->data[fmft_index_d_array(L, 1,  1, -1)] =  1.0 * (1.0 - cos(beta)) / 2.0;
	d->data[fmft_index_d_array(L, 1,  1,  0)] = -1.0 * sin(beta) / sqrt(2.0);
	d->data[fmft_index_d_array(L, 1,  1,  1)] =  1.0 * (1.0 + cos(beta)) / 2.0;

	for (int l = 2; l <= L; ++l)
	{
		for (int m = -l; m <= l; ++m)
		{
			double fact, fact1 = 1.0;
			//fact = sqrt( (2 * l + 1) / 2.);
			fact = 1;
			double d1 = fact;
			double d2 = fact;
			double d3 = fact;
			double d4 = fact;
			/* Eq. 26 Kostelec 2003 */
			for (int k = 1; k <= 2 * l; ++k)
			{
				fact = sqrt(k / (((k <= l + m) ? k : 1.0) * ((k <= l - m) ? k : 1.0)));
				fact1 *= fact;
				d1 *= fact * ((k <= l + m) ? cos(beta / 2.0) : 1.0)* ((k <= l - m) ? -sin(beta / 2.0) : 1.0);
				d2 *= fact * ((k <= l - m) ? cos(beta / 2.0) : 1.0)* ((k <= l + m) ?  sin(beta / 2.0) : 1.0);
				d3 *= fact * ((k <= l + m) ? cos(beta / 2.0) : 1.0)* ((k <= l - m) ?  sin(beta / 2.0) : 1.0);
				d4 *= fact * ((k <= l - m) ? cos(beta / 2.0) : 1.0)* ((k <= l + m) ? -sin(beta / 2.0) : 1.0);
			}

			d->data[fmft_index_d_array(L, l,  l,  m)] = d1;
			d->data[fmft_index_d_array(L, l, -l,  m)] = d2;
			d->data[fmft_index_d_array(L, l,  m,  l)] = d3;
			d->data[fmft_index_d_array(L, l,  m, -l)] = d4;

			for (int m1 = -l; m1 <=l; ++m1)
			{
				int j = l - 1;
				double val, val1;
				/* Eq. 28 Kostelec 2003 */
				if ((m1 > -l) && (m1 < l) && (m > -l) && (m < l))
				{
					val  = 1.0;
					val *= (j + 1) * (2.0 * j + 1) / sqrt(((j + 1) * (j + 1) - m * m) * ((j + 1) * (j + 1) - m1 * m1));
					val *= (cos(beta) - (double)(m * m1) / (double)(j * (j + 1))) * d->data[fmft_index_d_array(L, j, m, m1)];

					val1  = -1.0;
					val1 *= sqrt((j * j - m * m) * (j * j - m1 * m1));
					val1 *= (double)(j + 1.0) * d->data[fmft_index_d_array(L, j - 1, m, m1)] / (double)j;
					val1 /= sqrt(((j + 1.0) * (j + 1.0) - (double)(m * m)) * ((j + 1.0) * (j + 1.0) - (double)(m1*m1)));

					val += val1;
					d->data[fmft_index_d_array(L, l, m, m1)] = val;
				}
			}
		}
	}

	return d;
}

struct fmft_wigner_d_set* fmft_wigner_d_set_create(const int num_angles, const int L)
{
	struct fmft_wigner_d_matrix *d;
	struct fmft_wigner_d_set* md;
	md = fmft_wigner_d_set_allocate(L, num_angles);
	for (int i = 0 ; i < num_angles; ++i)
	{
		double beta = M_PI * (double)i / (double)(num_angles - 1);
		d = fmft_wigner_d_matrix_create(L, beta);
		for (int l = 0; l <= L; l++)
		{
			for (int m = -l; m <= l; m++)
			{
				for (int m1 = -l; m1 <= l; m1++)
				{
					md->data[fmft_index_d_set(L, i, l, m, m1)] = d->data[fmft_index_d_array(L, l, m, m1)];
				}
			}
		}
	}
	fmft_wigner_d_matrix_free(d);

	return md;
}

struct fmft_wigner_D_matrix* fmft_wigner_D_matrix_create(const int L,
					const double alpha,
					const double beta,
					const double gamma)
{
	struct fmft_wigner_D_matrix* D;
	D = fmft_wigner_D_matrix_allocate(L);
	struct fmft_wigner_d_matrix* d;
	d = fmft_wigner_d_matrix_create(L, beta);
	for (int l = 0; l <= L; ++l)
	{
		//printf("\nl = %d\n", l);
		for (int m = -l; m <= l; ++m)
		{
			for (int m1 = -l; m1 <= l; ++m1)
			{
				double dlmm1;
				dlmm1 = d->data[fmft_index_d_array(L, l, m, m1)];

				D->re[fmft_index_D_array(L, l, m, m1)] = cos(m * alpha + m1 * gamma) * dlmm1;
				D->im[fmft_index_D_array(L, l, m, m1)] = sin(m * alpha + m1 * gamma) * dlmm1;
			}
			// printf("\n");
		}
	}
	fmft_wigner_d_matrix_free(d);

	return D;
}

struct fmft_coeffset* fmft_coeffset_rotate(const struct fmft_coeffset* coeffs, const struct fmft_wigner_D_matrix* D)
{
	int N = coeffs->N;
	int L = N - 1;
	struct fmft_coeffset* rot_coeffs;
	rot_coeffs = fmft_coeffset_create(N);

	for (int n = 1; n <= N; ++n)
	{
		for (int l = 0; l <= L; ++l)
		{
			for (int m = -l; m <= l; ++m)
			{
				double re = 0.;
				double im = 0.;
				for (int m1 = -l; m1 <= l; ++m1)
				{
					double Dr = D->re[fmft_index_D_array(L, l, m, m1)];
					double Di = D->im[fmft_index_D_array(L, l, m, m1)];

					re +=  Dr * coeffs->re[fmft_index_coeffset(N, n, l, m1)] - Di * coeffs->im[fmft_index_coeffset(N, n, l, m1)];
					im +=  Dr * coeffs->im[fmft_index_coeffset(N, n, l, m1)] + Di * coeffs->re[fmft_index_coeffset(N, n, l, m1)];
				}
				rot_coeffs->re[fmft_index_coeffset(N, n, l, m)] = re;
				rot_coeffs->im[fmft_index_coeffset(N, n, l, m)] = im;
			}
		}
	}

	return rot_coeffs;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS

///////////////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.          //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Should get rid of this.                                                   //
///////////////////////////////////////////////////////////////////////////////
#endif // DOXYGEN_SHOULD_SKIP_THIS
