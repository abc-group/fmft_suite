/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include "simple_functions.h"

#define _MY_ROOT_ 0
//Returns a value between 0 and 1.  This would be the proportion of the circle.

int fmft_min(int i, int j)
{
	if(i<j)
		return i;
	else
		return j;
}
int fmft_max(int i, int j)
{
	if(i>j)
		return i;
	else
		return j;
}

float fmft_spherical_coord_phi_from_xy(float x,float y)
{
	const float PI = 3.14159265358979323846;
	float value;
	//printf("x = %f, y=%f, atan(y/x)/2PI = %f\n",x, y, atan(y/x)/(2*PI));
	if (x > 0.00)
	{
		value = atan(y/x)/(2.00*PI);
                if (y >= 0.00)
                {
			return value;
                }
                else
                {
                        return ( 1.00 + value );
                }
        }
        else if (x < 0.00)
        {
		value = atan(y/x)/(2*PI);
                return ( 0.50 + value );
        }
        else
        {
                if (y >= 0.00)
                {
                        return 0.25;
                }
                else
                {
                        return 0.75;
                }
	}
}

//Returns the value between 0 and PI.
float fmft_spherical_coord_theta_from_zr(float z, float r)
{
        if (r != 0)
        {
                return acos(z/r);
        }
        else
        {
                return 0;
        }
}

///////////////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.          //
///////////////////////////////////////////////////////////////////////////////
void fmft_root_printf(const int mpi_rank, const char* fmt, ... )
{
	va_list args;
	va_start( args, fmt );

#ifdef _MPI_
	if (mpi_rank == _MY_ROOT_)
	{
		vprintf( fmt, args );
	}
#else
	vprintf( fmt, args );
#endif

	va_end( args );
}

int fmft_is_whitespace_string(const char *s)
{
	int i = 0;

	while (s[i] != '\0')
	{
		if (!isspace(s[i]))
			return 0;
		i++;
	}

	return 1;
}

void fmft_safe_getline(char **lineptr, size_t *n, FILE *stream)
{
	ssize_t retval = getline(lineptr, n, stream);

	if (retval < 0)
	{
		fprintf(stderr, "Error reading file stream:\n");
		perror("Error:");
	}
}

//TODO make it count only non-emoty lines
unsigned int fmft_count_lines (FILE *file)
{
	unsigned int lines = 0;
	int c = '\0';
	int pc = '\n';

	while (c = fgetc (file), c != EOF)
	{
		if (c == '\n' && pc != '\n')
			lines++;
		pc = c;
	}
	if (pc != '\n')
		lines++;

	rewind(file);

	return lines;
}

unsigned int fmft_count_words (const char string[ ])
{
	int nwords = 0;

	// state:
	const char* it = string;
	int inword = 0;
	int line_end = 0;

	do switch (*it)
	{
		case '\0': case '\n':
			if (inword)
			{
				inword = 0;
				nwords++;
			}
			line_end = 1;
			break;

		case ' ': case '\t': // TODO others?
			if (inword)
			{
				inword = 0;
				nwords++;
			}
			break;

		default: inword = 1;
	} while (*it++ && line_end == 0);

	return nwords;
}
