/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#pragma once

#include <string.h>
#include <gmpxx.h>
#include "index.h"
#include "spf_coefficients.h"

/**
 * \addtogroup l_translation translation_matrix
 * \brief Translation of expansion coefficients.
 *
 * This module is dedicated to computation and management of the coefficient translation matrices.
 * The matrices are computed for the specific case when Gaussian oribitals are used as radial harmonics.
 * Most of the analytical expressions used for calculations are taken from [1].
 *
 * Due to some inherent properties of these translation matrices,
 * double precision is not sufficient for computation, so we use the GMP library
 * for arbitrary precision calculations.
 * To make the develpment easier, we have used the C++ interface of the GMP
 * that overloads most standard arythmetic operators.
 * As a downside, we now have to compile the whole library (which is essentially written in C)
 * using a C++ compiler.
 * Note that all the functions and structures that use GMP are marked with a "_gmp" suffix.
 *
 * Because of the precision issues and due to the heavy computations involved,
 * in most practical cases it is convinient to precompute the matrices and store them on the disc,
 * loading into memory when necessary. On the disc, the storage is always in double precision.
 *
 * Therefore, in practice, the most used bits of this module are:
 * - Tabulating the translation matrix for a given polynomial order, translation distance and lambda scaling factor:
 * 	-# \ref fmft_tmatrix_tabulate();
 * - Allocating an empty matrix for a set polynomial order:
 * 	-# \ref fmft_tmatrix_allocate();
 * - Loading the tabulated translation matrix for a given translation distance and lambda scaling factor into a preallocated array:
 * 	-# \ref fmft_tmatrix_load();
 *
 * Note that when loading the matrix, the polynomial order of the preallocated array can be lower than the one for which
 * the matrix was tabulated, which allows to reuse a matrix precomputed for sufficiently high order for different
 * calculations carried out at lower polynomial orders..
 *
 *
 * Another potentially useful option is the actual translation of expansion coefficients:
 * 	-# \ref fmft_coeffset_translate();
 *
 * There is a variaty of other functionality in here, but those things are more technical, and rarely used directly.
 *
 * @{
 */

// Tabulation constants:
//#define TFOLDER "data"
#define TFILE "transMatrix"
#define NMAX 32 /* Polinomial order, for which translation matrices will be tabulated.*/

// GMP constants:
#define GMP_PREC 256 /* Precision in bits, for which elements of translation matrix will be calculated.*/
typedef mpf_class fpval;

/**
 * 	This struct is used for storing elements of translation matrix
 * 	calculated up to polynomial order N in precision defined by the GMP_PREC constant.
 */
struct fmft_tmatrix_gmp
{
	int N; /**< Highest polinoial order, for which matrix elements are stored.*/
	fpval* data; /**< Array of size N*N*N*N*N. */
};

/**
 * 	This struct is used for storing elements of translation matrix
 * 	calculated up to polynomial order N in double precision.
 */
struct fmft_tmatrix
{
	int N;
	double* data;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * 	Indexer for
 * 	arrays storing elements of translation matrix.
 * 	\param N Highest polynomial order, for which the matrix elements are stored.
 * 	\param n In range [1, N].
 * 	\param l In range [0, n-1].
 * 	\param n1 In range [1, N].
 * 	\param l1 In range [0, n-1].
 * 	\param m In range [0, min(l, l1)]
 */
inline int fmft_index_tmatrix(int N, int n, int l, int n1, int l1, int m)
{
	return N*(N*(N*(N*m + l) + l1) + n-1) + n1-1;
}


/**
 * 	Allocates an instance of T_array_arb struct
 * 	that is intended to store translation matrix elements
 * 	up to polynomial order N in precision defined by the GMP_PREC constant.
 * 	\param N Highest polynomial order, for which matrix elements will be stored.
 * 	\return Pointer to the allocated struct.
 */
struct fmft_tmatrix_gmp* fmft_tmatrix_gmp_allocate(const int N);

/**
 * 	Deallocates an instance of T_array_arb struct.
 */
void fmft_tmatrix_gmp_free(struct fmft_tmatrix_gmp* T_arb);

/**
 * 	Allocates an instance of T_array_arb struct and
 * 	fills it with translation matrix elements calculated for a
 * 	specific value of translation distance z up to polynomial order N
 * 	in GMP_PREC bit precision.
 * 	\return Pointer to the filled struct.
 */
struct fmft_tmatrix_gmp* fmft_tmatrix_gmp_compute(const int N, const float lambda, const double z);

/**
 * 	Allocates an instance of fmft_tmatrix
 * 	capable of storing translation matrix elements
 * 	up to polynomial order N in double precision.
 * 	\param N	Highest polynomial order, for which matrix elements will be stored.
 * 	\return Pointer to the allocated struct.
 */
struct fmft_tmatrix* fmft_tmatrix_allocate(const int N);

/**
 * 	Deallocates an instance of A_array struct.
 */
void fmft_tmatrix_free(struct fmft_tmatrix* T);

/**
 * 	Get a double precision translation matrix from the arbitrary precision one.
 * 	\param T_gmp 	Translation matrix in arbitraty precision.
 * 	\return Translation matrix in double precision.
 */
struct fmft_tmatrix* fmft_tmatrix_from_gmp(const struct fmft_tmatrix_gmp* T_gmp);

/**
 *	Write an arbitrary precision translation matrix to file.
 *	\param T_gmp		An instance of fmft_tmatrix_gmp.
 *	\param filename	File to which the matrix will be written.
 */
void fmft_tmatrix_gmp_write(
			const struct fmft_tmatrix_gmp* T_gmp,
			const char* filename);

/**
 * Read a double precision translation matrix from file.
 * 	\param T		Preallocated fmft_tmatrix.
 * 	\param filename	File from which the matrix will be read.
 */
void fmft_tmatrix_read(
			struct fmft_tmatrix* T,
			const char* filename);

/**
 * 	Generates elements of translation matrix in GMP_PREC precision and writes
 * 	them in double precision into a file named (TFILE)_(N)_(z).dat in a folder determined
 * 	by the path parameter (z is taken with 2 decimal values after the dot).
 * 	Filename TFILE is a constant defined in translational_matrix.h.
 * 	Note that file written for a matrix calculated up to order N
 * 	can be used to fill matrices for all orders not greater than N,
 * 	so that's a good idea to tabulate the matrices calculated up to the highest
 * 	order you might further need and reuse these files.
 * 	\param N	Highest polynomial order, for which matrix elements will be calculated and stored.
 * 	\param lambda	Lambda scaling factor
 * 	\param z 	Translation distance, for which the matrix will be caclulated.
 * 	\param path Path to the folder, in which (TFILE)_(N)_(z).dat file will be created.
 */
void fmft_tmatrix_tabulate(
			const int N,
			const float lambda,
			const double z,
			const char* path);

/**
 * 	Checks for existence of a file named (TFILE)_(lambda)_(Nmax)_(z).dat
 * 	in folder determined by the path parameter and, if such file exists,
 * 	tries to fill a pre-allocated instance of fmft_tmatrix struct
 * 	according to the context of this file. Note that the polynomial order
 * 	of the preallocated matrix can be lower thab Nmax, and in such case
 * 	only the elements up to the order of T will be read from the file.
 * 	\param T	Pointer to a pre-allocated instance of fmft_tmatr struct.
 * 	\param Nmax	The polynomial order for which the matrix was tabulated.
 * 	\param lambda	Lambda scaling factor
 * 	\param z	Translation distance.
 * 	\param path Path to the folder, in which the (TFILE)_(lambda)_(Nmax)_(z).dat file is located.
 */
void fmft_tmatrix_load(
			struct fmft_tmatrix* T,
			const int Nmax,
			const float lambda,
			const double z,
			const char *path);

/**
 * 	Creates an instance of fmft_coeffset and
 * 	fills it with the result of multiplication of a set of SPF
 * 	coefficients stored in coeff by a translation matrix T
 * 	(Which effectively corresponds to translation of the property represented by the coefficients
 * 	along z axis by distance z for which the translation matrix was calculated).
 * 	Note that the highest polynomial order of a set of SPF coefficients N is supposed to be the same as
 * 	the highest polynomial order of translation matrix.
 * 	\param coeff	Pointer to an instance of fmft_coeffset struct, storing a set of SPF coefficients to be rotated.
 * 	\param T 	Pointer to an instance of an fmft_tmatrix, storing elements translation matrix.
 * 	\return Pointer to the instance of fmft_coeffset struct, filled with "rotated" coefficients.
 */
struct fmft_coeffset* fmft_coeffset_translate(
					const struct fmft_coeffset* coeff,
					const struct fmft_tmatrix* T);

#ifdef __cplusplus
}
#endif

///////////////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.          //
///////////////////////////////////////////////////////////////////////////////

/** @}*/
