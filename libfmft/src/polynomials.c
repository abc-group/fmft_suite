/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include "polynomials.h"

double* fmft_poly_array_harmonic_oscillator_norm(
					const int N,
					const float k)
{
	double* norm = (double*) calloc(N*N, sizeof(double));

	norm[fmft_index_nl(N, 1, 0)] = sqrt(2.00);
	for (int n = 2; n <= N; ++n) {
		norm[fmft_index_nl(N, n, 0)] = norm[fmft_index_nl(N, n - 1, 0)] * sqrt((n - 1.00) / (n - 1.00 / 2.00));
		for (int l = 1; l < n; ++l) {
			norm[fmft_index_nl(N, n, l)] = norm[fmft_index_nl(N, n, l-1)]/sqrt( k*(n-l) );
		}
	}

	return norm;
}

void fmft_poly_array_harmonic_oscillator(
					double* R,
					const double* norm_array,
					const int N,
					const float r,
					const float lambda)
{
	//TODO change to normal M_PI
	const float PI = 3.14159265358979323846;
	float c;
	float rho = r * r / lambda;

	double* L;
	L = fmft_poly_array_laguerre(N, rho, 0.5);

	c = sqrt(2.00 / sqrt(PI * lambda * lambda * lambda)) * exp(-rho / 2.00);
	for (int n = 1; n <= N; ++n) {
		double pow_r = 1;
		for(int l = 0; l < n; ++l) {
			R[fmft_index_nl(N, n, l)] = c * pow_r * norm_array[fmft_index_nl(N, n, l)] * L[fmft_index_laguerre(N, n - l - 1, l)];
			pow_r *= r;
		}
	}
	free(L);

	return;
}

double* fmft_poly_array_spherical_harmonic_norm(const int N)
{
	double *Y_norm;
	Y_norm = (double*) calloc (N * (2 * N - 1), sizeof(double));

	for (int l = 0; l < N; ++l) {
		double fact = 1.;
		double tmp0 = (2 * l + 1.) / (4. * M_PI);
		for(int m = 0; m <= l; ++m) {
			if (m > 0) fact *= (l - m + 1) * (l + m);
			double tmp = sqrt(tmp0/fact);
			double sgn = (m % 2 == 0) ? (1) : (-1);
			Y_norm[fmft_index_lm(N, l,  m)] =         tmp;
			Y_norm[fmft_index_lm(N, l, -m)] =   sgn * tmp;
		}
	}

	return Y_norm;
}

void fmft_poly_array_associated_legendre(
					double* P,
					const int N,
					const double x)
{
	double y = sqrt(1.-x*x);

	P[fmft_index_lm(N, 0, 0)] = 1;//initialization
	if (N == 1)
		return;

	P[fmft_index_lm(N, 1, 0)] = x;
	for (int l = 2; l < N; ++l) {  //First recursion for m=0 values.
		P[fmft_index_lm(N,l,0)] = (2.*l-1.)/l*x*P[fmft_index_lm(N,(l-1),0)] - (l-1.)/l*P[fmft_index_lm(N,(l-2),0)];
	}

	for (int m = 1; m < N - 1; ++m) {
		P[fmft_index_lm(N, m, m)] = -1.*(2.*m-1.)*y*P[fmft_index_lm(N,(m-1),(m-1))]; //The P_m^m quantities
		P[fmft_index_lm(N,(m+1), m)] =  (2.*m+1.)*x*P[fmft_index_lm(N,m,m)];//The P_m+1^m quantities.

		for (int l = m + 2; l < N; ++l) {  //Recursion for the rest of P_l^m
			P[fmft_index_lm(N,l,m)] = (2.*l-1.)/(l-m)*x*P[fmft_index_lm(N,(l-1),m)] - (l+m-1.)/(l-m)*P[fmft_index_lm(N,(l-2),m)];
		}
	}

	P[fmft_index_lm(N,(N-1),(N-1))] = -1.*(2.*(N-1.)-1.)*y*P[fmft_index_lm(N,(N-2),(N-2))]; //The P_bw-1^(+/-)bw-1 quantities

	return;
}

double* fmft_poly_array_laguerre(
				const int N,
				const float x,
				const float alpha_min)
{
	double* L = (double*) malloc((N + 1) * N / 2 * sizeof(double)); //The saved (outputed) Laguerre polynomials.

	float alpha;

	//First determine all L_0's ( n-l-1 = 0 for different l's)
	for (int l = 0; l < N; ++l) {
		L[fmft_index_laguerre(N, 0, l)] = 1;
	}

	//And now all L_1's ( n-l-1 = 1 for l's up to and including N-2)
	for (int l = 0; l < N-1; ++l) {
		L[fmft_index_laguerre(N, 1, l)] = -x + alpha_min + l + 1.0000;
	}

	for (int lower = 2; lower < N; ++lower) {
		for(int l=0; l < N - lower; ++l) {
			alpha = alpha_min + l;
			L[fmft_index_laguerre(N, lower, l)] = 1.0000/lower *
							(
								 (2.0000*lower-1.0000+alpha-x)*L[fmft_index_laguerre(N, lower-1, l)]
									- (lower-1.0000+alpha)*L[fmft_index_laguerre(N, lower-2, l)]
							);
		}
	}

	return L;
}
