/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include "grid.h"

struct fmft_grid_3D* fmft_grid_3D_create(int NX, int NY, int NZ)
{
	struct fmft_grid_3D* g = (struct fmft_grid_3D*) calloc(1, sizeof(struct fmft_grid_3D));

	g->shape.X = NX;
	g->shape.Y = NY;
	g->shape.Z = NZ;

	g->delta.X = 1.0;
	g->delta.Y = 1.0;
	g->delta.Z = 1.0;

	g->origin.X = 0.0;
	g->origin.Y = 0.0;
	g->origin.Z = 0.0;

	g->data = (float*) calloc(NX * NY * NZ, sizeof(float));

	return g;
}

void fmft_grid_3D_free(struct fmft_grid_3D *g)
{
	if (g != NULL) {
		if (g->data != NULL)
			free(g->data);
		free(g);
	}
}

struct fmft_grid_3D_list* fmft_grid_3D_list_create(int count)
{
	struct fmft_grid_3D_list* mg = (struct fmft_grid_3D_list*) calloc(1, sizeof(struct fmft_grid_3D_list));
	mg->count = count;
	mg->A = (struct fmft_grid_3D**) calloc(count, sizeof(struct fmft_grid_3D*));

	return mg;
}

void fmft_grid_3D_list_free(struct fmft_grid_3D_list *gl)
{
	if (gl != NULL) {
		for (int i = 0; i < gl->count; i++) {
			fmft_grid_3D_free(gl->A[i]);
		}
		free(gl->A);
		free(gl);
	}
}

enum header_state {
	DX_GRIDPOSITIONS,
	DX_ORIGIN,
	DX_DELTA,
	DX_GRIDCONNECTIONS,
	DX_ARRAY,
	DX_DONE
};

static void grid_3D_read_header(FILE* fp,
				const char* filename,
				struct fmft_vector_3i* shape,
				struct fmft_vector_3f* delta,
				struct fmft_vector_3f* origin)
{
	// skip comment lines
	char *str = NULL;
	size_t str_len = 0;

	float deltas[3][3];
	size_t delta_counter = 0;

	enum header_state state = DX_GRIDPOSITIONS;
	while (getline(&str, &str_len, fp)) {
		if (fmft_is_whitespace_string(str))
			continue;

		if (str[0] == '#')
			continue;

		switch(state) {
			case DX_GRIDPOSITIONS:
				if ((sscanf(str, "object 1 class gridpositions counts %d %d %d",
						    &shape->X,
						    &shape->Y,
						    &shape->Z) != 3)) {
					printf("Error reading dx file %s\n", filename);
					exit(EXIT_FAILURE);
				}
				state = DX_ORIGIN;
				break;
			case DX_ORIGIN:
				if ((sscanf(str, "origin %f %f %f",
							&origin->X,
							&origin->Y,
							&origin->Z) != 3)) {
					printf("Error reading dx file %s\n", filename);
					exit(EXIT_FAILURE);
				}
				state = DX_DELTA;
				break;
			case DX_DELTA:
				if (sscanf(str, "delta %f %f %f",
						    &deltas[delta_counter][0],
						    &deltas[delta_counter][1],
						    &deltas[delta_counter][2]) != 3) {
					printf("Error reading dx file %s\n", filename);
					exit(EXIT_FAILURE);
				}
				delta_counter++;
				if (delta_counter == 3)
					state = DX_GRIDCONNECTIONS;
				break;
			case DX_GRIDCONNECTIONS:
				state = DX_ARRAY;
				break;
			case DX_ARRAY:
				state = DX_DONE;

		}

		if (state == DX_DONE) {
			delta->X = deltas[0][0];
			delta->Y = deltas[1][1];
			delta->Z = deltas[2][2];
			break;
		}
	}

	if (str != NULL)
		free(str);
	if (state != DX_DONE) {
		printf("Error reading dx file %s\n", filename);
		exit(EXIT_FAILURE);
	}
}

struct fmft_grid_3D* fmft_grid_3D_read(const char* filename)
{
	FILE* fp = fopen(filename, "r");
	if (fp == NULL) {
		fprintf(stderr, "fopen error on %s:\n", filename);
		exit(EXIT_FAILURE);
	}

	struct fmft_vector_3i shape;
	struct fmft_vector_3f delta;
	struct fmft_vector_3f origin;
	grid_3D_read_header(fp, filename, &shape, &delta, &origin);

	struct fmft_grid_3D *g;
	g = fmft_grid_3D_create(shape.X, shape.Y, shape.Z);

	g->delta.X = delta.X;
	g->delta.Y = delta.Y;
	g->delta.Z = delta.Z;

	g->origin.X = origin.X;
	g->origin.Y = origin.Y;
	g->origin.Z = origin.Z;

	// read grid values:
	for (int i = 0; i < shape.X; ++i) {
		for (int j = 0; j < shape.Y; ++j) {
			for (int k = 0; k < shape.Z; ++k) {
				if (fscanf(fp, "%f", &g->data[fmft_index_grid_3D(shape, i, j, k)]) < 1) {
					printf("Error reading dx file %s\n", filename);
					exit(EXIT_FAILURE);
				}
			}
		}
	}

	// We ignore the footer of the dx file,
	fclose(fp);

	return g;
}

struct fmft_grid_3D_list* fmft_grid_3D_list_read(const char* filename)
{
	FILE* fp = fopen(filename, "r");
	if (fp == NULL)	{
		fprintf(stderr, "fopen error on %s:\n", filename);
		exit(EXIT_FAILURE);
	}

	int gl_count;
	fscanf(fp, "%d", &gl_count);
	struct fmft_grid_3D_list* gl = fmft_grid_3D_list_create(gl_count);
	for (int i = 0; i < gl->count; ++i) {
		char A_file[1024];
		fscanf(fp, "%s", A_file);
		gl->A[i] = fmft_grid_3D_read(A_file);
	}
	fclose(fp);

	//check that all grids have the same size and origin
	for (int i = 1; i < gl->count; ++i) {
		if ((gl->A[i]->shape.X != gl->A[0]->shape.X) ||
			(gl->A[i]->shape.Y != gl->A[0]->shape.Y) ||
			(gl->A[i]->shape.Z != gl->A[0]->shape.Z) ||
			(gl->A[i]->delta.X != gl->A[0]->delta.X) ||
			(gl->A[i]->delta.Y != gl->A[0]->delta.Y) ||
			(gl->A[i]->delta.Z != gl->A[0]->delta.Z) ||
			(gl->A[i]->origin.X != gl->A[0]->origin.X) ||
			(gl->A[i]->origin.Y != gl->A[0]->origin.Y) ||
			(gl->A[i]->origin.Z != gl->A[0]->origin.Z)) {
			printf("Grids in the set do not have the same size and/or origin");
			exit(EXIT_FAILURE);
		}
	}

	return gl;
}



static void grid_3D_write_header(const struct fmft_grid_3D* g, FILE* fp)
{
	fprintf(fp, "# DX density grid\n");

	fprintf(fp, "object 1 class gridpositions counts %d %d %d\n",
			g->shape.X, g->shape.Y, g->shape.Z);

	fprintf(fp, "origin  %.3f\t%.3f\t%.3f\n",
			g->origin.X, g->origin.Y, g->origin.Z);

	fprintf(fp, "delta  %.3f\t0\t0\n", g->delta.X);
	fprintf(fp, "delta  0\t%.3f\t0\n", g->delta.Y);
	fprintf(fp, "delta  0\t0\t%.3f\n", g->delta.Z);

	fprintf(fp, "object 2 class gridconnections counts %d %d %d\n",
			g->shape.X, g->shape.Y, g->shape.Z);

	fprintf(fp, "object 3 class array type double rank 0 items %d data follows\n",
			g->shape.X * g->shape.Y * g->shape.Z);

}

static void grid_3D_write_footer(FILE* fp)
{
	fprintf(fp, "attribute \"dep\" string \"positions\"\n");
	fprintf(fp, "object \"regular positions regular connections\" class field\n");
	fprintf(fp, "component \"positions\" value 1\n");
	fprintf(fp, "component \"connections\" value 2\n");
	fprintf(fp, "component \"data\" value 3\n");
}


void fmft_grid_3D_write(const struct fmft_grid_3D* g, const char* filename)
{
	FILE* fp = fopen(filename, "w");
	if (fp == NULL) {
		fprintf(stderr, "fopen error on %s:\n", filename);
		exit(EXIT_FAILURE);
	}

	grid_3D_write_header(g, fp);

	float val;
	for (int i = 0; i < g->shape.X; ++i) {
		for (int j = 0; j < g->shape.Y; ++j) {
			for (int k = 0; k < g->shape.Z; ++k) {
				val = g->data[k + g->shape.Z * (j + (g->shape.Y * i))];
				fprintf (fp, "%.6e\n", val);
			}
		}
	}

	grid_3D_write_footer(fp);

	fclose(fp);
}
