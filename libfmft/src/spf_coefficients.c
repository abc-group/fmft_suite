/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include "spf_coefficients.h"

struct fmft_coeffset* fmft_coeffset_create(int N)
{
	struct fmft_coeffset *coeff = (struct fmft_coeffset*) calloc(1, sizeof(struct fmft_coeffset));
	coeff->N = N;
	coeff->re = (double*) calloc(N * N *(2 * N - 1), sizeof (double));
	coeff->im = (double*) calloc(N * N *(2 * N - 1), sizeof (double));

	return coeff;
}

struct fmft_coeffset_stack* fmft_coeffset_stack_create(int N, int grids_count)
{
	struct fmft_coeffset_stack* coeff_st;
	coeff_st = (struct fmft_coeffset_stack*) calloc(1, sizeof(struct fmft_coeffset_stack));
	coeff_st->N = N;
	coeff_st->count = grids_count;
	coeff_st->re = (double*) calloc(N * N * (2 * N - 1) * grids_count, sizeof (double));
	coeff_st->im = (double*) calloc(N * N * (2 * N - 1) * grids_count, sizeof (double));

	return coeff_st;
}

void fmft_coeffset_free(struct fmft_coeffset* coeff)
{
	free(coeff->re);
	free(coeff->im);
	free(coeff);
}

void fmft_coeffset_stack_free(struct fmft_coeffset_stack* coeff_st)
{
	free(coeff_st->re);
	free(coeff_st->im);
	free(coeff_st);
}

struct fmft_coeffset* fmft_coeffset_read(const char *filename)
{
	int N;
	struct fmft_coeffset* coeff;

	FILE *fp = fopen(filename, "r");
	if (fp == NULL)	{
		fprintf(stderr, "File %s could not be opened.\n", filename);
		exit(EXIT_FAILURE);
	}

	if (fscanf(fp, "%d", &N) != 1) {
		fprintf(stderr, "Error on reading file %s.\n", filename);
		exit(EXIT_FAILURE);
	}
	coeff = fmft_coeffset_create(N);

	if (fscanf(fp, "%f", &coeff->lambda) != 1) {
		fprintf(stderr, "Error on reading file %s.\n", filename);
		exit(EXIT_FAILURE);
	}

	if (fscanf(fp, "%f %f %f\n",
			&coeff->origin.X,
			&coeff->origin.Y,
			&coeff->origin.Z) != 3) {
		fprintf(stderr, "Error on reading file %s.\n", filename);
		exit(EXIT_FAILURE);
	}

	for (int n = 1; n <= N; ++n) {
		for (int l = 0; l < n; ++l) {
			for (int m = -l; m <= l; ++m) {
				if (fscanf(fp, "%lf %lf",
						&coeff->re[fmft_index_coeffset(N, n, l, m)],
						&coeff->im[fmft_index_coeffset(N, n, l, m)]) != 2 )	{
					printf("Read error on file %s\n", filename);
					exit(EXIT_FAILURE);
				}
			}
		}
	}

	fclose(fp);

	return coeff;
}

void fmft_coeffset_write(const struct fmft_coeffset* coeff, const char *filename)
{
	int N = coeff->N;

	FILE *fp = fopen(filename, "w");
	if (fp == NULL)	{
		fprintf(stderr, "File %s could not be opened.\n", filename);
		exit(EXIT_FAILURE);
	}

	fprintf(fp, "%d\n", coeff->N);
	fprintf(fp, "%.2f\n", coeff->lambda);
	fprintf(fp, "%.7E %.7E %.7E\n",
			coeff->origin.X,
			coeff->origin.Y,
			coeff->origin.Z);

	for (int n = 1; n <= N; ++n) {
		for (int l = 0; l < n; ++l) {
			for (int m = -l; m <= l; ++m) {
				fprintf(fp, "%.15lf %.15lf\n",
						coeff->re[fmft_index_coeffset(N, n, l, m)],
						coeff->im[fmft_index_coeffset(N, n, l, m)]);
			}
		}
	}

	fclose(fp);
}

struct fmft_coeffset_stack* fmft_coeffset_stack_read(const char *filename)
{
	struct fmft_coeffset_stack* coeff_st;
	int N;
	int count;

	FILE *fp = fopen(filename, "r");
	if (fp == NULL)	{
		fprintf(stderr, "File %s could not be opened.\n", filename);
		exit(EXIT_FAILURE);
	}

	if (fscanf(fp, "%d %d\n", &N, &count) != 2) {
		fprintf(stderr, "Error on reading file %s.\n", filename);
		exit(EXIT_FAILURE);
	}

	coeff_st = fmft_coeffset_stack_create(N, count);

	if (fscanf(fp, "%f\n", &coeff_st->lambda) != 1) {
		fprintf(stderr, "Error on reading file %s.\n", filename);
		exit(EXIT_FAILURE);
	}

	if (fscanf(fp, "%f %f %f\n",
				&coeff_st->origin.X,
				&coeff_st->origin.Y,
				&coeff_st->origin.Z) != 3) {
		fprintf(stderr, "Error on reading file %s.\n", filename);
		exit(EXIT_FAILURE);
	}

	for (int p = 0; p < coeff_st->count; ++p) {
		for (int n = 1; n <= coeff_st->N; ++n) {
			for (int l = 0; l < n; ++l) {
				for (int m = -l; m <= l; ++m) {
					if (2 != fscanf(fp, "%lf %lf\n",
							&coeff_st->re[fmft_index_coeffset_stack(coeff_st->N, coeff_st->count, p, n, l, m)],
							&coeff_st->im[fmft_index_coeffset_stack(coeff_st->N, coeff_st->count, p, n, l, m)])) {
						fprintf(stderr, "Error on reading file %s.\n", filename);
						fclose(fp);
						exit(EXIT_FAILURE);
					}
				}
			}
		}
	}

	fclose(fp);

	return coeff_st;
}

struct fmft_coeffset_stack* fmft_coeffset_stack_get_low_order(const struct fmft_coeffset_stack* coeff_st, int N)
{
	if (N > coeff_st->N) {
		fprintf(stderr, "Polinomial order if the output set can't be higher than that of the input set");
		exit(1);
	}

	struct fmft_coeffset_stack* coeff_st_new;
	coeff_st_new = fmft_coeffset_stack_create(N, coeff_st->count);

	coeff_st_new->lambda   = coeff_st->lambda;
	coeff_st_new->origin.X = coeff_st->origin.X;
	coeff_st_new->origin.Y = coeff_st->origin.Y;
	coeff_st_new->origin.Z = coeff_st->origin.Z;

	for (int p = 0; p < coeff_st->count; ++p) {
		for (int n = 1; n <= coeff_st_new->N; ++n) {
			for (int l = 0; l < n; ++l) {
				for (int m = -l; m <= l; ++m) {
					coeff_st_new->re[fmft_index_coeffset_stack(coeff_st_new->N, coeff_st_new->count, p, n, l, m)] =
							coeff_st->re[fmft_index_coeffset_stack(coeff_st->N, coeff_st->count, p, n, l, m)];
					coeff_st_new->im[fmft_index_coeffset_stack(coeff_st_new->N, coeff_st_new->count, p, n, l, m)] =
							coeff_st->im[fmft_index_coeffset_stack(coeff_st->N, coeff_st->count, p, n, l, m)];
				}
			}
		}
	}

	return coeff_st_new;
}

void fmft_coeffset_stack_write(const struct fmft_coeffset_stack* coeff_st, const char *filename)
{
	int N = coeff_st->N;

	FILE *fp = fopen(filename, "w");
	if (fp == NULL) {
		fprintf(stderr, "File %s could not be opened.\n", filename);
		exit(EXIT_FAILURE);
	}

	fprintf(fp, "%d %d\n", coeff_st->N, coeff_st->count);
	fprintf(fp, "%.2f\n", coeff_st->lambda);
	fprintf(fp, "%.7E %.7E %.7E\n",
			coeff_st->origin.X,
			coeff_st->origin.Y,
			coeff_st->origin.Z);

	for (int p = 0; p < coeff_st->count; ++p) {
		for (int n = 1; n <= N; ++n) {
			for (int l = 0; l < n; ++l) {
				for (int m = -l; m <= l; ++m) {
					fprintf(fp, "%.15E %.15E\n",
						coeff_st->re[fmft_index_coeffset_stack(N, coeff_st->count, p, n, l, m)],
						coeff_st->im[fmft_index_coeffset_stack(N, coeff_st->count, p, n, l, m)]);
				}
			}
		}
	}

	fclose(fp);
}

struct fmft_coeffset_stack* fmft_coeffset_stack_scale(const struct fmft_coeffset_stack* coeff_st, struct fmft_weightset* wset)
{
	if (wset->n != coeff_st->count) {
		fprintf(stderr, "The number of weights in a set is different form the number of coefficient sets");
		exit(EXIT_FAILURE);
	}

	struct fmft_coeffset_stack* coeff_st_new;
	coeff_st_new = fmft_coeffset_stack_create(coeff_st->N, coeff_st->count);

	coeff_st_new->lambda   = coeff_st->lambda;
	coeff_st_new->origin.X = coeff_st->origin.X;
	coeff_st_new->origin.Y = coeff_st->origin.Y;
	coeff_st_new->origin.Z = coeff_st->origin.Z;

	for (int p = 0; p < coeff_st->count; ++p) {
		for (int n = 1; n <= coeff_st_new->N; ++n) {
			for (int l = 0; l < n; ++l) {
				for (int m = -l; m <= l; ++m) {
					coeff_st_new->re[fmft_index_coeffset_stack(coeff_st_new->N, coeff_st_new->count, p, n, l, m)] =
							coeff_st->re[fmft_index_coeffset_stack(coeff_st->N, coeff_st->count, p, n, l, m)] *
							wset->vals[p];
					coeff_st_new->im[fmft_index_coeffset_stack(coeff_st_new->N, coeff_st_new->count, p, n, l, m)] =
							coeff_st->im[fmft_index_coeffset_stack(coeff_st->N, coeff_st->count, p, n, l, m)] *
							wset->vals[p];
				}
			}
		}
	}

	return coeff_st_new;
}


struct fmft_weightset* fmft_weightset_create(int n)
{
	struct fmft_weightset* wset = (struct fmft_weightset*) calloc(1, sizeof(struct fmft_weightset));
	wset->n = n;
	wset->vals = (double*) calloc(n, sizeof(double));

	return wset;
}

struct fmft_weightset_list* fmft_weightset_list_create(int count)
{
	struct fmft_weightset_list* wlist = (struct fmft_weightset_list*) calloc(1, sizeof(struct fmft_weightset_list));
	wlist->count = count;
	wlist->wset = (struct fmft_weightset**) calloc(count, sizeof(struct fmft_weightset*));

	return wlist;
}

void fmft_weightset_free(struct fmft_weightset* wset)
{
	free(wset->vals);
	free(wset);
}

void fmft_weightset_list_free(struct fmft_weightset_list* wlist)
{
	for (int i = 0; i < wlist->count; ++i) {
		fmft_weightset_free(wlist->wset[i]);
	}
	free(wlist->wset);
	free(wlist);
}

struct fmft_weightset_list* fmft_weightset_list_read(char* path)
{
	int count, n;
	FILE *fp = fopen(path, "r");
	if (fp == NULL)	{
		fprintf(stderr, "File %s could not be opened.\n", path);
		exit(EXIT_FAILURE);
	}

	// get number of coefficient sets
	count = fmft_count_lines (fp);
	struct fmft_weightset_list* wlist = fmft_weightset_list_create(count);

	//read file line by line
	char line[1024];
	//read weight sets, one by one
	for (int i = 0; i < wlist->count; ++i)
	{
		fgets(line, 1023, fp);
		n = (fmft_count_words(line) - 1);
		wlist->wset[i] = fmft_weightset_create (n);

		//read set label
		char *pch;
		pch = strtok(line, " \t\n");
		wlist->wset[i]->label = atoi(pch);
		//read coefficients one by one
		for (int j = 0; j < wlist->wset[i]->n; ++j) {
			pch = strtok(NULL, " \t\n");
			wlist->wset[i]->vals[j] = atof(pch);
		}
	}
	fclose(fp);

	return wlist;
}

struct fmft_coeffset* fmft_coeffset_from_grid(
				const struct fmft_grid_3D* g,
				const int N,
				const float lambda,
				const struct fmft_vector_3f* origin,
				const float r_cutoff)
{
	struct fmft_coeffset* coeffs;
	coeffs = fmft_coeffset_create(N);

	coeffs->lambda = lambda;

	// This weird piece of code had to be introduced to ensure that
	// the results stay numerically consistent with the previous version of the code.
	struct fmft_vector_3f center;
	if (origin != NULL) {
		coeffs->origin = *origin;
		center.X = coeffs->origin.X - g->origin.X;
		center.Y = coeffs->origin.Y - g->origin.Y;
		center.Z = coeffs->origin.Z - g->origin.Z;
	} else {
		center.X = ceil(g->shape.X / 2) * g->delta.X;
		center.Y = ceil(g->shape.Y / 2) * g->delta.Y;
		center.Z = ceil(g->shape.Z / 2) * g->delta.Z;
		coeffs->origin.X = center.X + g->origin.X;
		coeffs->origin.Y = center.Y + g->origin.Y;
		coeffs->origin.Z = center.Z + g->origin.Z;
	}

	double d = g->delta.X * g->delta.Y * g->delta.Z;
	double X, Y, Z;

	double *Y_norm, *P, *R_norm, *R;
	double theta, phi, r;
	R = (double*) calloc(N * N, sizeof (double));
	P = (double*) calloc(N * (2 * N - 1), sizeof (double));

	Y_norm = fmft_poly_array_spherical_harmonic_norm(N);

	R_norm = fmft_poly_array_harmonic_oscillator_norm(N, lambda);

	double* re = coeffs->re;
	double* im = coeffs->im;
	for (int x = 0; x < g->shape.X; ++x)
	{
		X = x * g->delta.X - center.X;
		for (int y = 0; y < g->shape.Y; ++y)
		{
			Y = y * g->delta.Y - center.Y;
			for (int z = 0; z < g->shape.Z; ++z)
			{
				Z = z * g->delta.Z - center.Z;

				r     = sqrt (X*X + Y*Y + Z*Z);
				if (r_cutoff > 0 && r > r_cutoff)
					continue;

				double g_re = g->data[fmft_index_grid_3D(g->shape, x, y, z)];
				if (g_re == 0)
					continue;


				theta = fmft_spherical_coord_theta_from_zr(Z, r);
				phi   = fmft_spherical_coord_phi_from_xy(X, Y) * 2*M_PI;

				fmft_poly_array_associated_legendre(P, N, cos(theta));
				fmft_poly_array_harmonic_oscillator(R, R_norm, N, r, lambda);

				for (int m = 0; m <= N-1; ++m)
				{
					double cos_mf = cos (m*phi);
					double sin_mf = sin (m*phi);
					//int ind0 = N*N*(m + N - 1);
					for (int l = abs(m); l < N; ++l)
					{
						double dYP = d * Y_norm[fmft_index_lm(N, l, m)] * P[fmft_index_lm(N, l, m)];
						double yr = cos_mf * dYP;//y = exp(i * m * phi) * Y_norm * Plm
						double yi = sin_mf * dYP;

						double tmp_re = yr * g_re;
						double tmp_im = yi * g_re;

						for(int n = l + 1; n <= N; ++n)
						{
							re[fmft_index_coeffset(N, n, l, m)] += R[fmft_index_nl(N, n, l)] * tmp_re;
							im[fmft_index_coeffset(N, n, l, m)] += R[fmft_index_nl(N, n, l)] * tmp_im;
						}
					}
				}
			}
		}
	}

	for (int m = 0; m <= N-1; ++m)
	{
		for (int l = abs(m); l < N; ++l)
		{
			for (int n = l+1; n <= N; ++n)
			{
				double sgn = (m % 2 == 0) ? (1) : (-1);
				re[fmft_index_coeffset(N, n, l, -m)] =  sgn * re[fmft_index_coeffset(N, n, l, m)];
				im[fmft_index_coeffset(N, n, l, -m)] = -sgn * im[fmft_index_coeffset(N, n, l, m)];
			}
		}
	}
	free(R);
	free(P);
	free(Y_norm);
	free(R_norm);

	return coeffs;
}

struct fmft_coeffset_stack* fmft_coeffset_stack_from_grids(
					const struct fmft_grid_3D_list* gs,
					const int N,
					const float lambda,
					const struct fmft_vector_3f* origin,
					const float r_cutoff)
{
	struct fmft_coeffset_stack* coeff_st;
	if (gs->count <= 0) {
		printf ("Number of grid pairs in a set should be greater then zero!\n Exiting..n");
		exit(1);
	}
	coeff_st = fmft_coeffset_stack_create(N, gs->count);
	coeff_st->lambda = lambda;

	struct fmft_coeffset* coeff_tmp;
	for (int i = 0; i < gs->count; ++i) {
		clock_t start, end;
		start = clock();
		printf("Convert grid %d of %d\n", i, gs->count);
		coeff_tmp = fmft_coeffset_from_grid(gs->A[i], N, lambda, origin, r_cutoff);
		end = clock();
		printf("Transform time: %f\n", 1.0f * (end - start) / CLOCKS_PER_SEC);
		/** optimize SPF coeffitients storage **/
		/**        cache optimization         **/
		for (int m = -N + 1; m <= N - 1; ++m){
			for (int l = abs(m); l < N; ++l) {
				for (int n = l + 1; n <= N; ++n) {
					coeff_st->re[fmft_index_coeffset_stack(N, gs->count, i, n, l, m)]
					             = coeff_tmp->re[fmft_index_coeffset(N, n, l, m)];
					coeff_st->im[fmft_index_coeffset_stack(N, gs->count, i, n, l, m)]
					             = coeff_tmp->im[fmft_index_coeffset(N, n, l, m)];
				}
			}
		}
		if (i == 0)
			coeff_st->origin = coeff_tmp->origin;

		fmft_coeffset_free(coeff_tmp);
	}

	//set transform origin (assumed to be the same for all grids):
	/*
	coeff_st->origin.X = (gs->A[0]->origin.X + 0.5 * (float)(gs->A[0]->shape.X) * gs->A[0]->delta.X);
	coeff_st->origin.Y = (gs->A[0]->origin.Y + 0.5 * (float)(gs->A[0]->shape.Y) * gs->A[0]->delta.Y);
	coeff_st->origin.Z = (gs->A[0]->origin.Z + 0.5 * (float)(gs->A[0]->shape.Z) * gs->A[0]->delta.Z);
	*/
	return coeff_st;
}

struct fmft_grid_3D* fmft_coeffset_to_grid(
			const struct fmft_coeffset* coeff,
			const struct fmft_vector_3i* grid_shape,
			const struct fmft_vector_3f* grid_delta,
			const struct fmft_vector_3f* grid_origin)
{
	if (grid_shape == NULL) {
		fprintf(stderr, "[error][libfmft] grid_shape is NULL.");
		exit(EXIT_FAILURE);
	}

	struct fmft_grid_3D* g;
	g = fmft_grid_3D_create(grid_shape->X, grid_shape->Y, grid_shape->Z);

	if (grid_delta != NULL) {
		g->delta.X = grid_delta->X;
		g->delta.Y = grid_delta->Y;
		g->delta.Z = grid_delta->Z;
	} else {
		g->delta.X = 1.0;
		g->delta.Y = 1.0;
		g->delta.Z = 1.0;
	}

	if (grid_origin != NULL) {
		g->origin.X = grid_origin->X;
		g->origin.Y = grid_origin->Y;
		g->origin.Z = grid_origin->Z;
	} else {
		g->origin.X = coeff->origin.X - 0.5 * g->shape.X * g->delta.X;
		g->origin.Y = coeff->origin.Y - 0.5 * g->shape.Y * g->delta.Y;
		g->origin.Z = coeff->origin.Z - 0.5 * g->shape.Z * g->delta.Z;
	}

	double X, Y, Z;

	double *Y_norm, *P, *R_norm, *R;
	double theta, phi, r;
	int N = coeff->N;
	R = (double*) calloc(N*N, sizeof (double));
	P = (double*) calloc(N*(2*N - 1), sizeof (double));

	Y_norm = fmft_poly_array_spherical_harmonic_norm(N);
	R_norm = fmft_poly_array_harmonic_oscillator_norm(N, coeff->lambda);

	for (int i = 0; i < g->shape.X; ++i) {
		X = i * g->delta.X + g->origin.X - coeff->origin.X;
		for (int j = 0; j < g->shape.Y; ++j) {
			Y = j * g->delta.Y + g->origin.Y - coeff->origin.Y;
			for (int k = 0; k < g->shape.Z; ++k) {
				Z = k * g->delta.Z + g->origin.Z - coeff->origin.Z;
				double tmp = 0.0;

				r     = sqrt(X*X + Y*Y + Z*Z);
				theta = fmft_spherical_coord_theta_from_zr(Z, r);
				phi   = fmft_spherical_coord_phi_from_xy(X, Y) * 2 * M_PI;

				fmft_poly_array_associated_legendre(P, N, cos(theta));
				fmft_poly_array_harmonic_oscillator(R, R_norm, N, r, coeff->lambda);

				for (int m = -N + 1; m <= N - 1; ++m) {
					double cos_mf = cos(abs(m) * phi);
					double sin_mf = sin(abs(m) * phi);
					if (m < 0) {
						int sgn = (abs(m) % 2 == 0) ? (1) : (-1);
						cos_mf *=  sgn;
						sin_mf *= -sgn;
					}
					//int ind0 = N*N*(m + N - 1);
					for (int l = abs(m); l < N; ++l) {
						double YP = Y_norm[fmft_index_lm(N, l, abs(m))] * P[fmft_index_lm(N, l, abs(m))];
						double yr = cos_mf * YP;//y = exp(i * m * phi) * Y_norm * Plm
						double yi = sin_mf * YP;

						for (int n = l + 1; n <= N; ++n) {
							double coeff_re = coeff->re[fmft_index_coeffset(N, n, l, m)];
							double coeff_im = coeff->im[fmft_index_coeffset(N, n, l, m)];
							tmp += R[fmft_index_nl(N, n, l)] * (coeff_re * yr + coeff_im * yi);
						}
					}
				}
				g->data[fmft_index_grid_3D(g->shape, i, j, k)] = tmp;
			}
		}
	}

	free(R);
	free(P);
	free(Y_norm);
	free(R_norm);

	return g;
}

///////////////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.          //
///////////////////////////////////////////////////////////////////////////////
