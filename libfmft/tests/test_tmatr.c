#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>
#include "libfmft.h"
#include "gmpxx.h"

const double delta = 0.000001;
const double tolerance = 1e-3;
const double tolerance_strict = 1e-9;

// Test cases
START_TEST(test_tmatr_reading)
{
	int N = 15;
	struct fmft_tmatrix* T;
	T = fmft_tmatrix_allocate(N);
	fmft_tmatrix_read(T, "transMatrix_20.00_32_25.00.dat.ref");

	int n, n1, l, l1, m;
	n = 15; n1 = 14; l = 12; l1 = 5; m = 5;
	ck_assert(T->data[fmft_index_tmatrix(T->N, n, l, n1, l1, m)] - 7.137120155526020E-03 < 1e-14);

	n = 15; n1 = 15; l = 5; l1 = 12; m = 5;
	ck_assert(T->data[fmft_index_tmatrix(T->N, n, l, n1, l1, m)] + 7.137120155526020E-03 < 1e-14);

	n = 1; n1 = 1; l = 0; l1 = 0; m = 0;
	ck_assert(T->data[fmft_index_tmatrix(T->N, n, l, n1, l1, m)] - 4.046451693262645E-04 < 1e-14);

	n = 12; n1 = 13; l = 7; l1 = 8; m = 6;
	ck_assert(T->data[fmft_index_tmatrix(T->N, n, l, n1, l1, m)] - 7.686349386927227E-02 < 1e-14);

	fmft_tmatrix_free(T);
}
END_TEST

START_TEST(test_tmatr_compute)
{
	int N = 15;
	float lambda = 20.00;
	double z = 25.00;

	struct fmft_tmatrix_gmp* T_gmp;
	T_gmp = fmft_tmatrix_gmp_compute(N, lambda, z);

	struct fmft_tmatrix* T;
	T = fmft_tmatrix_from_gmp(T_gmp);
	fmft_tmatrix_gmp_free(T_gmp);

	struct fmft_tmatrix* T_ref;
	T_ref = fmft_tmatrix_allocate(N);
	fmft_tmatrix_read(T_ref, "transMatrix_20.00_32_25.00.dat.ref");

	// The first loop compares the potentially non-zero elements using the indexer-base access.
	double diff = 0;
	for (int n = 1; n <= T->N; ++n)	{
		for (int n1 = 1; n1 <= n; ++n1) {
			for (int l = 0; l < n; ++l) {
				for (int l1 = 0; l1 < n1; ++l1) {
					int m_max = (l < l1 ? l : l1);
					for (int m = 0; m <= m_max; ++m) {
						diff += fabs(	T->data[fmft_index_tmatrix(T->N, n, l, n1, l1, m)] -
								T_ref->data[fmft_index_tmatrix(T->N, n, l, n1, l1, m)]);
					}
				}
			}
		}
	}
	ck_assert(diff < 1e-14);

	// The second loop comares all elements.
	double diff2 = 0;
	for (size_t i = 0; i < N*N*N*N*N; ++i) {
		diff2 += fabs(T->data[i] - T_ref->data[i]);
	}
	ck_assert(diff2 < 1e-14);

	fmft_tmatrix_free(T);
	fmft_tmatrix_free(T_ref);
}
END_TEST


Suite *fmft_grid_suite(void)
{
	Suite *suite = suite_create("fmft_tmatr");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	//tcase_set_timeout(tcase, 30);
	tcase_add_test(tcase, test_tmatr_reading);
	tcase_add_test(tcase, test_tmatr_compute);


	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = fmft_grid_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}

