#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>
#include "libfmft.h"

const double delta = 0.000001;
const double tolerance = 1e-3;
const double tolerance_strict = 1e-5;

// Test cases
START_TEST(test_coeff_reading)
{
	struct fmft_coeffset* coeff;
	coeff = fmft_coeffset_read("1avx_helix_rvdw.coeff");

	ck_assert(coeff != NULL);
	ck_assert(coeff->N == 20);
	ck_assert(fabs(coeff->lambda - 20.00) < tolerance);
	ck_assert(fabs(coeff->origin.X - 2.1899000E+01) < tolerance);
	ck_assert(fabs(coeff->origin.Y - 6.1882000E+01) < tolerance);
	ck_assert(fabs(coeff->origin.Z - 9.1389000E+01) < tolerance);

	ck_assert(fabs(coeff->re[fmft_index_coeffset(coeff->N, 1, 0, 0)] -  (98.400512328523192)) < tolerance_strict);
	ck_assert(fabs(coeff->im[fmft_index_coeffset(coeff->N, 2, 1,-1)] - (-42.318483003366424)) < tolerance_strict);
	ck_assert(fabs(coeff->re[fmft_index_coeffset(coeff->N, 3, 1,-1)] -  (13.179250069152758)) < tolerance_strict);

	fmft_coeffset_free(coeff);
}
END_TEST

START_TEST(test_coeff_from_grid)
{
	struct fmft_grid_3D* g;
	g = fmft_grid_3D_read("1avx_helix_rvdw.dx");

	ck_assert(g != NULL);
	ck_assert(g->shape.X == 49);
	ck_assert(g->shape.Y == 49);
	ck_assert(g->shape.Z == 54);

	ck_assert(fabs(g->delta.X - 1.0) < tolerance);
	ck_assert(fabs(g->delta.Y - 1.0) < tolerance);
	ck_assert(fabs(g->delta.Z - 1.0) < tolerance);

	ck_assert(fabs(g->origin.X - (-2.101)) < tolerance);
	ck_assert(fabs(g->origin.Y - (37.882)) < tolerance);
	ck_assert(fabs(g->origin.Z - (64.389)) < tolerance);

	struct fmft_coeffset* coeff;
	coeff = fmft_coeffset_from_grid(g, 20, 20.0, NULL, -1);

	ck_assert(coeff != NULL);
	ck_assert(coeff->N == 20);
	ck_assert(fabs(coeff->lambda - 20.0) < tolerance);
	ck_assert(fabs(coeff->origin.X - 2.1899000E+01) < tolerance);
	ck_assert(fabs(coeff->origin.Y - 6.1882000E+01) < tolerance);
	ck_assert(fabs(coeff->origin.Z - 9.1389000E+01) < tolerance);

	struct fmft_coeffset*  coeff_ref;
	coeff_ref = fmft_coeffset_read("1avx_helix_rvdw.coeff");

	for (int n = 1; n <= coeff_ref->N; ++n) {
		for (int l = 0; l < n; ++l) {
			for (int m = -l; m <= l; ++m) {
				ck_assert(fabs(coeff->re[fmft_index_coeffset(coeff->N, n, l, m)] -
					coeff_ref->re[fmft_index_coeffset(coeff->N, n, l, m)]) < tolerance_strict);
				ck_assert(fabs(coeff->im[fmft_index_coeffset(coeff->N, n, l, m)] -
					coeff_ref->im[fmft_index_coeffset(coeff->N, n, l, m)]) < tolerance_strict);

			}
		}
	}

	fmft_grid_3D_free(g);
	fmft_coeffset_free(coeff);
	fmft_coeffset_free(coeff_ref);
}
END_TEST

START_TEST(test_coeff_to_grid)
{
	// Read a precomputed coeffset
	struct fmft_coeffset*  coeff;
	coeff = fmft_coeffset_read("1avx_helix_rvdw.coeff");

	ck_assert(coeff != NULL);
	ck_assert(coeff->N == 20);
	ck_assert(fabs(coeff->lambda - 20.0) < tolerance);
	ck_assert(fabs(coeff->origin.X - 2.1899000E+01) < tolerance);
	ck_assert(fabs(coeff->origin.Y - 6.1882000E+01) < tolerance);
	ck_assert(fabs(coeff->origin.Z - 9.1389000E+01) < tolerance);

	// Perform an inverse spf transform
	struct fmft_vector_3i g_shape = {49, 49, 49};
	struct fmft_vector_3f g_delta = {1.0, 1.0, 1.0};
	struct fmft_vector_3f g_origin = {-2.601, 37.382, 66.889};
	struct fmft_grid_3D* g;
	g = fmft_coeffset_to_grid(coeff, &g_shape, &g_delta, &g_origin);

	// Read in a reference grid and compare the transform results to it.
	struct fmft_grid_3D* g_ref;
	g_ref = fmft_grid_3D_read("1avx_helix_rvdw.inv.dx");

	ck_assert(g != NULL);
	ck_assert(g_ref != NULL);
	ck_assert(g->shape.X == g_ref->shape.X);
	ck_assert(g->shape.Y == g_ref->shape.Y);
	ck_assert(g->shape.Z == g_ref->shape.Z);

	ck_assert(fabs(g->delta.X - g_ref->delta.X) < tolerance);
	ck_assert(fabs(g->delta.Y - g_ref->delta.Y) < tolerance);
	ck_assert(fabs(g->delta.Z - g_ref->delta.Z) < tolerance);

	ck_assert(fabs(g->origin.X - g_ref->origin.X) < tolerance);
	ck_assert(fabs(g->origin.Y - g_ref->origin.Y) < tolerance);
	ck_assert(fabs(g->origin.Z - g_ref->origin.Z) < tolerance);


	for (int i = 0; i < g_ref->shape.X; ++i) {
		for (int j = 0; j < g_ref->shape.Y; ++j) {
			for (int k = 0; k < g_ref->shape.Z; ++k) {
				ck_assert(
					fabs(g->data[fmft_index_grid_3D(g->shape, i, j, k)] -
					g_ref->data[fmft_index_grid_3D(g_ref->shape, i, j, k)]) < delta * 10);

			}
		}
	}

	fmft_grid_3D_free(g);
	fmft_grid_3D_free(g_ref);
	fmft_coeffset_free(coeff);
}
END_TEST


Suite *fmft_grid_suite(void)
{
	Suite *suite = suite_create("fmft_coeff");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	tcase_add_test(tcase, test_coeff_reading);
	tcase_add_test(tcase, test_coeff_from_grid);
	tcase_add_test(tcase, test_coeff_to_grid);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = fmft_grid_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}

