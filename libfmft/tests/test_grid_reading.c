#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>
#include "libfmft.h"

const double delta = 0.000001;
const double tolerance = 1e-3;
const double tolerance_strict = 1e-9;

// Test cases
START_TEST(test_grid_reading)
{
	struct fmft_grid_3D* g = fmft_grid_3D_read("simple_grid.dx");

	ck_assert(g != NULL);
	ck_assert(g->shape.X == 3);
	ck_assert(g->shape.Y == 4);
	ck_assert(g->shape.Z == 5);

	ck_assert(fabs(g->delta.X - 1.2) < tolerance);
	ck_assert(fabs(g->delta.Y - 1.3) < tolerance);
	ck_assert(fabs(g->delta.Z - 1.4) < tolerance);

	ck_assert(fabs(g->origin.X - (-46.606)) < tolerance);
	ck_assert(fabs(g->origin.Y - (-22.236)) < tolerance);
	ck_assert(fabs(g->origin.Z - (-70.663)) < tolerance);

	int sum = 0;
	for (int i = 0; i < g->shape.X * g->shape.Y * g->shape.Z; ++i)
		sum += g->data[i];

	ck_assert(fabs(3.0 - sum) < tolerance);

	fmft_grid_3D_free(g);
}
END_TEST

Suite *fmft_grid_suite(void)
{
	Suite *suite = suite_create("fmft_grid");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	tcase_add_test(tcase, test_grid_reading);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = fmft_grid_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}

