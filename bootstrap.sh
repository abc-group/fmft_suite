#!/bin/bash

echo -e "\033[32mMake libfmft\033[0m"
cd libfmft
mkdir -p build
cd build
cmake -DCMAKE_INSTALL_PREFIX="../../install-local" -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=True ..
make
make install
cd ../..


echo -e "\033[32mMake fmft_core\033[0m"
cd fmft_core
mkdir -p build
cd build
cmake -DCMAKE_INSTALL_PREFIX="../../install-local" -DCMAKE_BUILD_TYPE=Release ..
make
make install
cd ../..


echo -e "\033[32mMake fmft_apps\033[0m"
cd fmft_apps
cp ./*.py "../install-local/bin/"
cp -r prms "../install-local/bin/"
cd ..

echo -e "\033[32mMake grid_gen\033[0m"
#cd libmol_dependent
cd grid_gen
./bootstrap_grid_gen.sh

echo -e "\033[32mDone!\033[0m"


