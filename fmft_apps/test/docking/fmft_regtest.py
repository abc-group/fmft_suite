#!/usr/bin/python

from sblu.ft import read_ftresults
from sblu.ft import read_rotations
import argparse
import json
from subprocess import call
import numpy as np

def cmp_ft_files_strict(ft_file_a, ft_file_b, limit=None):
    ftresults_a = read_ftresults(ft_file_a, limit)
    ftresults_b = read_ftresults(ft_file_b, limit)
    ndiff = 0;
    for i in range(len(ftresults_a)):
        if (ftresults_a[i]['roti'] != ftresults_b[i]['roti']):
            ndiff += 1
            continue
        if ((np.abs(ftresults_a[i]['tv'][0] - ftresults_b[i]['tv'][0]) > 0.1) or
            (np.abs(ftresults_a[i]['tv'][1] - ftresults_b[i]['tv'][1]) > 0.1) or
            (np.abs(ftresults_a[i]['tv'][2] - ftresults_b[i]['tv'][2]) > 0.1)):
            ndiff += 1
            continue
        if (np.abs(ftresults_a[i]['E'] - ftresults_b[i]['E']) > 0.1):
            ndiff += 1
            continue
    return ndiff

def cmp_rm_files_strict(rm_file_a, rm_file_b, limit=None):
    rm_a = read_rotations(rm_file_a, limit)
    rm_b = read_rotations(rm_file_b, limit)
    ndiff = 0;
    for i in range(len(rm_a)):
        diff = False
        for j in range(3):
            for k in range(3):
                if (np.abs(rm_a[i][j][k] - rm_b[i][j][k]) > 0.0001):
                    diff = True
        if diff == True:
            ndiff += 1
    return ndiff

def cmp_pwrmsd_files(pwrmsd_file_a, pwrmsd_file_b):
    pwrmsds_a = np.loadtxt(pwrmsd_file_a)
    pwrmsds_b = np.loadtxt(pwrmsd_file_b)
    diff = np.absolute(pwrmsds_a - pwrmsds_b)
    return diff.max()

def cmp_cluster_files(cluster_file_a, cluster_file_b):
    with open(cluster_file_a, "r") as f:
        cluster_data_a = json.load(f)
    with open(cluster_file_b, "r") as f:
        cluster_data_b = json.load(f)
    clusters_a = cluster_data_a["clusters"]
    clusters_b = cluster_data_b["clusters"]

    if (len(clusters_a) != len(clusters_b)):
        print("Different number of clusters in cluster files.")

    ntot = len(clusters_a)
    ndiff = 0
    for i in range(len(clusters_a)):
        if (clusters_a[i]["center"] != clusters_b[i]["center"]):
            ndiff += 1
        #if (len(clusters_a[i]["members"]) != len(clusters_b[i]["members"])):
        #    return False

    return ndiff, ntot

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--strict',
                        action='store_true')
    parser.add_argument('--nres',
                        type=int,
                        default=1000)
    args = parser.parse_args()

    if args.strict:
        # Check the coefficient file

        # Check ft files
        ft_file = 'ft.000.0.0'
        ft_file_ref = 'reference/ft.000.0.0'
        n_ft_diff =  cmp_ft_files_strict(ft_file, ft_file_ref, limit=args.nres)
        if (n_ft_diff != 0):
            print("FT\t files: (%d/%d) top results differ" 
                    % (n_ft_diff, args.nres))
        else:
            print("FT\t files: match strictly")

        # Check rm files
        rm_file = 'rm.000.0.0'
        rm_file_ref = 'reference/rm.000.0.0'
        n_rm_diff = cmp_rm_files_strict(rm_file, rm_file_ref, limit=args.nres)
        if (n_rm_diff != 0):
            print("RM\t files: (%d/%d) top results differ" 
                    % (n_rm_diff, args.nres))
        else:
            print("RM\t files: match strictly")

        # Check cluster files
        cluster_file = 'clusters.000.0.0.json'
        cluster_file_ref = 'reference/clusters.000.0.0.json'
        n_cl_diff = 0
        n_cl_diff , n_cl=  cmp_cluster_files(cluster_file, cluster_file_ref)
        if (n_cl_diff != 0):
            print("CLUSTER\t files: (%d/%d) cluster centers differ"
                    % (n_cl_diff, n_cl))
        else:
            print("CLUSTER\t files: match strictly")

    with open('evaluation.000.0.0', 'r') as f:
        evaluation = json.load(f)

    with open('reference/evaluation.000.0.0', 'r') as f:
        evaluation_ref = json.load(f)
    print("%10s\t%s\t%s" % ('--metric--', '--ref--', '--cur--'))
    for field in ['hits', 'rank', 'rmsd', 'population']:
        print("%10s\t%s\t%s" % (field,
                                str(evaluation_ref[field]),
                                str(evaluation[field])))

