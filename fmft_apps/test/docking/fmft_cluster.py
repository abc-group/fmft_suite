#!/usr/bin/python

#import json
import argparse
from subprocess import call

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('rec_pdb',
                        help='Receptor protein pdb file.',
                        type=str)
    parser.add_argument('lig_pdb',
                        help='Ligand protein pdb file.',
                        type=str)
    parser.add_argument('ft_file',
                        help='ft file produced by the fmft procedure',
                        type=str)
    parser.add_argument('rm_file',
                        help='rm file produced by the fmft procedure',
                        type=str)
    parser.add_argument('--pdb_models',
                        help='Build cluster pdb models.',
                        dest='pdb_models',
                        action='store_true')
    parser.add_argument('--no-pdb_models',
                        help='Don\'t build cluster pdb models.',
                        dest='pdb_models',
                        action='store_false')
    parser.set_defaults(pdb_models=True)
    parser.add_argument('--nres', '-n',
                        help='Number of top ftresults to process',
                        type=int,
                        default=1000)
    parser.add_argument('--out_suffix',
                        help='',
                        type=str,
                        default='XXX.X.X')

    args = parser.parse_args()

    pwrmsd_file = 'pwrmsd.' + args.out_suffix
    call(['sblu', 'measure', 'pwrmsd',
          '-n', str(args.nres),
          '--only-CA',
          '--only-interface',
          '--rec', args.rec_pdb,
          '-o', pwrmsd_file,
          args.lig_pdb,
          args.ft_file, args.rm_file])

    clusters_file = 'clusters.' + args.out_suffix + '.json'
    call(['sblu', 'docking', 'cluster',
          '--json',
          '-o', clusters_file,
          pwrmsd_file])

    if (args.pdb_models):
        call(['sblu', 'docking', 'gen_cluster_pdb',
              '-o', 'lig.' + args.out_suffix,
              clusters_file,
              args.ft_file,
              args.rm_file,
              args.lig_pdb])
