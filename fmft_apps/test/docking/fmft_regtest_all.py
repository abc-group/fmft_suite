#!/usr/bin/python

from sblu.ft import read_ftresults
from sblu.ft import read_rotations
import argparse
import json
import subprocess
from subprocess import call
import numpy as np

from contextlib import contextmanager
import os

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

def cleanup():
    file_list=['grids.list',
               'coeffstack.rec.0',
               'coeffstack.rec.0.000',
               'coeffstack.lig.0'
               'ft.000.0.0.lr',
               'rm.000.0.0.lr',
               'ft.000.0.0',
               'rm.000.0.0',
               'pwrmsd.000.0.0',
               'clusters.000.0.0.json',
               'rmsd.000.0.0',
               'evaluation.000.0.0']
    for f in file_list:
        if os.path.isfile(f):
            os.remove(f)


if __name__ == "__main__":

    fmft_dock_bin = "../../../../install-local/bin/fmft_dock.py"
    for case in ['1avx', '1gl1', '1hia', '7cei']:
        print("%s:" % case)
        with cd(case):
            rec_pdb = case + '_r_nmin.pdb'
            lig_pdb = case + '_l_nmin.pdb'
            ref_lig_pdb = case + '_l_nmin.pdb'
            weights_file = '../prms/fmft_weights_ei.txt'
            ft_file = 'ft.000.0.0'
            rm_file = 'rm.000.0.0'
            clusters_file = 'clusters.000.0.0.json'
             
            cleanup()
            call(['python', fmft_dock_bin,
                  rec_pdb,
                  lig_pdb,
                  weights_file])
                 #stdout=subprocess.STDOUT,
                 #stderr=subprocess.STDOUT)
            call(['python', '../fmft_cluster.py',
                  '--out_suffix', '000.0.0',
                  '--no-pdb_models',
                  rec_pdb,
                  lig_pdb,
                  ft_file,
                  rm_file])
                 #stdout=subprocess.STDOUT,
                 #stderr=subprocess.STDOUT)
            call(['python', '../fmft_evaluate.py',
                  '--out_suffix', '000.0.0',
                  rec_pdb,
                  lig_pdb,
                  ref_lig_pdb,
                  ft_file,
                  rm_file,
                  clusters_file])
                 #stdout=subprocess.STDOUT,
                 #stderr=subprocess.STDOUT)
            
            call(['python', '../fmft_regtest.py', '--strict'])

