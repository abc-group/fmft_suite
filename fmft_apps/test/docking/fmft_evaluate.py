#!/usr/bin/python

import json
import argparse
from subprocess import call
import numpy as np

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('rec_pdb',
                        help='Receptor protein pdb file.',
                        type=str)
    parser.add_argument('lig_pdb',
                        help='Ligand protein pdb file.',
                        type=str)
    parser.add_argument('ref_lig_pdb',
                        help='Ligand structure in bound orientation',
                        type=str)
    parser.add_argument('ft_file',
                        help='ft file produced by the fmft procedure',
                        type=str)
    parser.add_argument('rm_file',
                        help='rm file produced by the fmft procedure',
                        type=str)
    parser.add_argument('cluster_file',
                        help='Clustered docking poses',
                        type=str)
    parser.add_argument('--out_suffix',
                        help='',
                        type=str,
                        default='XXX.X.X')

    args = parser.parse_args()

    ftrmsd_file = 'rmsd.' + args.out_suffix
    call(['sblu', 'measure', 'ftrmsd',
          '-n', '1000',
          '--only-CA',
          '--only-interface',
          '--rec', args.rec_pdb,
          '-o', ftrmsd_file,
          args.lig_pdb,
          args.ref_lig_pdb,
          args.ft_file, args.rm_file])

    eval_results = {'hits': 0,
                    'rank': '-',
                    'rmsd': '-',
                    'population': '-'}

    ftrmsd = np.loadtxt(ftrmsd_file)
    n_hits = (ftrmsd < 10.0).sum()
    eval_results['hits'] = int(n_hits)

    with open(args.cluster_file, "r") as f:
        cluster_data = json.load(f)

    for i, cluster in enumerate(cluster_data['clusters']):
        rmsd = ftrmsd[cluster['center']]
        if (rmsd < 10.0):
            eval_results['rank'] = int(i+1)
            eval_results['rmsd'] = float(rmsd)
            eval_results['population'] = int(len(cluster['members']))
            break

    print(eval_results)
    with open('evaluation.' + args.out_suffix, "w") as f:
        json.dump(eval_results, f)
