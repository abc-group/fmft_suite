# **FMFT apps** 
Problem-specific FMFT-based protocols.

A collection of FMFT-based applications implemented as python wrappers over the core FMFT executables.

## App list ##
Currently the following protocols are implemented:  

* **fmft_dock** - a protein docking protocol.


## Instructions ##
### **fmft_dock**
A rigid body protein docking protocol that aims to predict the structure of a binary protein complex based on the structures of its unbound constituents.  
The protocol steps are described below. An example case can be found in ``example/docking/1avx``

* **System preparation:**  
   Before you can run fmft\_dock, the pdb structure files of the proteins being docked need to be preprocessed.
   We suggest using [SB Lab Utils](https://bitbucket.org/bu-structure/sb-lab-utils/src/master/) package 
   (or, more precisely, its dedicated interface to psfgen) to do the processing.

        $ sblu pdb prep --psfgen ${path-to-psfgen} \
                    --prm ${fmft-install-dir}/bin/prms/charmm_param.prm \
                    --rtf ${fmft-install-dir}/bin/prms/charmm_param.rtf \
                    --no-minimize \
                    --out-prefix protein_prep
                    protein.pdb
   Here ``${fmft-install-dir}`` is the fmft suite root installation directory (by default, ``install-local``)

* **Running fmft_dock**:  
   The default way to run fmft\_dock can be summaraized by the following command:
  
         $ python ${fmft-install-dir}/bin/fmft_dock.py \
                                             receptor_prep.pdb \
                                             ligand_prep.pdb \
                                             ${fmft-install-dir}/bin/prms/fmft_weights_ei.txt
   Here ``receptor_prep.pdb`` and ``ligand_prep.pdb`` are the preprocessed protein structures to be docked 
   and ``fmft_weights_ei.txt`` is the dedicated file supplying weighting coefficients for the scoring function. 
   You may use a different weighting file if you know what you are doing. 
   ``fmft_dock.py`` also accepts a variety of optional arguments (you can get the full list with ``--help`` option), t
   he most notable one being ``-p`` specifying the number of CPU cores to be used for computation.

* **Processing results:**  
  The direct output of ``fmft_dock.py`` consists of two files, ``ft.000.0.0`` and ``rm.000.0.0``, 
  specifying the top-scoring poses sampled by the protocol. These should NOT be taken as final docking predictions. 
  Instead, these poses are subject to post-processing with sblu:

        $ sblu measure pwrmsd ligand_prep.pdb -o pwrmsd.000.0.0 ft.000.0.0 rm.000.0.0
        $ sblu docking cluster --json -o clusters.000.0.0.json pwrmsd.000.0.0
  These post-processing steps essentially reduce to clustering the top-scoring poses.
  Note that by default the top-1000 poses from ft and rm files is processed. 
  (can be changed using optional arguments, but this is not recommended).  
  **The centers of the resulting clusters are the final docking models**.
  These models are ranked according to the populations of the corresponding clusters.
  This is the intended way to rank the final models, and we do not recommend using 
  raw scoring function values from ft files, as these are generally not discriminatory
  in the low energy limit. 
  Detailed information about the clusters can be found in the ``clusters.000.0.0.json`` file. 

    To visualize the final models, you can build the corresponding pdb files with sblu:

         $ sblu docking gen_cluster_pdb clusters.000.0.0.json ft.000.0.0 rm.000.0.0 ligand_prep.pdb

## References ##
Dzmitry Padhorny, Andrey Kazennov, Brandon S. Zerbe, Kathryn A. Porter, Bing Xia, Scott E. Mottarella, Yaroslav Kholodov, David W. Ritchie, Sandor Vajda, and Dima Kozakov  
[Protein-protein docking by fast generalized Fourier transforms on 5D rotational manifolds](http://www.pnas.org/content/113/30/E4286)  
PNAS 2016 113: E4286-E4293.
