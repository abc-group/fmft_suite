#!/bin/bash

fmft_install_dir="../../../../install-local"

# Preprocess the pdb files
sblu pdb prep --prm ${fmft_install_dir}/bin/prms/charmm_param.prm \
	      --rtf ${fmft_install_dir}/bin/prms/charmm_param.rtf \
              --no-minimize \
	      --out-prefix 1avx_r_prep \
              1avx_r_nmin.pdb
sblu pdb prep --prm ${fmft_install_dir}/bin/prms/charmm_param.prm \
	      --rtf ${fmft_install_dir}/bin/prms/charmm_param.rtf \
              --no-minimize \
	      --out-prefix 1avx_l_prep \
              1avx_l_nmin.pdb

# Run fmft_dock
python ${fmft_install_dir}/bin/fmft_dock.py \
	                        1avx_r_prep.pdb \
				1avx_l_prep.pdb \
				${fmft_install_dir}/bin/prms/fmft_weights_ei.txt

# Cluster results
sblu measure pwrmsd -o pwrmsd.000.0.0  1avx_l_prep.pdb ft.000.0.0 rm.000.0.0
sblu docking cluster --json -o clusters.000.0.0.json pwrmsd.000.0.0

# Build cluster models
sblu docking gen_cluster_pdb clusters.000.0.0.json ft.000.0.0 rm.000.0.0 1avx_l_prep.pdb
