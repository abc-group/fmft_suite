## Example case ##
This directory contains an example docking case for fmft\_dock.   
Run ``bash run.sh`` to execute the full preparation/docking/postprocessing pipeline as described in ``fmft_apps/README.md``.  
The resulting docking models will be written to ``ligXX.pdb`` files.  

