#!/usr/bin/python
import re
import os
import sys
import errno
import argparse
import json
from subprocess import call

bin_path = os.path.dirname(os.path.realpath(__file__))

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def get_optimal_z_range(ft_filename, z_step):
    """
    :param ft_filename: Low resolution ftresults file that contains
                        a single ftresult per translation step.
    :param z_step: Translation step size.
    :return: The "optimized" translation range for high resolution search.
    """
    with open(ft_filename, "r") as f:
        lines = f.readlines()
        e_arr = []

        for line in lines:
            e = re.split(" +", line.rstrip())[4]
            e_arr.append(float(e))

        e_inf = 0.0
        for i in range(len(e_arr)):
            if e_arr[i] <= e_inf:
                left_id = i
                break

        # we are chosing the region with energies lower
        # than mean as the optimal range.
        e_mean = sum(e_arr[left_id:])/len(e_arr[left_id:])
        for i in range(len(e_arr[left_id:])):
            if (e_arr[-i] <= e_mean):
                right_id = len(e_arr) - i - 1
                break

        z_start = z_step * left_id
        z_stop = z_step * (right_id + 6)
        # the '+6' is an empirically chosen padding

        return z_start, z_stop


def fmft_core(coeffs_file_a,
              coeffs_file_b,
              weights_file,
              nres=1000,
              z_start=0.0,
              z_stop=40.0,
              z_step=1.0,
              cutoff=6.0,
              fft_size=59,
              proc_count=1,
              low_res_scan=False,
              label="0.0"):
    with open(weights_file, "r") as f:
        weights = f.readlines()

    # Iterating through the weight sets in the weights file
    for weight_set in weights:
        weight_id = weight_set.split()[0]
        weight_suffix = "%03d" % int(weight_id)
        output_suffix = weight_suffix + '.' + label

        coeffs_file_a_scaled = coeffs_file_a + '.' + weight_suffix
        call([bin_path + '/fmft_scale_coeffstack',
              coeffs_file_a_scaled,
              coeffs_file_a,
              weights_file,
              weight_id])
        if not os.path.isfile(coeffs_file_a_scaled):
            print("[Error] File does not exist: %s" % coeffs_file_a_scaled)
            sys.exit(1)

        # Perform low resolution scanning to identify the optimal translation range.
        # This stage was designed for protein docking
        # and should only be used for that application.
        if low_res_scan:
            lr_n = 20
            lr_fft_size = 30
            call(['mpirun', '-np', str(proc_count),
                  bin_path + "/fmft_core",
                  '--fft_size', str(lr_fft_size),
                  '-N', str(lr_n),
                  '--nres_per_z', str(1),
                  '--output_suffix', output_suffix + '.lr',
                  '--skip_sort',
                  coeffs_file_a_scaled,
                  coeffs_file_b,
                  str(z_start),
                  str(z_stop),
                  str(z_step)])
            z_start_opt, z_stop_opt = get_optimal_z_range(
                                            'ft.' + output_suffix + '.lr',
                                            z_step)
        else:
            z_start_opt = z_start
            z_stop_opt = z_stop


        # Perform full resolution search.
        call(['mpirun', '-np', str(proc_count),
              bin_path + "/fmft_core",
              '--fft_size', str(fft_size),
              '--angular_cutoff', str(cutoff),
              '--nres_per_z', str(nres),
              '--output_suffix', output_suffix,
              coeffs_file_a_scaled,
              coeffs_file_b,
              str(z_start_opt),
              str(z_stop_opt),
              str(z_step)])
