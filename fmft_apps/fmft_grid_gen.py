#!/usr/bin/python

import os
import sys
import argparse
from subprocess import call

#def fmft_grid_gen_dock(grids_file,
#                       rec_pdb,
#                       lig_pdb,
#                       mask_rec = None,
#                       mask_lig = None,
#                       mask_radius = None)

def cleanup_partial():
    os.remove("rec_rvdw.dx")
    os.remove("rec_avdw.dx")
    os.remove("rec_coul.dx")
    os.remove("rec_born.dx")
    os.remove("rec_eigen_0.dx")
    os.remove("rec_eigen_1.dx")
    os.remove("rec_eigen_2.dx")
    os.remove("rec_eigen_3.dx")
    os.remove("lig_avdw.dx")
    os.remove("lig_coul.dx")
    os.remove("lig_born.dx")
    os.remove("lig_eigen_0.dx")
    os.remove("lig_eigen_1.dx")
    os.remove("lig_eigen_2.dx")
    os.remove("lig_eigen_3.dx")

def cleanup():
    os.remove("rec_rvdw.dx")
    os.remove("rec_avdw.dx")
    os.remove("rec_coul.dx")
    os.remove("rec_born.dx")
    os.remove("rec_eigen_0.dx")
    os.remove("rec_eigen_1.dx")
    os.remove("rec_eigen_2.dx")
    os.remove("rec_eigen_3.dx")
    os.remove("lig_rvdw.dx")
    os.remove("lig_avdw.dx")
    os.remove("lig_coul.dx")
    os.remove("lig_born.dx")
    os.remove("lig_eigen_0.dx")
    os.remove("lig_eigen_1.dx")
    os.remove("lig_eigen_2.dx")
    os.remove("lig_eigen_3.dx")
    os.remove("dummy.txt")

def dock(grids_rec_file,
         grids_lig_file,
         rec_pdb,
         lig_pdb):

    dir_path = os.path.dirname(os.path.realpath(__file__))
    #fixed parameters:
    atom_prm = dir_path + "/prms/atoms.0.0.4.prm.ms.3cap+0.5ace.Hr0rec"
    rot_prm  = dir_path + "/prms/rot70k.0.0.4.prm"

    # Create a dummy coefficient file
    open("dummy.txt", 'a').close()

    # Check that protein and EM map files exist

    # Generate grids
    call([dir_path + '/grid_gen.x86_64',
          '-p', atom_prm,
          '-f', 'dummy.txt',
          '-r', rot_prm,
          '-c', '1.0',
          '-k', '4',
          '-R', '1',
          rec_pdb, lig_pdb])

    with open(grids_rec_file, "w") as f:
        f.write("8\n");
        f.write("%s\n" % "rec_rvdw.dx")
        f.write("%s\n" % "rec_avdw.dx")
        f.write("%s\n" % "rec_coul.dx")
        f.write("%s\n" % "rec_born.dx")
        f.write("%s\n" % "rec_eigen_0.dx")
        f.write("%s\n" % "rec_eigen_1.dx")
        f.write("%s\n" % "rec_eigen_2.dx")
        f.write("%s\n" % "rec_eigen_3.dx")

    with open(grids_lig_file, "w") as f:
        f.write("8\n");
        f.write("%s\n" % "lig_rvdw.dx")
        f.write("%s\n" % "lig_avdw.dx")
        f.write("%s\n" % "lig_coul.dx")
        f.write("%s\n" % "lig_coul.dx")
        f.write("%s\n" % "lig_eigen_0.dx")
        f.write("%s\n" % "lig_eigen_1.dx")
        f.write("%s\n" % "lig_eigen_2.dx")
        f.write("%s\n" % "lig_eigen_3.dx")


def fit(grids_file,
        prot_pdb):

    dir_path = os.path.dirname(os.path.realpath(__file__))
    #fixed parameters:
    atom_prm = dir_path + "/prms/atoms.0.0.4.prm.ms.3cap+0.5ace.Hr0rec"
    rot_prm  = dir_path + "/prms/rot70k.0.0.4.prm"

    # Create a dummy coefficient file
    open("dummy.txt", 'a').close()

    # Check that protein and EM map files exist

    # Generate grids
    call([dir_path + '/grid_gen.x86_64',
                        '-p', atom_prm,
                        '-f', 'dummy.txt',
                        '-r', rot_prm,
                        '-c', '1.0',
                        '-k', '4',
                        '-R', '1',
                        prot_pdb, prot_pdb])

    with open(grids_file, "w") as f:
        f.write("1\n");
        f.write("%s" % ("lig_rvdw.dx"))
    cleanup_partial()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("grids_file",
                        help = "File that will contain the list of grid pairs",
                        type = str)
    parser.add_argument("protein_pdb",
                        help = "PDB of a protein to be fitted.",
                        type = str)
    args = parser.parse_args()

    #1. Check if files are there
    if not os.path.isfile(args.protein_pdb):
        print("[Error] File does not exist: %s" % args.protein_pdb)
        sys.exit(1)

    fit(args.grids_file,
        args.protein_pdb)
