#!/usr/bin/python

import os
import sys
import errno
import argparse
from subprocess import call
from fmft_core import fmft_core
import fmft_grid_gen

bin_path = os.path.dirname(os.path.realpath(__file__))


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def fmft_transform_stack(grids_file,
                         N,
                         coeffs_file,
                         lambda_factor=20.0,
                         transform_center=None,
                         cutoff_radius=None):
    """
    Transform the stack of grids specified in the grids_file
    and write the resulting coefficient stack to coeffs_file.
    Coefficients up to order N will be computed.
    :param grids_file       File specifying grids in the stack.
    :param N                Maximal coefficients polymomial order.
    :param coeffs_file      File to which the resulting coefficients
                             stack will be written.
    :param lambda_factor    Lambda scaling factor for radial harmonics used
                             in coefficient calculation.
    :param transform_center 3-vector (Provided as a space-delimited string)
                             specifying the center of transform.
                             By default, the center of the first grid in
                             the stack is used as transform center.
    :param cutoff_radius    Grid points lying outside of this distance
                             from the transform center will be ignored.
    """
    run_args=[bin_path + '/fmft_transform_stack']

    if transform_center != None:
        run_args += ["-c", "%s" % transform_center]

    if cutoff_radius != None:
        run_args += ["-r", "%f" % cutoff_radius]

    run_args+=[grids_file,
               N,
               str(lambda_factor),
               coeffs_file]

    call(run_args)

    if not os.path.isfile(coeffs_file):
        print("[Error] File does not exist: %s" % coeffs_file)
        sys.exit(1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("rec_pdb",
                        help="PDB file of the receptor protein.",
                        type=str)
    parser.add_argument("lig_pdb",
                        help="PDB file of the ligand protein.",
                        type=str)
    parser.add_argument("weights_file",
                        help="Scoring function weights file.",
                        type=str)
    parser.add_argument("--proc_count", "-p",
                        help="Number of cpu cores to use for computation.",
                        type=int,
                        default=4)
    parser.add_argument("--nres", "-n",
                        help="Number of results to store in the output file.",
                        type=int,
                        default=1000)
    parser.add_argument("--rec_center",
                        help="Center of receptor decomposition. 3-vector specified as 'X Y Z'. The quotation marks are important!",
                        type=str,
                        default=None)
    parser.add_argument("--z_start",
                        help="",
                        type=float,
                        default=0.0)
    parser.add_argument("--z_stop",
                        help="",
                        type=float,
                        default=50.0)
    parser.add_argument("--z_step",
                        help="",
                        type=float,
                        default=1.0)
    parser.add_argument("--no_prescan",
                        help="",
                        action="store_true")
    args = parser.parse_args()

    # Check if files are there.
    if not os.path.isfile(args.rec_pdb):
        print("[Error] File does not exist: %s" % args.rec_pdb)
        sys.exit(1)
    if not os.path.isfile(args.lig_pdb):
        print("[Error] File does not exist: %s" % args.lig_pdb)
        sys.exit(1)
    if not os.path.isfile(args.weights_file):
        print("[Error] File does not exist: %s" % args.weights_file)
        sys.exit(1)

    N = '30'

    # Generate grids and prepare a grids list.
    grids_rec_file = "grids.rec.list"
    grids_lig_file = "grids.lig.list"
    fmft_grid_gen.dock(grids_rec_file,
                       grids_lig_file,
                       args.rec_pdb,
                       args.lig_pdb)
    if not os.path.isfile(grids_rec_file):
        print("[Error] File does not exist: %s" % grids_rec_file)
        sys.exit(1)
    if not os.path.isfile(grids_lig_file):
        print("[Error] File does not exist: %s" % grids_lig_file)
        sys.exit(1)

    # Convert grids to coefficients.
    coeffs_rec_file = "coeffstack.rec.0"
    coeffs_lig_file = "coeffstack.lig.0"
    fmft_transform_stack(grids_rec_file,
                         N,
                         coeffs_rec_file,
                         transform_center=args.rec_center)
    fmft_transform_stack(grids_lig_file,
                         N,
                         coeffs_lig_file)

    # Cleanup
    fmft_grid_gen.cleanup()

    # Perform fmft search.
    fmft_core(coeffs_rec_file,
              coeffs_lig_file,
              args.weights_file,
              nres=args.nres,
              z_start=args.z_start,
              z_stop=args.z_stop,
              z_step=args.z_step,
              cutoff=6.0,
              fft_size=59,
              low_res_scan=(not args.no_prescan),
              proc_count=args.proc_count)
