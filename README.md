# **FMFT** 
**F**ast **M**anifold **F**ourier **T**ransform suite.

![Scheme](method.jpg)

FMFT suite is a software toolkit designed to tackle the problem of rigid body search.  

It is based on the theory of Fast Manifold Fourier transforms and can be used to approach a variety of search problems as long as the score of any rigid body conformation can be expressed as an overlap of two densities. 
 
FMFT protein docking protocol also lives here.

## Code organization ##
The toolkit is split into the following major components:  

* **libfmft** - the central part of the package, a C library containing various routines needed to compute FMFT correlataions.
* **fmft_core** - the core executables written in C that perform the computationally intensive parts of work.
* **fmft_apps** - problem-specific protocols implemented as python wrappers over core executables.
* **grid_gen** - a precompiled module needed for the protein docking application. Note that it is distributed under a separate license.

## Requirements ##
* Main
    * GCC compiler
    * FFTW library
    * GMP library
    * Doxygen (to generate developer documentation)
    * [Check](https://libcheck.github.io/check/) 
    * MPI
    * Cmake 3.0 and above.  

* Application-specific.  
    * Protein docking:
        * Python version 3.4.X and above.
        * [SB Lab Utils](https://bitbucket.org/bu-structure/sb-lab-utils/src/master/) (Installing from source is recommended) 

## Installation ##
1.  To build the code, run

        $ ./bootstrap.sh
    All the components will be installed to `install-local/`, which is created in the current directory.

2.  Some of the core components require translation matrices to be precomputed.  
    This has to be done only once and helps to save a lot of computational resources. 

        $ ./gen_trans_matrices.sh 0 90 1.0 
    >The whole process might take about an hour, the resulting files will be stored in `install-local/fmft_data/`

3.  Add `install-local/bin` to your **PATH** variable.

## Usage ##
See **fmft_apps/README.md** for application-specific usage instructions.

## License ##
See LICENSE.TXT for details.

## Authors ##
Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

## References ##
Dzmitry Padhorny, Andrey Kazennov, Brandon S. Zerbe, Kathryn A. Porter, Bing Xia, Scott E. Mottarella, Yaroslav Kholodov, David W. Ritchie, Sandor Vajda, and Dima Kozakov  
[Protein-protein docking by fast generalized Fourier transforms on 5D rotational manifolds](http://www.pnas.org/content/113/30/E4286)  
PNAS 2016 113: E4286-E4293.
