#!/bin/bash
# ./gen_trans_matrices.sh Z_START Z_STOP Z_STEP
# Generates translation matrices for all translations from Z_START to Z_STOP Angstrom
# with a step of Z_STEP Angstrom and writes them to install-local/bin/data/.
#
Z_START=${1}
Z_STOP=${2}
Z_STEP=${3}
DATA_FOLDER="install-local/fmft_data"
LAMBDA="20.00"
mkdir -p install-local/fmft_data
echo -e "Genertaing translation matrix files for all translations in range ${Z_START}...${Z_STOP} with a step of ${Z_STEP}"
echo -e "\033[33m Running command: ./install-local/bin/fmft_tab_tmatrix ${DATA_FOLDER} ${Z_START} ${Z_STOP} ${Z_STEP} ${LAMBDA}\033[0m"
./install-local/bin/fmft_tab_tmatrix ${DATA_FOLDER} ${Z_START} ${Z_STOP} ${Z_STEP} ${LAMBDA}
