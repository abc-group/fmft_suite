/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include "fmft/libfmft.h"
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define _USE_MATH_DEFINES
#include <fenv.h>
/*
Command line format:

./test_complex_inv input_file cell_size grid_size output_file

*/
int main(int argc, char *argv[])
{
	feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
	if (argc != 5)
	{
		printf("Usage %s input_file grid_size output_file\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	char* input_file = argv[1];
	int grid_size = atoi(argv[2]);
	char* output_file = argv[3];

	printf("Reading coefficients from %s\n", input_file);
	struct fmft_coeffset* coeffs;
	coeffs = fmft_coeffset_read(input_file);

	printf("Converting spf coefficients to grid\n");
	struct fmft_vector_3i grid_shape = {grid_size, grid_size, grid_size};
	struct fmft_grid_3D *g;
	g = fmft_coeffset_to_grid(coeffs, &grid_shape, NULL, NULL);

	printf("Saving results to file %s...\n", output_file);
	fmft_grid_3D_write(g, output_file);
	printf(".dx created.\n");

	fmft_coeffset_free(coeffs);
	fmft_grid_3D_free(g);

	printf("Done.\n");

	return 0;
}

