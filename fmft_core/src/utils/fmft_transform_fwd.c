/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include "fmft/libfmft.h"
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define _USE_MATH_DEFINES
#include <fenv.h>
/*
Command line format:

./test_complex_for input_file N_limit output_file

*/
int main(int argc, char *argv[])
{
	feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
	if (argc != 4)
	{
		printf("Usage %s input_file N_limit output_file\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	char* input_file = argv[1];
	int N_limit = atoi(argv[2]);
	char* output_file = argv[3];

	printf("Reading grid from %s\n", input_file);
	struct fmft_grid_3D* g;
	g = fmft_grid_3D_read(input_file);
	printf("The grid size is %d by %d by %d grid points.\n",
						g->shape.X,
						g->shape.Y,
						g->shape.Z);

	printf("grid origin: %f %f %f\n", g->origin.X, g->origin.Y, g->origin.Z);

	printf("Converting grid to spf coefficiets.\n");
	struct fmft_coeffset* coeffs;
	coeffs = fmft_coeffset_from_grid(g, N_limit, 20.0, NULL, -1);

	printf("Saving results to file %s...\n", output_file);
	fmft_coeffset_write(coeffs, output_file);

	fmft_grid_3D_free(g);
	fmft_coeffset_free(coeffs);

	printf("Done.\n");

	return 0;
}

