/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include "fmft/libfmft.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <libgen.h>
#include <unistd.h>

/*
 * ./tab_trans_matrix Z_start Z_stop Z_step
 */
int main(int argc, char **argv)
{
	clock_t t1, t2;

	if (argc != 6) {
		printf("Usage: %s path Z_start Z_stop Z_step lambda\n", argv[0]);
		printf("Tabulate a series of translation matrices for spf coefficients\n");
		printf("in tanslational range [Z_start; Z_stop] with a step of Z_step,\n");
		printf("and using a scaling parameter lambda.\n");
		printf("The resulting matrix files will be created in the path directory\n");
		exit(EXIT_FAILURE);
	}
	char* folder_name = argv[1];
	float Z_start = atof(argv[2]);
	float Z_stop  = atof(argv[3]);
	float Z_step  = atof(argv[4]);
	float lambda  = atof(argv[5]);

	char T_file[1024];
	for (double z = Z_start; z <= Z_stop; z += Z_step) {
		sprintf(T_file, "%s/%s_%.2f_%d_%.2lf.dat", folder_name, TFILE, lambda, NMAX, z);
		if (access(T_file, F_OK) == -1) {
			printf("Calculating translation matrix for z = %.2lf:\n", z);
			t1 = clock();
			fmft_tmatrix_tabulate(NMAX, lambda, z, folder_name);
			t2 = clock();
			printf("time: %lf\n", 1.0*(t2 - t1)/CLOCKS_PER_SEC );
		} else {
			printf("File %s already exists, skipping.\n", T_file);
		}

	}

	printf("Done.\n");

	return 0;
}
