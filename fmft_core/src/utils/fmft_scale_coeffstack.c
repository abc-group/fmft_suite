/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include "fmft/libfmft.h"

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		printf("usage: %s coeffs_scaled_file coeffs_file weights_file weight_set_id", argv[0]);
		printf("Scale the coefficient sets in a coefficeint stack\n");
		printf("using weight set weight_set_id from weights_file\n");
		exit(1);
	}
	char* coeffs_scaled_file = argv[1];
	char* coeffs_file = argv[2];
	char* weights_file = argv[3];
	int weight_id = atoi(argv[4]);
	struct fmft_coeffset_stack* mcoeffs = fmft_coeffset_stack_read(coeffs_file);
	struct fmft_weightset_list* mweights = fmft_weightset_list_read(weights_file);
	if (weight_id + 1 > mweights->count)
	{
		fprintf(stderr, "Specified weight index is out of range\n Exiting...");
		exit(1);
	}
	struct fmft_coeffset_stack* mcoeffs_scaled = fmft_coeffset_stack_scale(mcoeffs, mweights->wset[weight_id]);

	fmft_coeffset_stack_write(mcoeffs_scaled, coeffs_scaled_file);

	return 0;
}
