/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include "fmft/libfmft.h"
#define DATA_FOLDER "data"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <libgen.h>
#include <fenv.h>

struct args {
	char* grid_list_file;
	int N;
	float lambda;
	char* output_file;
	struct fmft_vector_3f* center;
	float r_cutoff;
};

void print_help_short(char *binary_name)
{
	fprintf(stderr, "\n");
	fprintf(stderr, "usage: %s [options] GRID_LIST_FILE N LAMBDA OUTPUT_FILE\n\n", binary_name);
	fprintf(stderr, "Perform a Spherical polar Fourier transform\n");
	fprintf(stderr, "for a set of density grids specified in GRID_LIST_FILE,\n");
	fprintf(stderr, "computing the transform coefficients up to a polynomial order N,\n");
	fprintf(stderr, "and using a scaling parameter LAMBDA.\n");
	fprintf(stderr, "Resulting coefficient sets are written to OUTPUT_FILE\n");
}

void print_help_full(char *binary_name)
{
	print_help_short(binary_name);
	fprintf(stderr, "\n");
	fprintf(stderr, "Optional arguments:\n");
	fprintf(stderr, " %20s %s",
			"-c 'X Y Z'",
			"Center of decomposition. Note the quotation marks\n");
}

void parse_argv(int argc, char *argv[], struct args* arg)
{
	char* binary_name = argv[0];

	int c;
	while ((c = getopt(argc, argv, "c:hr:")) != -1) {
		switch(c) {
			case 'c': {
				arg->center = (struct fmft_vector_3f*) calloc(1, sizeof(struct fmft_vector_3f));
				char* tmp_str = strdup(optarg);
				char* tok;
				tok = strtok(tmp_str, " ");
				if (tok == NULL) {
					fprintf(stderr, "[Error] Transform center should be a space-delimited 3-vector\n");
					exit(EXIT_FAILURE);
				}
				arg->center->X = atof(tok);
				tok = strtok(NULL, " ");
				if (tok == NULL) {
					fprintf(stderr, "[Error] Transform center should be a space-delimited 3-vector\n");
					exit(EXIT_FAILURE);
				}
				arg->center->Y = atof(tok);
				tok = strtok(NULL, " ");
				if (tok == NULL) {
					fprintf(stderr, "[Error] Transform center should be a space-delimited 3-vector\n");
					exit(EXIT_FAILURE);
				}
				arg->center->Z = atof(tok);

				break;
			}
			case 'r': {
				arg->r_cutoff = atof(optarg);
				break;
			}
			case 'h': {
				print_help_full(binary_name);
				exit(EXIT_SUCCESS);
				break;
			}
			default:
				break;
		}
	}
	if (optind + 1 < argc) {
		arg->grid_list_file = argv[optind];
		optind++;
		arg->N = atoi(argv[optind]);
		optind++;
		arg->lambda = atof(argv[optind]);
		optind++;
		arg->output_file = argv[optind];
		optind++;
		while (optind < argc) {
			fprintf(stderr, "[Warning] Ignored argument: %s\n", argv[optind]);
			optind++;
		}
	}
	else {
		print_help_short(binary_name);
		fprintf(stderr, "try '%s -h' for a list of options\n", binary_name);
		exit(EXIT_FAILURE);
	}

}

int main(int argc, char *argv[])
{

	struct args arg;
	arg.grid_list_file = NULL;
	arg.N = 0;
	arg.lambda = 0;
	arg.output_file = NULL;
	arg.center = NULL;
	arg.r_cutoff = -1;
	parse_argv(argc, argv, &arg);

	printf("grid_list_file: %s\n", arg.grid_list_file);
	printf("N             : %d\n", arg.N);
	printf("lambda        : %f\n", arg.lambda);
	printf("output_file   : %s\n", arg.output_file);
	if (arg.center != NULL)
		printf("center        : %lf, %lf, %lf\n", arg.center->X, arg.center->Y, arg.center->Z);
	if (arg.r_cutoff > 0)
		printf("r_cutoff      : %lf\n", arg.r_cutoff);

	//read grids
	struct fmft_grid_3D_list* grids = fmft_grid_3D_list_read(arg.grid_list_file);

	//calculate spf coefficients
	clock_t start, end;
	start = clock();
	struct fmft_coeffset_stack* coeff_st = fmft_coeffset_stack_from_grids(
									grids,
									arg.N,
									arg.lambda,
									arg.center,
									arg.r_cutoff);
	end = clock();
	printf("SPF transform time: %f sec\n", 1.0f * (end - start)/CLOCKS_PER_SEC);

	//write spf coefficients to output_file
	fmft_coeffset_stack_write(coeff_st, arg.output_file);

	fmft_grid_3D_list_free(grids);
	fmft_coeffset_stack_free(coeff_st);

	return 0;
}
