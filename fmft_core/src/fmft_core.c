/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#include "fmft/libfmft.h"
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>
#include <libgen.h>
#include <fenv.h>
#include <time.h>
#include <fftw3.h>

#define _USE_MATH_DEFINES
#define _MY_ROOT_ 0
#define DATA_FOLDER "data"


#ifdef _MPI_
#include "mpi.h"
#endif

#ifdef _MPI_

MPI_Datatype mpi_type_score ()
{
	struct fmft_result sendscore[1];
	MPI_Datatype MPI_SCORE;
	MPI_Datatype types[7] = {MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE};
	int blocklens[7] = {1, 1, 1, 1, 1, 1, 1};
	MPI_Aint addrs[8];
	MPI_Aint disps[7];

	/* Compute element offset
	 * relative to sendscore */
	MPI_Address(sendscore, &addrs[0]);
	MPI_Address(&(sendscore->val),    &addrs[1]);
	MPI_Address(&(sendscore->beta),   &addrs[2]);
	MPI_Address(&(sendscore->gamma),  &addrs[3]);
	MPI_Address(&(sendscore->alpha1), &addrs[4]);
	MPI_Address(&(sendscore->beta1),  &addrs[5]);
	MPI_Address(&(sendscore->gamma1), &addrs[6]);
	MPI_Address(&(sendscore->Z),      &addrs[7]);

	disps[0] = addrs[1] - addrs[0];
	disps[1] = addrs[2] - addrs[0];
	disps[2] = addrs[3] - addrs[0];
	disps[3] = addrs[4] - addrs[0];
	disps[4] = addrs[5] - addrs[0];
	disps[5] = addrs[6] - addrs[0];
	disps[6] = addrs[7] - addrs[0];

	MPI_Type_struct (7, blocklens, disps, types, &MPI_SCORE);
	MPI_Type_commit (&MPI_SCORE);

	return MPI_SCORE;
}
#endif


struct args {
	char*  coeffs_file_a;	// generalized Fourier coefficients file
	char*  coeffs_file_b;	// generalized Fourier coefficients file
	double z_start;
	double z_stop;
	double z_step;

	double angular_cutoff;
	int    fft_size;
	int    N;
	int    results_per_z;   // Number of best scores processed for each translation step
	char*  output_suffix;
	int    sort_results;
	char*  tmatrix_folder; //TODO Change to a more safe variant.
};

void args_init(struct args* arg)
{
	arg->coeffs_file_a = NULL;
	arg->coeffs_file_b = NULL;
	arg->z_start = -1;
	arg->z_stop = -1;
	arg->z_step = -1;

	arg->angular_cutoff = -1;
	arg->fft_size = -1;
	arg->N = -1;
	arg->results_per_z = 1000;
	arg->output_suffix = NULL;
	arg->sort_results = 1;
	arg->tmatrix_folder = NULL;
}

void print_help_short(char *binary_name)
{
	fprintf(stderr, "\n");
	fprintf(stderr, "usage: %s [options] COEFFS_FILE_A COEFFS_FILE_B Z_START Z_STOP Z_STEP\n\n", binary_name);
	fprintf(stderr, "Perform the correlation of the two coefficient stacks on the SO(3)\\SxSO(3) manifold\n");
	fprintf(stderr, "for a series of translation steps and report the filtered correltation minima.\n");
}

void print_help_full(char *binary_name)
{
	print_help_short(binary_name);
	fprintf(stderr, "\n");
	fprintf(stderr, "Optional arguments:\n");

	fprintf(stderr, " %20s %s\n %-20s (default: %s)\n",
			"--angular_cutoff",
			"Angular cutoff for the filtering step.",
			" ", "360/(fft_size+1) degrees");

	fprintf(stderr, " %20s %s\n %-20s (default: %s)\n",
			"--fft_size",
			"Bandwidth B of the FFT array.",
			"", "B = 2N+1, where N is the highest polynomial order of coefficients.");

	fprintf(stderr, " %20s %s\n %-20s (default: %s)\n",
			"--N",
			"Highest plynomial order of coefficients.",
			"", "Determined from coefficient stack files.");

	fprintf(stderr, " %20s %s\n %-20s (default: %s)\n",
			"--nres_per_z",
			"Number of filtered results reported per translations step.",
			"", "1000");

	fprintf(stderr, " %20s %s\n %-20s (default: %s)\n",
			"--output suffix",
			"The suffix appended to the output ft and rm files.",
			"", "xxx");

	fprintf(stderr, " %20s %s\n %-20s (default: %s)\n",
			"--skip_sort",
			"Skip global sorting of the results. Results are only sorted for each translation step.",
			"", "");

	fprintf(stderr, " %20s %s\n %-20s (default: %s)\n",
			"--tmatrix_folder",
			"The folder in which the program will look for tabulated translation matrices.",
			"", "${bin_dir}/../fmfr_data , wehere bin_dir is the location of the program binary.");
}

void parse_argv(int argc, char *argv[], struct args* arg)
{
	char* binary_name = argv[0];

	args_init(arg);

	struct option long_opt[] = {
		{"help",		no_argument,		NULL, 'h'},
		{"angular_cutoff",	required_argument,	NULL, 'a'},
		{"fft_size",		required_argument,	NULL, 'f'},
		{"N",			required_argument,	NULL, 'N'},
		{"nres_per_z",		required_argument,	NULL, 'n'},
		{"output_suffix",	required_argument,	NULL, 'o'},
		{"skip_sort",		no_argument,		NULL, 's'},
		{"tmatrix_folder",	required_argument,	NULL, 't'}
	};

	int c;
	while ((c = getopt_long(
			argc, argv, "ha:f:N:n:o:st:",
			long_opt, NULL)) != -1) {
		switch(c) {
			case 'h': {
				print_help_full(binary_name);
				exit(EXIT_SUCCESS);
				break;
			}
			case 'a': {
				arg->angular_cutoff = atof(optarg) * M_PI / 180.0;
				break;
			}
			case 'f': {
				arg->fft_size = atoi(optarg);
				break;
			}
			case 'N': {
				arg->N = atoi(optarg);
				break;
			}
			case 'n': {
				arg->results_per_z = atoi(optarg);
				break;
			}
			case 'o': {
				arg->output_suffix = optarg;
				break;
			}
			case 's': {
				arg->sort_results = 0;
				break;
			}
			case 't': {
				arg->tmatrix_folder = optarg;
				break;
			}
			default:
				break;
		}
	}
	if (optind + 1 < argc) {
		arg->coeffs_file_a = argv[optind];
		optind++;
		arg->coeffs_file_b = argv[optind];
		optind++;
		arg->z_start = atof(argv[optind]);
		optind++;
		arg->z_stop = atof(argv[optind]);
		optind++;
		arg->z_step = atof(argv[optind]);
		optind++;
		while (optind < argc) {
			fprintf(stderr, "[Warning] Ignored argument: %s\n", argv[optind]);
			optind++;
		}

		// Set defaults for unset string arguments
		if (arg->output_suffix == NULL) {
			arg->output_suffix = (char*) calloc(1024, sizeof(char));
			sprintf(arg->output_suffix, "xxx");
		}
		if (arg->tmatrix_folder == NULL) {
			arg->tmatrix_folder = (char*) calloc(1024, sizeof(char));
			sprintf(arg->tmatrix_folder, "%s/fmft_data", dirname(dirname(argv[0]))); //, "data");
		}
	} else {
		print_help_short(binary_name);
		fprintf(stderr, "try '%s -h' for a list of options\n", binary_name);
		exit(EXIT_FAILURE);
	}

}


int main(int argc, char *argv[])
{
	struct args arg;
	parse_argv(argc, argv, &arg);

	int myrank = 0; // rank id of process
	int nprocs = 1; // the number of processes
	int myroot = 0; //

#ifdef _MPI_
	if ((MPI_Init(&argc, &argv)) != MPI_SUCCESS)
	{
		(void) fprintf (stderr, "MPI_Init failed\n");
		MPI_Abort (MPI_COMM_WORLD, 1);
	}
	if (MPI_Comm_rank(MPI_COMM_WORLD, &myrank) != MPI_SUCCESS)
		MPI_Abort (MPI_COMM_WORLD, 1);
	if (MPI_Comm_size(MPI_COMM_WORLD, &nprocs) != MPI_SUCCESS)
		MPI_Abort (MPI_COMM_WORLD, 1);
#endif


	// read spf coefficients
	struct fmft_coeffset_stack* coeff_st_a_tmp = fmft_coeffset_stack_read(arg.coeffs_file_a);
	struct fmft_coeffset_stack* coeff_st_b_tmp = fmft_coeffset_stack_read(arg.coeffs_file_b);
	if (coeff_st_a_tmp->count != coeff_st_b_tmp->count) {
		fprintf(stderr, "[Error][fmft_core] The number of coefficient sets in the correlated stacks is not equal.\n");
		exit(EXIT_FAILURE);
	}
	if (coeff_st_a_tmp->N != coeff_st_b_tmp->N) {
		fprintf(stderr, "[Error][fmft_core] The polynomial order of coefficient sets in the correlated stacks is not equal.\n");
		exit(EXIT_FAILURE);
	}
	if (coeff_st_a_tmp->lambda != coeff_st_b_tmp->lambda) {
		fprintf(stderr, "[Error][fmft_core] The lambda scaling parameter of the correlated stacks is not equal.\n");
		exit(EXIT_FAILURE);
	}

	// If a maximal polynomial order is set, trim the coefficients to that order.
	struct fmft_coeffset_stack* coeff_st_a = NULL;
	struct fmft_coeffset_stack* coeff_st_b = NULL;
	if (arg.N != -1) {
		coeff_st_a = fmft_coeffset_stack_get_low_order(coeff_st_a_tmp, arg.N);
		coeff_st_b = fmft_coeffset_stack_get_low_order(coeff_st_b_tmp, arg.N);
		fmft_coeffset_stack_free(coeff_st_a_tmp);
		fmft_coeffset_stack_free(coeff_st_b_tmp);
	} else {
		coeff_st_a = coeff_st_a_tmp;
		coeff_st_b = coeff_st_b_tmp;
	}

	// if fft_size was not set, set it to 2 * N - 1
	if (arg.fft_size == -1) {
		arg.fft_size = 2 * coeff_st_a->N - 1;
	}
	if (arg.angular_cutoff == -1) {
		arg.angular_cutoff = 2 * M_PI / (arg.fft_size + 1);
	}


	//get ligand position relative to receptor
	struct fmft_vector_3f ref_lig_pos;
	ref_lig_pos.X = coeff_st_b->origin.X - coeff_st_a->origin.X;
	ref_lig_pos.Y = coeff_st_b->origin.Y - coeff_st_a->origin.Y;
	ref_lig_pos.Z = coeff_st_b->origin.Z - coeff_st_a->origin.Z;

	// distribute the work accross cpu cores
	int z_steps_count = floor((arg.z_stop - arg.z_start) / arg.z_step + 1); // total number of translation steps;
	int z_steps_count_local = z_steps_count / nprocs + 1;       // number of translation steps per core

	// set the translation range boundaries for the current core
	double z_start_local = arg.z_start + myrank * z_steps_count_local * arg.z_step;
	double z_stop_local  = z_start_local + (z_steps_count_local - 1) * arg.z_step;

	// set the number of results produced by the current core
	int results_count_local = z_steps_count_local * arg.results_per_z;
	struct fmft_result* results_local = (struct fmft_result*) malloc(results_count_local * sizeof(struct fmft_result));

	fmft_root_printf(myrank,
			"Using spf coefficients calculated up to N = %d to compute %d correlations.\n",
			coeff_st_a->N, coeff_st_a->count);
	fmft_root_printf(myrank,
			"Using FFT grid with dimensions: %d x %d x (%d x %d x %d)\n",
			arg.fft_size / 2 + 1, arg.fft_size / 2 + 1, arg.fft_size, arg.fft_size, arg.fft_size);
	fmft_root_printf(myrank,
			"Using angular cutoff of %.2lf deg. for filtering. (Largest grid step is %.2lf deg.)\n",
			arg.angular_cutoff * 180.0 / M_PI, 180.0 / ((int)arg.fft_size / 2) );
	fmft_root_printf(myrank,
			"\"Raw\" translation range: %.2lf - %.2lf Angstrom, step: %.2lf Angstrom\n",
			arg.z_start, arg.z_stop, arg.z_step);
	fmft_root_printf(myrank,
			"Results will be provided relative to the reference position: %lf %lf %lf\n",
			ref_lig_pos.X, ref_lig_pos.Y, ref_lig_pos.Z);
	fmft_root_printf(myrank,
			"The task will be completed using %d cpu cores.\n",
			nprocs);
	fmft_root_printf(myrank,
			"Each core will handle %d translation steps.\n",
			z_steps_count_local);
	fmft_root_printf(myrank,
			"Padded translation range: %.2lf - %.2lf Angstrom.\n",
			arg.z_start, arg.z_start + (z_steps_count_local * nprocs - 1) * arg.z_step);


	// correlate and report correlation minima
	fmft_coeffstack_correlate(
			results_local,
			arg.results_per_z,
			coeff_st_a,
			coeff_st_b,
			arg.fft_size,
			z_start_local,
			z_stop_local,
			arg.z_step,
			arg.angular_cutoff,
			arg.tmatrix_folder,
			myrank);

	// collect results
	int results_count = results_count_local * nprocs; // total number of scores
	struct fmft_result* results;
#ifdef _MPI_
	if (myrank == myroot)
	{
		results = (struct fmft_result*) malloc(results_count * sizeof(struct fmft_result));
	}

	MPI_Datatype MPI_SCORE = mpi_type_score ();

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Gather(results_local, results_count_local, MPI_SCORE,
			results, results_count_local, MPI_SCORE, myroot,
			MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Type_free (&MPI_SCORE);
#else
	results = results_local;
#endif

	if (myrank == myroot) {
		// sort results
		if (arg.sort_results == 1)
			fmft_results_sort(results, results_count, results_count);

		// write results in piper-compatible format
		char* output_ft_file = (char*) calloc(1024, sizeof(char));
		char* output_rm_file = (char*) calloc(1024, sizeof(char));
		sprintf(output_ft_file, "ft.%s", arg.output_suffix);
		sprintf(output_rm_file, "rm.%s", arg.output_suffix);
		fmft_results_write_ft(results, results_count, ref_lig_pos, output_ft_file, output_rm_file);
		//free(output_scores);
	}
	fmft_coeffset_stack_free(coeff_st_a);
	fmft_coeffset_stack_free(coeff_st_b);
	free(results_local);

	/***MPI FINALIZE***/
#ifdef _MPI_
	if ((MPI_Finalize()) != MPI_SUCCESS)
	{
		fprintf (stderr, "MPI_Finalize failed\n");
		MPI_Abort (MPI_COMM_WORLD, 1);
	}
#endif
	return 0;
}
